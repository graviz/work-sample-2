<?php

namespace HIP\WaveSlidesInstallBundle\Controller;

use HIP\WaveSlidesBundle\Controller\AbstractController;
use HIP\WaveSlidesCoreBundle\Document\Account\Role;
use HIP\WaveSlidesCoreBundle\Document\Account\SessionData;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage;
use HIP\WaveSlidesCoreBundle\Document\System;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

class DefaultController extends AbstractController {

    /**
     * @Sensio\Route("/install", name="wave_install")
     * @Sensio\Method("GET")
     */
    public function indexAction() {
        $this->dropAll();

        $this->installRoles();
        $this->setupUsers();
        $this->setupLanguages();
        $this->installSystem();
        return $this->render('HIPWaveSlidesInstallBundle:Default:index.html.twig', array());
    }

    private function dropAll() {
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\System')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\Deck')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\Slide')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\SlideElements\Image')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\Account\WaveUser')->drop();
        $this->dm()->getDocumentCollection('HIP\WaveSlidesCoreBundle\Document\Account\Role')->drop();
    }

    private function installRoles() {
        $userRole = new Role();
        $userRole->setName('User');
        $userRole->setRole(Role::USER);

        $adminRole = new Role();
        $adminRole->setName('Admin');
        $adminRole->setRole(Role::ADMIN);

        $this->dm()->persist($userRole);
        $this->dm()->persist($adminRole);
        $this->dm()->flush();
    }

    private function setupUsers() {
        /** @var Role $userRole */
        $userRole = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Account\Role')->findOneBy(array(Role::Role => Role::USER));

        $userManager = $this->get('fos_user.user_manager');

        for ($i = 1; $i < 3; $i++) {
            $user = new WaveUser();
            $user->setUsername('user' . $i);
            $user->setUsernameCanonical('user' . $i);
            $user->setEmail('user' . $i . '@gmx.de');
            $user->setEmailCanonical('user' . $i . '@gmx.de');
            $user->setLocked(false);
            $user->setEnabled(true);
            $user->setExpired(false);
            $user->setRole($userRole);
            $user->setLastLogin(new \DateTime());
            $user->setPlainPassword('user' . $i);
            $user->setSession(SessionData::generate());
            $userManager->updatePassword($user);
            $this->dm()->persist($user);
        }

        $this->dm()->flush();
    }

    private function setupLanguages() {
        $langs = [
            [
                'value' => 'AS',
                'name' => 'ActionScript',
                'css' => 'actionscript',
            ],
            [
                'value' => 'APACHE',
                'name' => 'Apache',
                'css' => 'apache',
            ],
            [
                'value' => 'BASH',
                'name' => 'Bash',
                'css' => 'sh',
            ],
            [
                'value' => 'CSHARP',
                'name' => 'C#',
                'css' => 'cs',
            ],
            [
                'value' => 'CPP',
                'name' => 'C++',
                'css' => 'cpp',
            ],
            [
                'value' => 'CSS',
                'name' => 'CSS',
                'css' => 'css',
            ],
            [
                'value' => 'COFFEESCRIPT',
                'name' => 'CoffeeScript',
                'css' => 'coffee',
            ],
            [
                'value' => 'DIFF',
                'name' => 'Diff',
                'css' => 'diff',
            ],
            [
                'value' => 'HTML',
                'name' => 'HTML',
                'css' => 'html',
            ],
            [
                'value' => 'JSON',
                'name' => 'JSON',
                'css' => 'json',
            ],
            [
                'value' => 'JAVA',
                'name' => 'Java',
                'css' => 'java',
            ],
            [
                'value' => 'JAVASCRIPT',
                'name' => 'JavaScript',
                'css' => 'js',
            ],
            [
                'value' => 'MAKEFILE',
                'name' => 'Makefile',
                'css' => 'makefile',
            ],
            [
                'value' => 'MARKDOWN',
                'name' => 'Markdown',
                'css' => 'md',
            ],
            [
                'value' => 'OBJECTIVEC',
                'name' => 'Objective C',
                'css' => 'objectivec',
            ],
            [
                'value' => 'PHP',
                'name' => 'PHP',
                'css' => 'php',
            ],
            [
                'value' => 'PERL',
                'name' => 'Perl',
                'css' => 'perl',
            ],
            [
                'value' => 'PYTHON',
                'name' => 'Python',
                'css' => 'python',
            ],
            [
                'value' => 'RUBY',
                'name' => 'Ruby',
                'css' => 'ruby',
            ],
            [
                'value' => 'SQL',
                'name' => 'SQL',
                'css' => 'sql',
            ],
            [
                'value' => 'SCALA',
                'name' => 'Scala',
                'css' => 'scala',
            ],
        ];

        foreach ($langs as $lang) {
            $language = new ScriptLanguage();
            $language->setName($lang['name']);
            $language->setValue($lang['value']);
            $language->setCss($lang['css']);
            $this->dm()->persist($language);
        }

        $this->dm()->flush();
    }

    private function installSystem() {
        $system = new System();
        $system->setStrings([
            'write_something' => 'Write Something',
            'drag_images_here' => 'Drag images here',
            'place_images_here' => 'Place images here',
            'info' => 'Info',
            'original_width' => 'Original width',
            'original_height' => 'Original height',
            'language' => 'Language',
            'appearance' => 'Appearance',
            'opacity' => 'Opacity',
            'background_color' => 'Background color',
            'bounds' => 'Bounds',
            'width' => 'Width',
            'height' => 'Height',
            'slide' => 'Slide',
            'deck' => 'Presentation',
            'new' => 'New',
            'play' => 'Play',
            'textfield' => 'Textfield',
            'code' => 'Code',
            'shape' => 'Shape',
            'image' => 'Image',
            'table' => 'Table',
            'chart' => 'Chart',
            'formula' => 'Formula',
            'step_forward' => 'Step forward',
            'step_backward' => 'Step backward',
        ]);

        $this->dm()->persist($system);
        $this->dm()->flush();

    }

}
