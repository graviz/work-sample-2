<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;

class Environment {

    /**
     * @var ServiceHelper
     */
    public static $serviceHelper;

}