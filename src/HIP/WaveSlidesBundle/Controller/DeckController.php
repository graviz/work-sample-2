<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use DateTime;
use HIP\WaveSlidesBundle\DeckExportHelper;
use HIP\WaveSlidesCoreBundle\Common\FileHandler;
use HIP\WaveSlidesCoreBundle\Common\FileResponse;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Document\Slide;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TextElement;
use HIP\WaveSlidesCoreBundle\Widget\SlideElementWidgetFactory;
use HIP\WaveSlidesCoreBundle\ZMQ\ImageUploadedMessage;
use HIP\WaveSlidesCoreBundle\ZMQ\ZMQMessage;
use MongoId;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DeckController
 * @package HIP\WaveSlidesBundle\Controller
 *
 * @Sensio\Route("/deck")
 * @Sensio\Security("has_role('ROLE_USER')")
 */
class DeckController extends AbstractController {

    const DECK_NAME_PARAM = 'name';
    const SHARED_USER_PARAM = 'sharedUser';
    const SHARED_USERS_PARAM = 'sharedUsers';
    const DECK_FORMAT_PARAM = 'format';
    const CACHE_DURATION_IN_SECOND = 3600; // 1 hour

    const EXPORT_SERVLET = '/exportAsHTML';

    /**
     * Creates a Deck with a name and shared users.
     *
     * @Sensio\Route("/create", name="deck_create", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function createAction(Request $request) {
        $this->initAjax($request);

        $name = $this->getAjax(self::DECK_NAME_PARAM);
        $sharedUsersIds = $this->getAjax(self::SHARED_USERS_PARAM);
        $format = $this->getAjax(self::DECK_FORMAT_PARAM);

        $deck = $this->createDeck($name, $sharedUsersIds, $format);
        $this->dm()->persist($deck);

        $this->getUser()->getDecks()->add($deck);
        $this->dm()->flush();

        $deckBox = $this->renderView(
            'HIPWaveSlidesBundle:Dashboard:deck_box.html.twig',
            array('deck' => $deck, 'shared' => false)
        );

        return new Response($deckBox);
    }

    /**
     * Deletes a Deck. Removing it from the users deck list.
     *
     * @param $id
     * @return JsonResponse
     *
     * @Sensio\Route("/delete/{id}", name="deck_delete", options={"expose"=true})
     * @Sensio\Method("GET")
     */
    public function deleteAction($id) {
        /** @var Deck $deck */
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);

        if ($deck == null)
            return new JsonResponse(array('success' => false));

        if ($deck->getCreator()->getId() != $this->getUser()->getId())
            return new JsonResponse(array('success' => false));

        // Remove deck from shared users
        $users = $this->dm()
            ->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')
            ->findBy(array(WaveUser::SHARED_DECKS . '.$id' => new MongoId($deck->getId())));
        foreach ($users as $user) {
            /** @var WaveUser $user */
            $user->getSharedDecks()->removeElement($deck);
        }

        $this->getUser()->getDecks()->removeElement($deck);
        $this->dm()->remove($deck);

        $this->dm()->flush();

        return new JsonResponse(array('success' => true));
    }

    /**
     * Change the name of a deck.
     *
     * @param $id
     * @param Request $request
     * @return JsonResponse
     *
     * @Sensio\Route("/setName/{id}", name="deck_set_name", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function setNameAction(Request $request, $id) {
        $this->initAjax($request);

        /** @var Deck $deck */
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);

        if ($deck->getCreator()->getId() != $this->getUser()->getId())
            return new JsonResponse(array('success' => false));

        $deck->setName($this->getAjax(self::DECK_NAME_PARAM));
        $this->dm()->flush();

        return new JsonResponse(array('success' => true));
    }

    /**
     * Add an user to the shared users list of a deck
     *
     * @Sensio\Route("/addShare/{id}", name="deck_add_share", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function addShareAction(Request $request, $id) {
        $this->initAjax($request);

        /** @var Deck $deck */
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);

        if ($deck->getCreator()->getId() != $this->getUser()->getId())
            return new JsonResponse(array('success' => false));

        $userId = $this->getAjax(self::SHARED_USER_PARAM);

        /** @var WaveUser $user */
        $user = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')->find($userId);

        if ($user != null) {
            $exists = $user->getSharedDecks()->exists(function ($key, Deck $a) use ($deck) {
                return $a->getId() == $deck->getId();
            });

            if (!$exists)
                $user->getSharedDecks()->add($deck);

            $this->dm()->flush();

            return new JsonResponse(array('success' => true));
        }

        return new JsonResponse(array('success' => false));
    }

    /**
     * Add an user to the shared users list of a deck
     *
     * @Sensio\Route("/removeShare/{id}", name="deck_remove_share", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function removeShareAction(Request $request, $id) {
        $this->initAjax($request);

        /** @var Deck $deck */
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);

        if ($deck->getCreator()->getId() != $this->getUser()->getId())
            return new JsonResponse(array('success' => false));

        $userId = $this->getAjax(self::SHARED_USER_PARAM);

        /** @var WaveUser $user */
        $user = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')->find($userId);

        if ($user != null)
            $user->getSharedDecks()->removeElement($deck);

        $this->dm()->flush();

        return new JsonResponse(array('success' => true));
    }

    /**
     * {id}: Id of the ImageElement
     *
     * @Sensio\Route("/image/{id}/{filename}.{format}", name="deck_image")
     * @Sensio\Method("GET")
     */
    public function imageAction($id, $filename, $format) {
        /** @var ImageElement $imageElement */
        $imageElement = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:SlideElements\SlideElement')->find($id);
        $deck = $this->deckByElement($imageElement);

        $forbidden = true;

        if ($deck->getCreator()->getId() == $this->getUser()->getId())
            $forbidden = false;

        if ($this->getUser()->getSharedDecks()->contains($deck))
            $forbidden = false;

        if ($forbidden)
            return new Response('', Response::HTTP_FORBIDDEN);

        $response = new Response();
        $response->headers->set('Content-Type', $imageElement->getImage()->getContentType());
        $response->headers->set('Cache-Control', 'max-age=' . self::CACHE_DURATION_IN_SECOND . ', must-revalidate');
        $response->headers->set('Last-Modified', date('D, d M Y H:i:s', time()) . ' GMT');
        $response->headers->set('Expires', date('D, d M Y H:i:s', time() + self::CACHE_DURATION_IN_SECOND) . ' GMT');
        $response->headers->set('Pragma', 'public');
        $response->setContent($imageElement->getImage()->getFile()->getBytes());
        return $response;
    }

    /**
     * Uploads an image to a specific slide.
     * After the upload notifies the other users connected with the web socket server.
     *
     * @Sensio\Route("/upload", name="deck_upload", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function uploadAction(Request $request) {
        /** @var $upload \Symfony\Component\HttpFoundation\File\UploadedFile */
        $upload = $request->files->get('file');

        $jsonData = json_decode($request->request->get('json'), true);
        $deckId = $jsonData['deck'];
        $slideId = $jsonData['slide'];
        $senderConId = $jsonData['sender'];

        $deck = $this->getUser()->getDeck($deckId);

        if ($deck == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        $slide = $deck->getSlideById($slideId);

        if ($slide == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        /** @var ZMQMessage $zmqMessage */
        $zmqMessage = null;
        if (FileHandler::isImage($upload)) {
            $image = FileHandler::processImage($upload);

            $imageElement = new ImageElement();
            $imageElement->setImage($image);

            $this->dm()->persist($imageElement);
            $this->dm()->flush($imageElement);

            $slide->getElements()->add($imageElement);

            /** @var ImageUploadedMessage $zmqMessage */
            $zmqMessage = new ImageUploadedMessage();
            $zmqMessage->type = ImageUploadedMessage::Role;
            $zmqMessage->slideId = $slideId;
            $zmqMessage->createdView = SlideElementWidgetFactory::render($imageElement);
            $zmqMessage->sender = $senderConId;

        } else {
            return new Response('', Response::HTTP_FORBIDDEN);
        }

        $this->dm()->flush();
        $zmqMessage->send();

        return new JsonResponse(['success' => true]);
    }

    /**
     * Exports a deck as a zip file.
     *
     * @Sensio\Route("/export/{id}", name="deck_export_html", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @param $id
     * @throws \Doctrine\ODM\MongoDB\LockException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function exportHTMLAction($id) {
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);
        $deckFileInfo = DeckExportHelper::export($deck);

        return FileResponse::create(
            $deckFileInfo->pathname,
            $deckFileInfo->filename,
            $deckFileInfo->contentType
        );
    }

    /**
     * Returns a rendered html widget with all shared users of this deck.
     *
     * @Sensio\Route("/shares/{id}", name="deck_shares", options={"expose"=true})
     * @Sensio\Method("GET")
     */
    public function sharesAction($id) {
        $deck = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($id);

        if ($deck == null)
            return new JsonResponse(array('success' => false));

        if ($deck->getCreator()->getId() != $this->getUser()->getId())
            return new JsonResponse(array('success' => false));

        $users = $this->dm()
            ->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')
            ->findBy(array(WaveUser::SHARED_DECKS . '.$id' => new MongoId($deck->getId())));

        return $this->render('@HIPWaveSlides/Dashboard/share_user_results.twig', array('users' => $users));
    }

    // ---------------------------------------------------------------------------------

    /**
     * @param string $name
     * @param string $format
     * @param array $sharedUsersIds
     * @return Deck
     */
    private function createDeck($name, array $sharedUsersIds, $format) {
        $slide = new Slide();
        $slide->setIndex(0);
        $slide->getElements()->add(new TextElement());

        $deck = new Deck();
        $deck->setName($name);
        $deck->setCreator($this->getUser());
        $deck->setCreated(new DateTime());
        $deck->getSlides()->add($slide);
        $deck->setFormat($format);

        foreach ($sharedUsersIds as $id) {
            /** @var WaveUser $user */
            $user = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')->find($id);
            $user->getSharedDecks()->add($deck);
        }

        return $deck;
    }

    private function deckByElement(SlideElement $element) {
        /** @var Slide $slide */
        $slide = $this->dm()
            ->getRepository('HIPWaveSlidesCoreBundle:Slide')
            ->findOneBy(array(
                Slide::Elements . '.$id' => new MongoId($element->getId()),
            ));

        if ($slide == null)
            return null;

        /** @var Deck $deck */
        $deck = $this->dm()
            ->getRepository('HIPWaveSlidesCoreBundle:Deck')
            ->findOneBy(array(
                Deck::Slides . '.$id' => new MongoId($slide->getId()),
            ));

        return $deck;
    }

}

interface DeckExportResponse {
    const FILE_PATH = 'filepath';
    const FILE_NAME = 'filename';
    const CONTENT_TYPE = 'contentType';
}