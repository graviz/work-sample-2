<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Document\System;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Sensio\Route("/editor")
 * @Sensio\Security("has_role('ROLE_USER')")
 */
class EditorController extends AbstractController {

    /**
     * Starts the WaveEditor App.
     * @Sensio\Route("/{id}", name="editor_index", options={"expose"=true})
     * @Sensio\Method("GET")
     * @Sensio\Template()
     * @param string $id
     * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function indexAction($id) {
        /** @var Deck $deck */
        $deck = $this->getUser()->getDeck($id);

        if ($deck == null)
            return new RedirectResponse($this->generateUrl('dashboard_index'));

        return array(
            'deck_id' => $deck->getId(),
            'ws_ip' => $this->wsIP(),
            'languages' => $this->dm()->getRepository('HIPWaveSlidesCoreBundle:SlideElements\ScriptLanguage')->findAll(),
            'strings' => System::instance()->getStrings()
        );

    }

    private function wsIP() {
        return ServiceHelper::parameter('ws_ip') . ':' . ServiceHelper::parameter('ws_port');
    }

} 