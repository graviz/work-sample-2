<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class AbstractController extends Controller {

    /**
     * Post field name containing json data.
     */
    const AJAX = 'json';

    /**
     * @var array
     */
    private $ajaxData;

    /**
     * @return DocumentManager
     */
    public function dm() {
        return $this->get('doctrine_mongodb')->getManager();
    }

    /**
     * For better IDE typechecking
     * @return WaveUser
     */
    public function getUser() {
        return parent::getUser();
    }

    /**
     * Ajax data is stored in the "ajax" parameter of the post data
     * Returns the complete ajax data as key value array
     * @param string $key
     * @return mixed
     * @throws Exception
     */
    protected function getAjax($key) {
        if ($this->ajaxData == null)
            throw new Exception('Ajax data not initialized. Did you forgot to call initAjax(Request)');
        return $this->ajaxData[$key];
    }

    protected function initAjax(Request $request) {
        $this->ajaxData = json_decode($request->request->get(self::AJAX), true);
    }

}