<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Sensio\Security("has_role('ROLE_USER')")
 * @Sensio\Route("/user")
 */
class UserController extends AbstractController {

    /**
     * Search for users by an username or email.
     * @Sensio\Route("/search/{search}", name="user_search", options={"expose"=true})
     * @Sensio\Method("GET")
     * @Sensio\Template()
     */
    public function searchAction($search) {

        $query = $this->dm()
            ->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')
            ->createQueryBuilder();

        $expressionUsername = $query->expr()->field(WaveUser::USERNAME)->equals(new \MongoRegex('/' . $search . '/i'));
        $expressionEmail = $query->expr()->field(WaveUser::EMAIL)->equals(new \MongoRegex('/' . $search . '/i'));
        $query->addOr($expressionUsername);
        $query->addOr($expressionEmail);
        $users = $query->getQuery()->execute();

        $html = $this->renderView('@HIPWaveSlides/Dashboard/share_user_results.twig', array('users' => $users));
        $noResults = $html == '' ? true : false;

        return new JsonResponse(array(
            'html' => $html,
            'noResults' => $noResults,
        ));
    }

} 