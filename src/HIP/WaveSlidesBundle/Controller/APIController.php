<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesCoreBundle\Document\Account\Role;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Widget\SlideWidget;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class APIController
 * @package HIP\WaveSlidesBundle\Controller
 *
 * The API Controller will be used for 3rd party components. For example an mobile App or other client.
 *
 * @Sensio\Route("/api")
 */
class APIController extends AbstractController {

    const WAVESLIDES_FIREWALL = 'waveslides';

    const USERNAME_PARAM = 'username';
    const PASSWORD_PARAM = 'password';

    /**
     * Manually logs in a user via a POST request against the Symfony Firewall Security mechanism.
     *
     * @Sensio\Route("/login", name="api_login")
     * @Sensio\Method("POST")
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function loginAction(Request $request) {
        $username = $request->get(self::USERNAME_PARAM);
        $password = $request->get(self::PASSWORD_PARAM);

        $repo = $this->dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser');

        /** @var WaveUser $user */
        $user = $repo->findOneBy(array(WaveUser::USERNAME => $username));

        if (!$user)
            return new JsonResponse(array('success' => false));

        $encoder = $this->get('security.encoder_factory')->getEncoder($user);
        $password = $encoder->encodePassword($password, $user->getSalt());

        if ($user->getPassword() != $password)
            return new JsonResponse(array('success' => false));

        $token = new UsernamePasswordToken($user, $password, self::WAVESLIDES_FIREWALL, $user->getRoles());
        $this->get('security.context')->setToken($token); //now the user is logged in

        $event = new InteractiveLoginEvent($request, $token);
        $this->get('event_dispatcher')->dispatch('security.interactive_login', $event);

        return new JsonResponse(array('success' => true));
    }

    /**
     * Returns information about the decks of a user in json form.
     *
     * @Sensio\Route("/decks", name="api_decks")
     * @Sensio\Method("GET")
     *
     * @return JsonResponse
     */
    public function decksAction() {
        if (!$this->isAuth())
            return new Response('', Response::HTTP_FORBIDDEN);

        /** @var WaveUser $user */
        $user = $this->getUser();

        $response = array();

        foreach ($user->getDecks() as $deck)
            $response[] = $this->deckToJson($deck, false);

        foreach ($user->getSharedDecks() as $deck)
            $response[] = $this->deckToJson($deck, true);

        return new JsonResponse($response);
    }

    /**
     * Returns a rendered html preview of the first slide of a deck.
     *
     * @Sensio\Route("/deck/preview/{id}", name="api_slide_preview", options={"expose"=true})
     * @Sensio\Method("GET")
     *
     * @param string $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function previewAction($id) {
        if (!$this->isAuth())
            return new Response('', Response::HTTP_FORBIDDEN);

        // Due to the fact that this action is called multiple times at the same time
        // by the same user we close the session file to prevent i/o blocking.
        session_write_close();

        $deck = $this->getUser()->getDeck($id);

        if ($deck == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        return new Response(SlideWidget::render($slides->first()));
    }

    /**
     * Returns a rendered html presentation of the deck.
     *
     * @Sensio\Route("/slides/{id}", name="api_slides", options={"expose"=true})
     * @Sensio\Method("GET")
     * @param string $id ID of the deck
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function slidesAction($id) {
        if (!$this->isAuth())
            return new Response('', Response::HTTP_FORBIDDEN);

        $deck = $this->getUser()->getDeck($id);

        if ($deck == null)
            return new Response('', Response::HTTP_NOT_FOUND);

        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        return new JsonResponse([
            Deck::Slides => SlideWidget::renderAll($slides),
            Deck::Format => $deck->getFormat()
        ]);
    }

    // -----------------------------------------------------------------------------

    private function deckToJson(Deck $deck, $shared) {
        return array(
            Deck::ID => $deck->getId(),
            Deck::Name => $deck->getName(),
            Deck::Created => $deck->getCreated()->getTimestamp(),
            Deck::Format => $deck->getFormat(),
            'shared' => $shared
        );
    }

    /**
     * @return bool
     */
    private function isAuth() {
        return $this->get('security.context')->isGranted(Role::USER);
    }

}
