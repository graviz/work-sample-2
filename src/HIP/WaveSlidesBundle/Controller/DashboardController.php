<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use HIP\WaveSlidesCoreBundle\Document\Account\DashboardSettings;
use HIP\WaveSlidesCoreBundle\Document\Account\SortOption;
use HIP\WaveSlidesCoreBundle\Document\Account\ViewOption;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DashboardController
 * @package AEE\RenewsMobilBundle\Controller
 * @Sensio\Route("/dashboard")
 * @Sensio\Security("has_role('ROLE_USER')")
 */
class DashboardController extends AbstractController {

    const UPDATE_OPTION = 'updateOption';
    const UPDATE_VALUE = 'updateValue';

    /**
     * Renders the dashboard of all decks and shared decks.
     * @Sensio\Route("/", name="dashboard_index")
     * @Sensio\Method("GET")
     * @Sensio\Template()
     */
    public function indexAction() {
        $decks = $this->getUser()->getDecks()->toArray();
        $sharedDecks = $this->getUser()->getSharedDecks()->toArray();

        $decks = array_merge($decks, $sharedDecks);

        if ($this->getUser()->getDashboardSettings()->getSortOption() == SortOption::SORT_DATE)
            $this->sortDecksByCreatedDate($decks);
        else if ($this->getUser()->getDashboardSettings()->getSortOption() == SortOption::SORT_NAME)
            $this->sortDecksByName($decks);

        return array(
            'decks' => $this->renderDeckSection($decks),
        );
    }

    /**
     * Renders the dashboard of all decks and shared decks.
     * @Sensio\Route("/updateSettings", name="dashboard_update_settings", options={"expose"=true})
     * @Sensio\Method("POST")
     */
    public function updateSettingsAction(Request $request) {
        $this->initAjax($request);
        $updateOption = $this->getAjax(self::UPDATE_OPTION);
        $updateValue = $this->getAjax(self::UPDATE_VALUE);

        switch ($updateOption) {
            case DashboardSettings::SORT_OPTION:
                $this->getUser()->getDashboardSettings()->setSortOption($updateValue);
                break;
            case DashboardSettings::VIEW_OPTION:
                $this->getUser()->getDashboardSettings()->setViewOption($updateValue);
                break;
        }

        $this->dm()->flush();

        return new JsonResponse(array('success' => true));
    }

    //----------------------------------------------------------------------------------


    /**
     * @param array $decks
     * @return string
     */
    private function renderDeckSection($decks) {
        $deckRow = '';
        $columnSize = 4;

        $deckRow .= $this->renderView('@HIPWaveSlides/Dashboard/deck_add_button.html.twig');
        $counter = 1;

        $deckSection = '';

        foreach ($decks as $deck) {
            /** @var Deck $deck */
            $counter++;

            $visible = true;

            switch ($this->getUser()->getDashboardSettings()->getViewOption()) {
                case ViewOption::VIEW_MY:
                    $visible = $this->getUser()->getId() == $deck->getCreator()->getId();
                    break;
                case ViewOption::VIEW_SHARED:
                    $visible = $this->getUser()->getId() != $deck->getCreator()->getId();
                    break;
            }

            /** @var Deck $deck */
            $deckRow .= $this->renderView('HIPWaveSlidesBundle:Dashboard:deck_box.html.twig', array(
                'deck' => $deck,
                'shared' => $deck->getCreator()->getId() != $this->getUser()->getId(),
                'visible' => $visible
            ));

            if ($counter == $columnSize) {
                $deckSection .= $this->renderDeckRow($deckRow);
                $deckRow = '';
                $counter = 0;
            }
        }

        if ($counter != 0)
            $deckSection .= $this->renderDeckRow($deckRow);

        return $deckSection;
    }

    private function renderDeckRow($deckRow) {
        return $this->renderView('HIPWaveSlidesBundle:Dashboard:deck_row.html.twig', array(
            'deck_row' => $deckRow
        ));
    }

    private function sortDecksByName(array &$decks) {
        uasort($decks, function (Deck $deckA, Deck $deckB) {
            return strtolower($deckA->getName()) > strtolower($deckB->getName());
        });
    }

    private function sortDecksByCreatedDate(array &$decks) {
        uasort($decks, function (Deck $deckA, Deck $deckB) {
            return $deckA->getCreated()->getTimestamp() < $deckB->getCreated()->getTimestamp();
        });
    }

}
