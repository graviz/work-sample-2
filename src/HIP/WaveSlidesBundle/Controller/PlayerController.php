<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use HIP\WaveSlidesCoreBundle\Document\Deck;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * @Sensio\Security("has_role('ROLE_USER')")
 * @Sensio\Route("/player")
 */
class PlayerController extends AbstractController {

    /**
     * Starts th WavePlayer App.
     * @Sensio\Route("/{id}", name="player_index", options={"expose"=true})
     * @Sensio\Method("GET")
     * @Sensio\Template()
     */
    public function indexAction($id) {
        /** @var Deck $deck */
        $deck = $this->getUser()->getDeck($id);

        if ($deck == null)
            return new RedirectResponse($this->generateUrl('dashboard_index'));

        return array('deck' => $deck);
    }

} 