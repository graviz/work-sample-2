<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use \FOS\UserBundle\Controller\SecurityController as BaseSecurityController;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends BaseSecurityController {

    public function loginAction(Request $request) {
        // IS_AUTHENTICATED_FULLY also implies IS_AUTHENTICATED_REMEMBERED, but IS_AUTHENTICATED_ANONYMOUSLY doesn't
        if ($this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            return new RedirectResponse($this->container->get('router')->generate('dashboard_index'));

        return parent::loginAction($request);
    }

}