<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle;

use HIP\WaveSlidesCoreBundle\Common\FileUtil;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\ZipUtil;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Widget\SlideWidget;
use RecursiveIteratorIterator;
use SplFileInfo;

//TODO:: clear old cache directories?
class DeckExportHelper {

    const TEMPLATE_DIR = '@HIPWaveSlidesBundle/Resources/assets/playerTemplate';
    const TEMP_DIR = 'temp';
    const ASSETS_IMAGES_DIR = 'assets/images';
    const DATE_FORMAT = 'd_m_Y_H_i_s';
    const INDEX_HTML = 'index.html';
    const IMAGE_PATH_PARAM = 'slide_image_path_offline';

    /**
     * @var string
     */
    protected $rootPath;

    /**
     * @var string
     */
    protected $tempDir;

    protected $assetsImageDir;

    /**
     * @var Deck
     */
    protected $deck;

    public static function export(Deck $deck) {
        $exporter = new DeckExportHelper($deck);
        return $exporter->exportFile();
    }

    private function __construct(Deck $deck) {
        $this->deck = $deck;
        $this->rootPath = ServiceHelper::locator()->locate(self::TEMPLATE_DIR);
        $this->tempDir = $this->deckTempDir($deck);
        $this->assetsImageDir = $this->tempDir . '/' . self::ASSETS_IMAGES_DIR;
    }

    protected function exportFile() {
        /** 1. Try cache */
        if (FileUtil::fileExist($this->getPathname()))
            return $this->buildFileInfo();

        FileUtil::mkdir($this->tempDir);
        FileUtil::copyRecursive($this->rootPath, $this->tempDir);
        FileUtil::createFile($this->tempDir . '/' . self::INDEX_HTML, $this->renderTemplate());

        ZipUtil::zip($this->tempDir, $this->getPathname());

        $this->clean();

        return $this->buildFileInfo();
    }

    protected function buildFileInfo() {
        $pathname = $this->getPathname();
        $filename = $this->deckFilename();
        $contentType = FileUtil::mime($pathname);

        return new DeckFileInfo($contentType, $filename, $pathname);
    }

    protected function getPathname() {
        return $this->tempDir . '/' . $this->deckFilename();
    }

    /**
     * @return string
     */
    protected function deckTempDir() {
        return $this->tempRootDir() . '/' . $this->deck->getId() . '_' . $this->deck->getLastUpdated()->getTimestamp();
    }

    /**
     * @return string
     */
    protected function tempRootDir() {
        return ServiceHelper::kernel()->getRootDir() . '/../' . self::TEMP_DIR;
    }

    /**
     * @return string
     */
    protected function deckFilename() {
        return $this->deck->getAlias() . '_' . $this->deck->getLastUpdated()->format(self::DATE_FORMAT) . '.zip';
    }

    /**
     * @return string
     */
    protected function renderTemplate() {
        $slides = SlideWidget::renderAll(
            $this->deck->getSlides(),
            true,
            ServiceHelper::parameter(self::IMAGE_PATH_PARAM),
            function (SlideElement $element) {
                if ($element instanceof ImageElement)
                    $this->saveImage($element);
            }
        );

        return ServiceHelper::templating()->render('@HIPWaveSlides/Player/offline_index.html.twig', [
            'deck' => $this->deck,
            'slides' => $slides
        ]);
    }

    protected function saveImage(ImageElement $imageElement) {
        $filename = $imageElement->getImage()->getFilename();
        $imageDir = $this->assetsImageDir . '/' . $imageElement->getId();
        FileUtil::mkdir($imageDir);
        $imageElement->getImage()->getFile()->write($imageDir . '/' . $filename);
    }

    private function clean() {
        FileUtil::traverseDirectoryRecursive(
            $this->tempDir,
            function (SplFileInfo $item) {
                if ($item->isDir())
                    rmdir($item->getRealPath());
                else if ($item->getExtension() != FileUtil::ZIP)
                    unlink($item->getRealPath());
            },
            RecursiveIteratorIterator::CHILD_FIRST
        );
    }

}

class DeckFileInfo {

    public $pathname;
    public $filename;
    public $contentType;

    function __construct($contentType, $filename, $pathname) {
        $this->contentType = $contentType;
        $this->filename = $filename;
        $this->pathname = $pathname;
    }
}