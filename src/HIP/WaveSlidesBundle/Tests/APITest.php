<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class APITest extends WebTestCase {

    const LOGIN_ROUTE = '/api/login';
    const DECKS_ROUTE = '/api/decks';
    const DECK_PREVIEW_ROUTE = '/api/deck/preview/';
    const SLIDE_ROUTE = '/api/slides/';

    public function testAPI() {
        $client = static::createClient();

        // 1. Test Login
        echo '1. Test Login' . PHP_EOL . PHP_EOL;
        $client->request(
            'POST',
            self::LOGIN_ROUTE,
            ['username' => 'user1', 'password' => 'user1']
        );

        $feedback = json_decode($client->getResponse()->getContent(), true);

        $this->assertTrue($feedback['success']);
        $this->assertTrue($client->getResponse()->getStatusCode() == 200);

        // 2. Test Decks
        echo '2. Test Decks' . PHP_EOL . PHP_EOL;
        $client->request(
            'GET',
            self::DECKS_ROUTE
        );

        $this->assertTrue($client->getResponse()->getStatusCode() == 200);
        $decks = json_decode($client->getResponse()->getContent(), true);

        $deckId = $decks[0]['id'];

        // 3. Test deck preview
        echo '3. Test deck preview' . PHP_EOL . PHP_EOL;
        $client->request(
            'GET',
            self::DECK_PREVIEW_ROUTE . $deckId
        );

        $this->assertTrue($client->getResponse()->getStatusCode() == 200);
        print_r($client->getResponse()->getContent());
        echo PHP_EOL;
        echo PHP_EOL;

        // 4. Test slide data
        echo '4. Test slide data' . PHP_EOL . PHP_EOL;
        $client->request(
            'GET',
            self::SLIDE_ROUTE . $deckId
        );

        $this->assertTrue($client->getResponse()->getStatusCode() == 200);
        print_r(json_decode($client->getResponse()->getContent(), true));
    }


}