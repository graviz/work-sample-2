<?php

namespace HIP\WaveSlidesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class HIPWaveSlidesBundle extends Bundle {

    public function __construct() {

    }

    public function getParent() {
        return 'FOSUserBundle';
    }
}

