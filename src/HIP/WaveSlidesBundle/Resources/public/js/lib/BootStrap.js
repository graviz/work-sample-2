Array.prototype.addSorted = function (object, keyOf) {

    var insertIndex = -1;
    var length = this.length;
    for (var i = 0; i < length; i++) {
        var value = keyOf(this[i]);
        if (keyOf(object) < value) {
            insertIndex = i;
            break;
        }
    }

    if (insertIndex == -1)
        this.splice(this.length, 0, object);
    else
        this.splice(insertIndex, 0, object);

};

Array.prototype.remove = function (object) {
    var index = this.indexOf(object);
    if (index != -1)
        this.splice(this.indexOf(object), 1);
    else
        console.error("object not found in array");
};

/**
 * Source: http://stackoverflow.com/questions/12920225/text-selection-in-divcontenteditable-when-double-click
 * @param evt
 * @returns {*}
 */
function getCaretRange(evt) {
    var range, x = evt.clientX, y = evt.clientY;

    // Try the simple IE way first
    if (document.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToPoint(x, y);
    }

    else if (typeof document.createRange != "undefined") {
        // Try Mozilla's rangeOffset and rangeParent properties, which are exactly what we want

        if (typeof evt.rangeParent != "undefined") {
            range = document.createRange();
            range.setStart(evt.rangeParent, evt.rangeOffset);
            range.collapse(true);
        }

        // Try the standards-based way next
        else if (document.caretPositionFromPoint) {
            var pos = document.caretPositionFromPoint(x, y);
            range = document.createRange();
            range.setStart(pos.offsetNode, pos.offset);
            range.collapse(true);
        }

        // Next, the WebKit way
        else if (document.caretRangeFromPoint) {
            range = document.caretRangeFromPoint(x, y);
        }
    }

    return range;
}

/**
 * Source: http://stackoverflow.com/questions/12920225/text-selection-in-divcontenteditable-when-double-click
 * @param range
 */
function selectRange(range) {
    if (range) {
        if (typeof range.select != "undefined") {
            range.select();
        } else if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            sel.removeAllRanges();
            sel.addRange(range);
        }
    }
}

/**
 * Source: http://stackoverflow.com/questions/2920150/insert-text-at-cursor-in-a-content-editable-div
 * @param text
 */
function insertTextAtCursor(text) {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            range.insertNode(document.createTextNode(text));
        }
    } else if (document.selection && document.selection.createRange) {
        document.selection.createRange().text = text;
    }
}


/**
 * Source: http://stackoverflow.com/questions/2920150/insert-text-at-cursor-in-a-content-editable-div
 * @param text
 */
function saveSelection() {
    if (window.getSelection) {
        var sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            return sel.getRangeAt(0);
        }
    } else if (document.selection && document.selection.createRange) {
        return document.selection.createRange();
    }
    return null;
}