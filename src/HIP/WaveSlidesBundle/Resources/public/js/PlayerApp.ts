/// <reference path="definitions/references.d.ts" />

import net = require("src/net");
import ui = require("src/ui");
import watcher = require("src/watcher");

export class PlayerApp implements watcher.KeyboardObserver {

    private deckWrapper:JQuery = $(".deckWrapper");
    private slideContainer:JQuery;
    private loader:JQuery = $(".waveloader");
    private currentSlide:JQuery;
    private controlsElement:JQuery = $(".controls");
    private controlTimeout;
    private controlNextButton:JQuery;
    private controlPrevButton:JQuery;
    private controlFullscreenButton:JQuery;
    private dimensions:Dimension;

    private static SLIDES_DATA_ROUTE:string = "api_slides";

    constructor() {
        this.slideContainer = this.deckWrapper.find(".slides");

        watcher.KeyboardWatcher.init();
        watcher.KeyboardWatcher.register(this);

        /**
         * Checking if SlideContainer is empty because this file is used for the offline player too.
         */
        if (this.slideContainer.is(':empty')) {
            var url = Routing.generate(PlayerApp.SLIDES_DATA_ROUTE, {id: DECK_ID});
            net.Ajax.getJson(url, (response:SlideDataResponse)=> {
                this.dimensions = ui.UISlideContainer.dimension(response.format);
                this.loader.hide();
                this.slideContainer.append(response.slides);
                this.initPlayer();
                $("html, body").removeAttr("style");
            });
        } else {
            this.dimensions = ui.UISlideContainer.dimension(this.slideContainer.data("format"));
            this.initPlayer();
        }

    }

    /**
     * Navigate to the next or previous slide.
     */
    public onKeyDown(keycode:Number, event:JQueryKeyEventObject):void {
        if (keycode == watcher.KeyboardWatcher.ARROW_RIGHT)
            this.nextSlide();
        else if (keycode == watcher.KeyboardWatcher.ARROW_LEFT)
            this.prevSlide();
    }

    public onKeyUp(keycode:Number, event:JQueryKeyEventObject):void {
    }

    /**
     * Reset the control timer. When the timer expired, hide the navigation control element.
     */
    private resetControlTimer():void {
        window.clearTimeout(this.controlTimeout);
        this.controlTimeout = setTimeout(() => {
            if (!this.controlsElement.is(":hover"))
                this.hideControls()
        }, 2000);
    }

    /**
     * Hides the navigation control element.
     */
    private hideControls():void {
        this.controlsElement.fadeOut();
        this.deckWrapper.addClass("noCursor");
    }

    /**
     * Shows the navigation control element.
     */
    private showControls():void {
        this.controlsElement.show()
        this.deckWrapper.removeClass("noCursor");
    }

    /**
     * Displays the next slide if available.
     */
    private nextSlide():void {
        var next = this.currentSlide.next();

        if (next.hasClass("uiSlide")) {
            next.show();
            this.currentSlide.hide();
            this.currentSlide = next;
        }
    }

    /**
     * Displays the previous slide if available.
     */
    private prevSlide():void {
        var prev = this.currentSlide.prev();

        if (prev.hasClass("uiSlide")) {
            prev.show();
            this.currentSlide.hide();
            this.currentSlide = prev;
        }
    }

    /**
     * Init the player. Inserts and renders the slides.
     * Adjust the zoom scale when browser window is resized.
     * Adds a listener to the document to display the navigation control element on mouse move.
     */
    private initPlayer():void {
        this.slideContainer.find(".uiSlide").each((i:number, e:Element)=> {
            this.renderSlide($(e));
            if (i == 0)
                $(e).show();

            $(e).width(this.dimensions.width);
            $(e).height(this.dimensions.height);
        });

        $(window).resize(() => {
            this.adjustSize();
        });

        this.currentSlide = this.slideContainer.find(".uiSlide").first();
        this.currentSlide.show();
        this.deckWrapper.show();
        this.controlsElement.show();
        $("body").removeAttr("style");

        this.controlNextButton = this.controlsElement.find(".button.next");
        this.controlNextButton.click(()=> {
            this.nextSlide();
        });

        this.controlPrevButton = this.controlsElement.find(".button.prev");
        this.controlPrevButton.click(()=> {
            this.prevSlide();
        });

        this.controlFullscreenButton = this.controlsElement.find(".button.fullscreen");
        this.controlFullscreenButton.click(()=> {
            if (this.controlFullscreenButton.hasClass("maximized"))
                ui.UIKit.exitFullScreen();
            else
                ui.UIKit.requestFullScreen($("body")[0]);

            this.controlFullscreenButton.toggleClass("maximized");
            this.adjustSize();
        });

        $(document).mousemove(() => {
            this.showControls();
            this.resetControlTimer();
        });

        this.resetControlTimer();
        this.adjustSize();
    }

    /**
     * Adjust the slides container element to fit to the browser window size by scaling it.
     */
    private adjustSize():void {
        var scale;
        var newHeight;
        var newWidth;
        var paddingTop = 0;

        if (this.dimensions.width / this.dimensions.height > $(window).width() / $(window).height()) {
            newHeight = $(window).width() / (this.dimensions.width / this.dimensions.height);
            newWidth = $(window).width();
            scale = newHeight / this.dimensions.height;
        } else {
            scale = $(window).height() / this.dimensions.height;
            newHeight = scale * this.dimensions.height;
            newWidth = scale * this.dimensions.width;
        }

        this.slideContainer.css({
            width: newWidth,
            height: newHeight
        });

        ui.UIKit.transformScale(this.slideContainer.find(".uiSlide"), scale, scale, 0, 0);

        if (this.dimensions.width / this.dimensions.height > $(window).width() / $(window).height())
            paddingTop = Math.round(($(window).height() - this.slideContainer.height()) / 2);

        this.slideContainer.css({
            paddingTop: paddingTop
        });

        this.adjustControlElementPosition();
    }

    /**
     * Centers the navigation control element to the browser window.
     */
    private adjustControlElementPosition():void {
        this.controlsElement.css({
            left: Math.round(($(window).width() / 2) - (this.controlsElement.width() / 2)),
            top: $(window).height() - 50
        });
    }

    /**
     * Uses highlightjs to render ScriptView's.
     */
    private renderSlide(slideElement:JQuery):void {
        slideElement.find("." + ui.UIKit.UIScriptView + " .content").each((i:number, e:Element) => {
            hljs.highlightBlock(e);
        });
    }

}

interface SlideDataResponse {
    slides:string;
    format:string;
}