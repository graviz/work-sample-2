interface ViewObject {
    type:string;
    id:string;
    styles:Object;
}

interface SlideElementViewObject extends ViewObject {
    bounds: Bounds;
    z: number;
    alpha:number;
}

interface AbstractTextViewObject extends SlideElementViewObject {
    text:string;
}

interface ScriptViewObject extends AbstractTextViewObject {
    langName:string;
    langValue:string;
    langCss:string;
}

interface TextViewObject extends AbstractTextViewObject {
}

interface ImageViewObject extends SlideElementViewObject {
    imageRef:string;
}

interface TableViewObject extends SlideElementViewObject {
    rows: TableRow[];
}

interface TableRow {
    cells: TableCell[];
}

interface TableCell {
    value:string;
    styles:Object;
    dimension:Dimension;
}

interface Border {
    width: number;
    color: RGBA;
    style:string;
}

interface SlideObject extends ViewObject {
    index:number
}

interface Bounds {
    x:number;
    y:number;
    width:number;
    height:number;
}

interface StyleComponent {
    type:string;
}

interface BackgroundColorComponent extends StyleComponent {
    color: RGBA;
}

interface FontColorComponent extends StyleComponent {
    color: RGBA
}

interface FontSizeComponent extends StyleComponent {
    fontSize: number;
}

interface BorderComponent extends StyleComponent {
    width: number;
    color: RGBA;
    style: string;
}

interface TextObject {
    text?: string;
    size?: number;
    align?: string;
    color?: RGBA;
    font?: string;
    script?: string; /* super, sub*/
    bold?: boolean;
    italic?: boolean;
    strikeout?:boolean;
    underline?:boolean;
}