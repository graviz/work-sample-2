interface SecureRequestData {
    session:string;
    user:string;
    type:string;
}

interface DeckRequestData extends SecureRequestData {
    deck:string;
}


interface ImageUploadRequestData extends SecureRequestData {
    sender:string;
    deck:string;
    slide:string;
}

