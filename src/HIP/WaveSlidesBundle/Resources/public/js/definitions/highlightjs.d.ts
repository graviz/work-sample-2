// Type definitions for highlight.js
// Project: https://github.com/isagalaev/highlight.js
// Definitions by: Niklas Mollenhauer <https://github.com/nikeee/>
// Definitions: https://github.com/borisyankov/DefinitelyTyped

interface HLJS {
    LANGUAGES:{ [name: string] : any;
    };

    configure(object:any);

    blockText(block:Node, ignoreNewLines:boolean):string;

    blockLanguage(block:Node):string;

    highlight(language_name:string, value:string):IHighlightResult

    highlightAuto(text:string):IHighlightResult

    fixMarkup(value:string, tabReplace:boolean, useBR:boolean):string;

    highlightBlock(block:Node, tabReplace?:boolean, useBR?:boolean):void;

    initHighlighting():void;

    initHighlightingOnLoad():void;

    tabReplace:string;

    // Common regexps
    IDENT_RE:string;
    UNDERSCORE_IDENT_RE:string;
    NUMBER_RE:string;
    C_NUMBER_RE:string;
    BINARY_NUMBER_RE:string;
    RE_STARTERS_RE:string;

    // Common modes
    BACKSLASH_ESCAPE:IMode;
    APOS_STRING_MODE:IMode;
    QUOTE_STRING_MODE:IMode;
    C_LINE_COMMENT_MODE:IMode;
    C_BLOCK_COMMENT_MODE:IMode;
    HASH_COMMENT_MODE:IMode;
    NUMBER_MODE:IMode;
    C_NUMBER_MODE:IMode;
    BINARY_NUMBER_MODE:IMode;
}
interface IHighlightResult {
    relevance: number;
    keyword_count: number;
    value: string;
}

interface IAutoHighlightResult extends IHighlightResult {
    language: string;
    second_best?: IAutoHighlightResult;
}


// Reference:
// https://github.com/isagalaev/highlight.js/blob/master/docs/reference.rst
interface IMode {
    className?: string;
    begin: string;
    end?: string;
    beginWithKeyword?: boolean;
    endsWithParent?: boolean;
    lexems?: string;
    keywords?: Object;
    illegal?: string;
    excludeBegin?: boolean;
    excludeEnd?: boolean;
    returnBegin?: boolean;
    returnEnd?: boolean;
    contains?: IMode[];
    starts?: string;
    subLanguage?: string;
    relevance?: number;
}

declare var hljs:HLJS;
