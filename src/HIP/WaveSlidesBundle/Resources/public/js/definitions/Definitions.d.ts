declare var DECK_ID;
declare var SESSION_ID;
declare var CON_ID;
declare var USER_ID;
declare var WS_URL;

declare function execCommandOnElement(arg:any, arg2:any, arg3:any);
declare function getCaretRange(evt:any);
declare function selectRange(range:any);
declare function insertTextAtCursor(text:string);
declare function saveSelection():any;

interface Array<T> {
    addSorted(object, keyOf);
    remove(object);
}

interface Document extends MSDocumentExtensions {
    caretPositionFromPoint(x:any, y:any);
    caretRangeFromPoint(x:any, y:any);
    addEventListener(type:string, listener:(ev:any) => any, useCapture?:boolean): void;
}

interface Document {
    exitFullscreen();
    mozCancelFullScreen();
    webkitExitFullscreen();
}

interface HTMLElement {
    createTextRange();
    checked:boolean;
}

interface Element{

}

interface RGBA extends RGB {
    a: number;
}

interface RGB {
    r: number;
    g: number;
    b: number;
}

interface Dimension {
    width: number;
    height: number;
}

declare class Routing {
    public static generate(route_id:string, params?:Object, absolute?:boolean):string;
}