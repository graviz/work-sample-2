/// <reference path="definitions/references.d.ts" />

import net = require("src/net");
import ui = require("src/ui");
import view = require("src/view");
import watcher = require("src/watcher");

export class DashboardApp {

    public static USER_SEARCH_ROUTE:string = "user_search";
    public static PLAYER_ROUTE:string = "player_index";
    public static EDITOR_ROUTE:string = "editor_index";
    public static DECK_CREATE_ROUTE:string = "deck_create";
    public static DECK_ADD_SHARE_ROUTE:string = "deck_add_share";
    public static DECK_REMOVE_SHARE_ROUTE:string = "deck_remove_share";
    public static DECK_CHANGE_NAME_ROUTE:string = "deck_set_name";
    public static DECK_DELETE_ROUTE:string = "deck_delete";
    public static DECK_SHARES_ROUTE:string = "deck_shares";
    public static DECK_EXPORT_HTML_ROUTE:string = "deck_export_html";
    public static SLIDE_PREVIEW_ROUTE:string = "api_slide_preview";
    public static DASHBOARD_UPDATE_ROUTE:string = "dashboard_update_settings";

    public static UIOverlay:JQuery = $(".ui-overlay");

    private static DECK_SELECTOR_CLASS = ".deck-info";
    private static ADD_DECK_SELECTOR = "add-deck-button";
    private static ADD_DECK_CLASS = ".add-deck-button";

    private static SORT_BY_NAME = 'sort_name';
    private static SORT_BY_DATE = 'sort_date';

    private static VIEW_MY = 'view_my';
    private static VIEW_SHARED = 'view_shared';

    private static SORT_OPTION = 'sortOption';
    private static VIEW_OPTION = 'viewOption';

    private static COLUMN_COUNT = 4;

    private deckDialog:CreateDeckDialog = null;
    private decks:DeckWidget[] = [];
    private searchWidget:SearchWidget = new SearchWidget($(".option.search"));
    private sortOption:string;
    private viewOption:string;

    private makeChosen(element:JQuery):void {
        element.chosen({disable_search_threshold: 10});
    }

    constructor() {
        var sortSelector = $("#deck-sort");
        this.makeChosen(sortSelector);

        var viewSelector = $("#deck-filter");
        this.makeChosen(viewSelector);

        this.sortOption = sortSelector.val();
        this.viewOption = viewSelector.val();

        this.deckDialog = new CreateDeckDialog(
            $(DashboardApp.ADD_DECK_CLASS),
            $("#createDeckDialog")
        );

        DeckMenuWidget.instance();

        DeckMenuWidget.instance().onDeckRemoved((id:string)=> {
            this.onDeckRemoved(id);
        });

        sortSelector.change(()=> {
            this.searchWidget.clear();
            this.sortOption = $("#deck-sort").val();
            this.updateSortSetting();
        });

        viewSelector.change(()=> {
            this.searchWidget.clear();
            this.viewOption = $("#deck-filter").val();
            this.updateViewSetting();
        });

        this.deckDialog.onCreated((response:string) => {
            this.onDeckCreated(response);
        });

        $(DashboardApp.DECK_SELECTOR_CLASS).each((i:number, e:Element)=> {
            if (!$(e).hasClass(DashboardApp.ADD_DECK_SELECTOR))
                this.decks.push(new DeckWidget($(e)));
        });

        this.searchWidget.onChange((value:string) => {
            this.searchDecks(value);
        });

        this.layoutDecks(this.filter());
    }

    private updateSortSetting():void {
        this.layoutForCurrentViewSettings();
        this.updateDashboardSetting(DashboardApp.SORT_OPTION, this.sortOption);
    }

    private updateViewSetting():void {
        this.layoutForCurrentViewSettings();
        this.updateDashboardSetting(DashboardApp.VIEW_OPTION, this.viewOption);
    }

    private updateDashboardSetting(option:string, value:string):void {
        var url = Routing.generate(DashboardApp.DASHBOARD_UPDATE_ROUTE);
        var data:DashboardUpdateRequest = {
            updateOption: option,
            updateValue: value
        };

        net.Ajax.postJson(url, data);
    }

    private searchDecks(search:string):void {
        var decks = this.filter();

        decks = decks.filter((a:DeckWidget)=> {
            return a.Name.toLowerCase().search(search.toLowerCase()) != -1;
        });

        this.layoutDecks(decks);
    }

    private filter():DeckWidget[] {
        for (var i:number = 0; i < this.decks.length; i++)
            this.decks[i].detach();

        var decks = [].concat(this.decks);

        switch (this.viewOption) {
            case DashboardApp.VIEW_MY:
                decks = decks.filter((a:DeckWidget)=> {
                    return !a.Shared;
                });
                break;
            case DashboardApp.VIEW_SHARED:
                decks = decks.filter((a:DeckWidget)=> {
                    return a.Shared;
                });
                break;
        }

        return decks;
    }

    private onDeckRemoved(id:string):void {
        for (var i:number = 0; i < this.decks.length; i++) {
            var deck:DeckWidget = this.decks[i];
            if (deck.Id == id) {
                deck.remove();
                this.decks.remove(deck);
                this.layoutForCurrentViewSettings();
                break;
            }
        }
    }

    private layoutForCurrentViewSettings():void {
        this.sortDecks();
        var decks = this.filter();
        this.layoutDecks(decks);
    }

    private onDeckCreated(response:string):void {
        this.searchWidget.clear();
        this.decks.push(new DeckWidget($(response)));
        this.sortDecks();
        this.layoutDecks(this.filter());
    }

    private sortDecks():void {
        switch (this.sortOption) {
            case DashboardApp.SORT_BY_DATE:
                this.decks = this.decks.sort((deckA:DeckWidget, deckB:DeckWidget)=> {
                    return deckA.Created < deckB.Created ? 1 : -1;
                });
                break;
            case DashboardApp.SORT_BY_NAME:
                this.decks = this.decks.sort((deckA:DeckWidget, deckB:DeckWidget)=> {
                    return deckA.Name.toLowerCase() > deckB.Name.toLowerCase() ? 1 : -1;
                });
                break;
        }

    }

    private layoutDecks(decks:DeckWidget[]):void {
        this.deckDialog.detach();

        for (var i:number = 0; i < decks.length; i++)
            decks[i].detach();

        var decksElement = $(".decks");
        decksElement.find(".row").remove();

        var rowElement = this.rowElement();

        this.deckDialog.append(rowElement);

        var counter = 1;
        for (var i:number = 0; i < decks.length; i++) {
            counter++;
            decks[i].append(rowElement);

            if (counter == DashboardApp.COLUMN_COUNT) {
                this.closeRow(rowElement, decksElement);
                rowElement = this.rowElement();
                counter = 0;
            }
        }

        if (counter != 0)
            this.closeRow(rowElement, decksElement);
    }

    private closeRow(rowElement:JQuery, decksElement:JQuery):void {
        rowElement.append(this.clearElement());
        decksElement.append(rowElement);
    }

    private rowElement():JQuery {
        return $('<div class="row"></div>');
    }

    private clearElement():JQuery {
        return $('<div class="clear"></div>');
    }

}

class DeckWidget {

    private element:JQuery;
    private id:string;
    private name:string;
    private format:string;
    private created:Date;
    private shared:boolean;
    private nameElement:JQuery;
    private menuContainer:JQuery;

    private static SlidePreviewSelector = ".slide-preview .slide-content";

    constructor(element:JQuery) {
        this.element = element;
        this.menuContainer = this.element.find(".menuContainer");

        this.id = element.data("id");
        this.created = new Date(element.data("created") * 1000);
        this.name = element.find(".deck-name").html();
        this.shared = element.hasClass("shared");
        this.format = element.data("format");
        this.nameElement = element.find(".deck-name");

        this.element.on("mousemove", ()=> {
            DeckMenuWidget.instance().show(this.id, this.shared, this.menuContainer);
        });

        this.element.on("mouseleave", ()=> {
            DeckMenuWidget.instance().hide();
        });

        var url = Routing.generate(DashboardApp.SLIDE_PREVIEW_ROUTE, {id: this.id});
        net.Ajax.getHtml(url, (response:string) => {
            this.element.addClass("loaded");
            var element = $(response).appendTo(this.element.find(DeckWidget.SlidePreviewSelector));
            this.renderSlide(element);
        });

        if (!this.shared)
            this.bindNameEdit();
    }

    get Created():number {
        return this.created.getTime();
    }

    get Name():string {
        return this.name;
    }

    get Id():string {
        return this.id;
    }

    get Shared():boolean {
        return this.shared;
    }

    public remove():void {
        this.element.remove();
    }

    public detach():void {
        this.element.detach();
    }

    public append(parent:JQuery):void {
        parent.append(this.element);
        this.element.show();
    }

    /**
     * Renders the slide, adjusting the zoom scale and using highlight js.
     */
    private renderSlide(element:JQuery):void {
        element.show();
        var dimension = ui.UISlideContainer.dimension(this.format);

        element.css({
            width: dimension.width,
            height: dimension.height
        });

        var scale = element.parent().height() / dimension.height;
        ui.UIKit.transformScale(element, scale, scale, 0, 0);

        element.find("." + ui.UIKit.UIScriptView + " .content").each((i:number, e:Element) => {
            hljs.highlightBlock(e);
        });
    }

    private bindNameEdit():void {
        this.nameElement.click((e:JQueryMouseEventObject)=> {
            this.makeNameEditable(e);
        });
    }

    /**
     * Make the title element of the selected presentation editable.
     */
    private makeNameEditable(e:JQueryMouseEventObject):void {
        if (this.nameElement.hasClass(ui.UIKit.editable))
            return;

        ui.UIKit.enableEditable(this.nameElement);

        this.nameElement.focus(0);

        var caretRange = getCaretRange(e);
        selectRange(caretRange);

        this.nameElement.focusout(()=> {
            this.stopEditing(this.nameElement);
        });

        this.nameElement.keydown((e:JQueryKeyEventObject) => {
            if (e.keyCode == watcher.KeyboardWatcher.ENTER) {
                e.preventDefault();
                this.stopEditing(this.nameElement);
            }
        });

        this.nameElement.keyup((e:JQueryKeyEventObject) => {
            if (e.keyCode == watcher.KeyboardWatcher.ENTER)
                e.preventDefault();
        });
    }

    /**
     * Disable the editable feature of the title element of the selected presentation.
     */
    private stopEditing(nameElement):void {
        ui.UIKit.disableEditable(nameElement);
        nameElement.unbind();
        nameElement.focusout(null);
        nameElement.blur();
        var newName = nameElement.html().replace("<br>", "");
        if (newName == "" || newName == this.name)
            nameElement.html(this.name);
        else {
            nameElement.html(newName);
            var url = Routing.generate(DashboardApp.DECK_CHANGE_NAME_ROUTE, {id: this.id});
            net.Ajax.postJson(url, {name: newName});
            this.name = newName;
        }
        this.bindNameEdit();
    }
}

class SearchWidget {

    private element:JQuery;
    private input:JQuery;
    private clearer:JQuery;

    private onChangeFunction:(value:string) => any;

    constructor(element:JQuery) {
        this.element = element;
        this.input = element.find("input");
        this.clearer = element.find(".clearer");

        this.input.keyup(()=> {
            this.onInputChanged();
        });

        this.clearer.click(()=> {
            this.Value = "";
            this.hideClearer();
            this.onChangeFunction("");
        });
    }

    public clear():void {
        this.Value = "";
        this.hideClearer();
    }

    public onChange(onChange:(value:string)=>any) {
        this.onChangeFunction = onChange;
    }

    private onInputChanged():void {
        if (this.Value != "") {
            this.showClearer();
            this.element.addClass("selected");
        }
        if (this.Value == "")
            this.hideClearer();

        this.onChangeFunction(this.Value);
    }

    get Value():string {
        return this.input.val();
    }

    set Value(value:string) {
        this.input.val(value);
    }

    private hideClearer():void {
        if (!this.clearer.is(":visible"))
            return;

        this.element.removeClass("selected");
        this.input.width(this.input.width() + this.clearer.width());
        this.clearer.hide();
    }

    private showClearer():void {
        if (this.clearer.is(":visible"))
            return;

        this.input.width(this.input.width() - this.clearer.width());
        this.clearer.show();
    }


}

/**
 * Menu element widget to perform operations for the selected presentation.
 */
class DeckMenuWidget {

    private element:JQuery = $("#deck-menu");

    /**
     * Anchor element to redirect to the presentation editor.
     */
    private editButton:JQuery;

    /**
     * Button to open the share dialog.
     */
    private shareButton:JQuery;

    /**
     * Button to delete the selected presentation.
     */
    private deleteButton:JQuery;

    /**
     * Anchor element to redirect to the presentation player.
     */
    private playButton:JQuery;

    /**
     * Anchor element to redirect to the export page
     */
    private exportButton:JQuery;

    private deckId:string;

    private onDeckRemovedFunction:(id:string)=>any;

    private static _instance:DeckMenuWidget;

    public static instance():DeckMenuWidget {
        if (DeckMenuWidget._instance == null)
            DeckMenuWidget._instance = new DeckMenuWidget();

        return DeckMenuWidget._instance;
    }

    constructor() {

        var shareDeckDialog = new ShareDeckDialog();

        this.exportButton = this.element.find(".button.export");
        this.editButton = this.element.find(".button.edit");
        this.playButton = this.element.find(".button.play");
        this.shareButton = this.element.find(".button.share");
        this.deleteButton = this.element.find(".button.delete");

        this.shareButton.click((e:JQueryMouseEventObject)=> {
            e.stopPropagation();
            shareDeckDialog.open(this.deckId);
        });

        this.deleteButton.click((e:JQueryMouseEventObject)=> {
            var url = Routing.generate(DashboardApp.DECK_DELETE_ROUTE, {id: this.deckId});
            net.Ajax.getJson(url, (response:AjaxResponse)=> {
                if (response.success) {
                    this.detachMenu();
                    this.onDeckRemovedFunction(this.deckId);
                }
            });
        });

    }

    public onDeckRemoved(onDeckRemovedFunction:(id:string)=>any) {
        this.onDeckRemovedFunction = onDeckRemovedFunction;
    }

    public Element():JQuery {
        return this.element;
    }

    /**
     * Hides the menu.
     */
    public hide():void {
        this.element.hide();
    }

    /**
     * Shows the menu.
     */
    public show(deckId:string, shared:boolean, parent:JQuery):void {
        if (this.element.is(":visible"))
            return;

        this.deckId = deckId;

        var urlParams = {id: this.deckId};
        var editorUrl = Routing.generate(DashboardApp.EDITOR_ROUTE, urlParams);
        var playerUrl = Routing.generate(DashboardApp.PLAYER_ROUTE, urlParams);
        var exportUrl = Routing.generate(DashboardApp.DECK_EXPORT_HTML_ROUTE, urlParams);

        this.editButton.find("a").attr("href", editorUrl);
        this.playButton.find("a").attr("href", playerUrl);
        this.exportButton.find("a").attr("href", exportUrl);

        // preserver jquery bindings
        this.element.detach();
        parent.html(this.element);
        this.element.show();

        if (shared) {
            this.deleteButton.hide();
            this.shareButton.hide();
        } else {
            this.deleteButton.show();
            this.shareButton.show();
        }

    }

    /**
     * Detaches the menu by attaching if to the html body and hiding it.
     */
    private detachMenu():void {
        this.element.detach();
        this.element.hide();
    }

}

/**
 * Widget component to search users and make users selectable.
 */
class ShareUserWidget {

    private static USER_SELECTOR = ".user";

    private element:JQuery;
    private shareInput:JQuery;
    private resultContainer:JQuery;
    private addButton:JQuery;
    private selectedUserContainer:JQuery;
    private selectedUser:JQuery;

    private request:net.Ajax = new net.Ajax();

    private onUserAddedFunction:(element:JQuery)=>any;

    constructor(element:JQuery) {
        this.element = element;
        this.shareInput = this.element.find("input");
        this.resultContainer = this.element.find(".searchResult");
        this.addButton = this.element.find(".button.add");
        this.selectedUserContainer = this.element.find("#selectedUser");
        this.addButton.addClass(ui.UIKit.disabled);

        this.shareInput.keyup(()=> {
            this.loadUsers();
        });

        this.shareInput.click(()=> {
            if (!this.resultContainer.is(":empty"))
                this.resultContainer.show();
        });

        this.addButton.click(()=> {
            if (this.selectedUser == null)
                return;

            this.selectedUserContainer.parent().removeClass("userSelected");
            this.selectedUser.remove();
            this.shareInput.show();
            this.onUserAddedFunction(this.selectedUser);
            this.selectedUser = null;
        });

        this.element.focusout(()=> {
            if (!this.resultContainer.is(":hover"))
                this.resultContainer.hide();
        });
    }

    public onUserAdded(p:(element:JQuery)=>any) {
        this.onUserAddedFunction = p;
    }

    /**
     * Cleans the widget. Removing the input value und all selected users.
     */
    public close():void {
        this.shareInput.val("");
        this.hideResults(true);
    }

    /**
     * The input field to search users has changed or a user was removed from the selection.
     */
    private loadUsers():void {
        if (this.shareInput.val() == "") {
            this.hideResults(true);
            return;
        }

        var url = Routing.generate(DashboardApp.USER_SEARCH_ROUTE, {search: this.shareInput.val()});
        this.request.getJson(url, (response:SearchUserResponse)=> {
            if (response.noResults)
                return;

            this.displayResults(response.html);
        });
    }

    private displayResults(html:string):void {
        this.resultContainer.html(html);
        this.resultContainer.show();
        this.resultContainer.find(ShareUserWidget.USER_SELECTOR).each((i:number, e:Element) => {
            $(e).click(()=> {
                this.userSelected($(e));
            });
        });
    }

    private hideResults(empty:boolean):void {
        if (empty)
            this.resultContainer.empty();

        this.resultContainer.hide();
    }

    private userSelected(e:JQuery) {
        this.selectedUser = e;
        this.selectedUserContainer.append(e);
        this.shareInput.hide();
        this.shareInput.val("");
        this.selectedUserContainer.parent().addClass("userSelected");
        this.addButton.removeClass(ui.UIKit.disabled);

        this.selectedUser.find(".remove").click(()=> {
            this.selectedUserContainer.parent().removeClass("userSelected");
            this.selectedUser.remove();
            this.shareInput.show();
            this.selectedUser = null;
        });
    }

}

/**
 * Dialog widget to create a new presentation.
 */
class CreateDeckDialog {

    private dialog:JQuery;
    private buttonElement:JQuery;
    private addButton:JQuery;
    private submitButton:JQuery;
    private closeButton:JQuery;
    private nameInput:JQuery;
    private sharedUsersWidget:ShareUserWidget;
    private onCreatedFunction:(response:string) =>any;
    private deckFormat:string = ui.UISlideContainer.FORMAT_4_3;
    private selectedShares:JQuery;

    constructor(buttonElement:JQuery, dialog:JQuery) {
        this.buttonElement = buttonElement;
        this.addButton = this.buttonElement.find(".slide-preview");
        this.dialog = dialog;
        this.submitButton = this.dialog.find(".button.submit");
        this.nameInput = this.dialog.find("p.name input");
        this.closeButton = this.dialog.find(".button.close");
        this.selectedShares = this.dialog.find(".shares");

        this.sharedUsersWidget = new ShareUserWidget(this.dialog.find(".user-search"));
        this.sharedUsersWidget.onUserAdded((selectedUser:JQuery)=> {
            if (this.selectedShares.find(".user[data-id=" + selectedUser.data("id") + "]").length != 0)
                return;

            this.selectedShares.append(selectedUser);

            selectedUser.find(".remove").click(() => {
                selectedUser.remove();
            });
        });

        this.addButton.click(()=> {
            this.open();
        });

        this.submitButton.click(()=> {
            this.submit();
        });

        this.closeButton.click(()=> {
            this.close();
        });

        var self = this;
        this.dialog.find(".format-options .button").click(function () {
            self.dialog.find(".format-options .button").removeClass("ui-state-selected");
            $(this).addClass("ui-state-selected");
            self.deckFormat = $(this).data("format");
        });
    }

    public detach():void {
        this.buttonElement.detach();
    }

    public append(parent:JQuery):void {
        parent.append(this.buttonElement);
    }

    public onCreated(p:(response:string)=>any):void {
        this.onCreatedFunction = p;
    }

    /**
     * Shows the dialog.
     */
    private open():void {
        this.dialog.show();
        DashboardApp.UIOverlay.show();
    }

    /**
     * Closes the dialog.
     */
    private close():void {
        this.dialog.hide();
        this.sharedUsersWidget.close();
        DashboardApp.UIOverlay.hide();
        this.reset();
    }

    private reset():void {
        this.nameInput.val("");
        this.deckFormat = ui.UISlideContainer.FORMAT_4_3;
        this.dialog.find(".format-options .button").removeClass("ui-state-selected");
        this.dialog.find(".format-options .button").first().addClass("ui-state-selected");
    }


    /**
     * Send the dialog data.
     */
    private submit():void {
        if (this.nameInput.val() == "")
            return;

        var sharedUsers:string[] = [];

        this.selectedShares.find(".user").each((i:number, e:Element)=> {
            sharedUsers.push($(e).data("id"));
        });

        var data:CreateDeckRequest = {
            name: this.nameInput.val(),
            sharedUsers: sharedUsers,
            format: this.deckFormat
        };

        net.Ajax.postHtml(Routing.generate(DashboardApp.DECK_CREATE_ROUTE), data, (response:string)=> {
            this.close();
            this.onCreatedFunction(response)
        });

        this.nameInput.val("");
    }

}

/**
 * Dialog widget to share the presentation with selected users.
 */
class ShareDeckDialog {

    private dialog:JQuery = $("#shareDeckDialog");
    private closeButton:JQuery;
    private sharedUsersWidget:ShareUserWidget;
    private currentSharesElement:JQuery;

    private static PROCESSING = "processing";

    /**
     * Id of the presentation.
     */
    private deck:string;

    constructor() {
        this.closeButton = this.dialog.find(".button.close");
        this.currentSharesElement = this.dialog.find(".shares");
        this.sharedUsersWidget = new ShareUserWidget(this.dialog.find(".user-search"));

        this.sharedUsersWidget.onUserAdded((selectedUser:JQuery)=> {
            if (this.currentSharesElement.find(".user[data-id=" + selectedUser.data("id") + "]").length != 0)
                return;

            this.currentSharesElement.append(selectedUser);
            selectedUser.addClass(ShareDeckDialog.PROCESSING);
            var data:ShareDeckRequest = {sharedUser: selectedUser.data("id")};
            var url = Routing.generate(DashboardApp.DECK_ADD_SHARE_ROUTE, {id: this.deck});

            net.Ajax.postJson(url, data, ()=> {
                selectedUser.removeClass(ShareDeckDialog.PROCESSING);
            });

            selectedUser.find(".remove").click(()=> {
                this.removeShare(selectedUser);
            });
        });

        this.closeButton.click(()=> {
            this.close();
        });

    }

    /**
     * Open the dialog with the given id of the presentation.
     */
    public open(deck:string):void {
        this.deck = deck;
        this.dialog.show();
        this.currentSharesElement.html("");
        DashboardApp.UIOverlay.show();
        this.dialog.find(".processing").show();

        var url = Routing.generate(DashboardApp.DECK_SHARES_ROUTE, {id: this.deck});
        net.Ajax.getHtml(url, (response:string)=> {
            this.dialog.find(".processing").hide();
            this.currentSharesElement.html(response);
            this.currentSharesElement.find(".user").each((i:number, e:Element)=> {
                $(e).find(".remove").click(()=> {
                    this.removeShare($(e));
                });
            });
        });
    }

    private removeShare(e:JQuery):void {
        var data:ShareDeckRequest = {sharedUser: e.data("id")};
        var url = Routing.generate(DashboardApp.DECK_REMOVE_SHARE_ROUTE, {id: this.deck});
        e.addClass(ShareDeckDialog.PROCESSING);
        net.Ajax.postJson(url, data, ()=> {
            $(e).remove();
        });
    }

    /**
     * Closes the dialog.
     */
    private close():void {
        DashboardApp.UIOverlay.hide();
        this.dialog.hide();
        this.sharedUsersWidget.close();
    }

}

interface DashboardUpdateRequest {
    updateOption:string;
    updateValue:string;
}


interface AjaxResponse {
    success:boolean;
}

interface SearchUserResponse {
    html:string;
    noResults:boolean;
}

interface CreateDeckRequest {
    name:string;
    format: string;
    sharedUsers: string[];
}

interface ShareDeckRequest {
    sharedUser: string;
}
