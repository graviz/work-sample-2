/// <reference path="PlayerApp.ts" />

require.config({
    baseUrl: "/bundles/hipwaveslides/js"
});

require([
    'PlayerApp'
],
    (main) => {
        new main.PlayerApp();
    }
);