({
    baseUrl: "../../../js/lib",
    out: "../out/editor.lib.build.js",
    paths: {
        "jquery": "jquery.1.10",
        "jquery.ui": "jquery.ui.1.10.3",
        "jquery.mousewheel": "jquery.mousewheel.min",
        "bootstrap": "BootStrap",
        "jquery.mCustomScrollbar": "jquery.mCustomScrollbar.min",
        "jquery.spectrum": "jquery.spectrum",
        "highlightjs": "highlight.pack",
        "less": "less",
        "requirelib": "require"
    },
    include: [
        "less",
        "bootstrap",
        "requirelib",
        "jquery",
        "jquery.ui",
        "jquery.mousewheel",
        "jquery.mCustomScrollbar",
        "jquery.spectrum",
        "highlightjs"
    ]
})