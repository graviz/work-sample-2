({
    baseUrl: "../../../js/lib",
    out: "../out/player.lib.build.js",
    paths: {
        "jquery": "jquery.1.10",
        "jquery.ui": "jquery.ui.1.10.3",
        "highlightjs": "highlight.pack",
        "requirelib": "require"
    },
    include: [
        "requirelib",
        "jquery",
        "jquery.ui",
        "highlightjs"
    ]
})