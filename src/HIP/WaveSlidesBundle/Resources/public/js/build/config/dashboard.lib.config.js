({
    baseUrl: "../../../js/lib",
    out: "../out/dashboard.lib.build.js",
    paths: {
        "jquery": "jquery.1.10",
        "jquery.ui": "jquery.ui.1.10.3",
        "jquery.chosen": "chosen.jquery.min",
        "highlightjs": "highlight.pack",
        "requirelib": "require",
        "bootstrap": "BootStrap"
    },
    include: [
        "requirelib",
        "jquery",
        "jquery.ui",
        "jquery.chosen",
        "highlightjs",
        "bootstrap"
    ]
})