/// <reference path="definitions/references.d.ts" />

import ui = require("src/ui");
import property = require("src/property");
import view = require("src/view");
import chat = require("src/chat");
import common = require("src/common");
import ws = require("src/ws");

export class EditorApp {

    constructor() {
        new common.System();
        new ui.UIKit();

        var wsUrl = WS_URL;

        ui.Workbench.get().Socket = new ws.Socket(wsUrl);
        ui.Workbench.get().Chat = new chat.ChatWidget();

        hljs.configure({
            useBR: false,
            languages: []
        });
    }

}