///<reference path="../definitions/references.d.ts" />

import ui = require("src/ui");

export class TextFormatter {

    public static formatElement(element:JQuery, format:string, value:string) {
        switch (format) {
            case "bold":
                formatIt("b");
                break;
            case "italic":
                formatIt("i");
                break;
            case "underline":
                formatIt("u");
                break;
            case "strikeThrough":
                formatIt("strike");
                break;
            case "superscript":
                formatIt("sup");
                break;
            case "subscript":
                formatIt("sub");
                break;
            case "justifyLeft":
                alignText("left");
                break;
            case "justifyCenter":
                alignText("center");
                break;
            case "justifyRight":
                alignText("right");
                break;
            case "justifyFull":
                alignText("justify");
                break;

        }

        function formatIt(tag) {
            if (element.has(tag).length)
                element.find($(tag)).contents().unwrap();
            else
                element.html("<" + tag + ">" + element.html() + "</" + tag + ">");
        }

        function alignText(tag) {
            element.find('div[align="left"]').contents().unwrap();
            element.find('div[align="right"]').contents().unwrap();
            element.find('div[align="center"]').contents().unwrap();
            element.find('div[align="justify"]').contents().unwrap();
            element.html('<div align="' + tag + '">' + element.html() + '</div>');
        }
    }

    public static formatSelection(format:string, value:string) {
        document.execCommand(format, false, null);
    }

    /**
     * Currently not used.
     * @returns {string}
     */
    private static getSelectionHtml():string {
        var html = "";
        if (typeof window.getSelection != "undefined") {
            var sel = window.getSelection();
            if (sel.rangeCount) {
                var container = document.createElement("div");
                for (var i = 0, len = sel.rangeCount; i < len; ++i)
                    container.appendChild(sel.getRangeAt(i).cloneContents());
                html = container.innerHTML;
            }
        } else if (typeof document.selection != "undefined")
            if (document.selection.type == "Text")
                html = document.selection.createRange().htmlText;

        return html;
    }

    /**
     * Currently not used.
     * @param htmlString
     */
    private static replaceSelection(htmlString) {
        var range;

        if (window.getSelection && window.getSelection().getRangeAt) {
            range = window.getSelection().getRangeAt(0);
            range.deleteContents();
            var div = document.createElement("div");
            div.innerHTML = htmlString;

            var frag = document.createDocumentFragment(), child;
            while ((child = div.firstChild)) {
                frag.appendChild(child);
            }
            range.insertNode(frag);
        } else if (document.selection && document.selection.createRange) {
            range = document.selection.createRange();
            range.pasteHTML(htmlString);
        }
    }

    /**
     * Currently not used.
     * @param str
     * @returns {string|void}
     */
    private static br2nl(str) {
        return str.replace(/<br\s*\/?>/mg, "\n");
    }


    /**
     * Currently not used.
     * @param element
     * @returns {*}
     */
    public static convertToTextObject(element:JQuery):TextObject[] {
        var textObjects:TextObject[] = [];

        if (element.children().length == 0)
            return [{
                text: element.html()
            }];

        element.children().each((i:number, e:Element)=> {
            var jquery = $(e);

            if (jquery.html() == "")
                return true;

            var text:TextObject = {};
            text.text = TextFormatter.br2nl(jquery.html());

            if (jquery.css("font-size") != null)
                text.size = parseInt(jquery.css("font-size"));

            if (jquery.css("text-align") != null && jquery.css("text-align") != "start")
                text.align = jquery.css("text-align");

            if (jquery.css("color") != null)
                text.color = ui.UIColor.fromString(jquery.css("color")).toObject();

            if (jquery.css("font-family") != null)
                text.font = jquery.css("font-family");

            if (jquery.hasClass("sub"))
                text.script = "sub";

            if (jquery.hasClass("super"))
                text.script = "super";

            if (jquery.hasClass("bold"))
                text.bold = true;

            if (jquery.hasClass("italic"))
                text.italic = true;

            if (jquery.hasClass("strikeout"))
                text.strikeout = true;

            if (jquery.hasClass("underline"))
                text.underline = true;

            textObjects.push(text);
        });

        return textObjects;
    }

}
