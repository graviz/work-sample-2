///<reference path="../definitions/references.d.ts" />

import watcher = require("src/watcher");
import view = require("src/view");
import ui = require("src/ui");

/**
 * Responsible for uploading files.
 */
export class FileManager {

    public static isImage(file:File):boolean {
        switch (file.type) {
            case "image/bmp":
            case "image/gif":
            case "image/jpeg":
            case "image/png":
            case "image/tiff":
                return true;
        }
        return false;
    }

    /**
     * Uploads a file which is placed on slide.
     */
    public static uploadSlideFile(file:File, slideId:string, onUploaded:(response:string)=>any, onProgress:(progress:number, loaded:number)=>any):void {
        var formData:FormData = new FormData();
        formData.append("file", file);
        formData.append("json", JSON.stringify(<ImageUploadRequestData>{
            type: "ImageUploadRequestData",
            slide: slideId,
            deck: DECK_ID,
            sender: CON_ID,
            user: USER_ID,
            session: SESSION_ID
        }));

        var url = Routing.generate("deck_upload");
        Ajax.postFormData(url, formData,
            (e:ProgressEvent) => {
                var progress:number = Math.round(100 * (e.loaded / e.total));
                onProgress(progress, e.loaded);
            },
            onUploaded
        );
    }

}

/**
 * Wrapper class for the JQuery Ajax methods.
 */
export class Ajax {

    /**
     * Performs post request with a FormData object.
     */
    public static postFormData(url:string, formData:FormData, onProgress:(e:ProgressEvent)=>any, onSuccess:(response:string)=>void) {
        var request = new XMLHttpRequest();

        request.onload = function (e) {
            if (onSuccess) {
                var response = "";
                try {
                    response = JSON.parse((<any>(e.currentTarget)).response)
                } catch (e) {
                    console.log(e);
                }
                onSuccess(response);
            }

        };

        request.upload.onprogress = (e:ProgressEvent)=> {
            if (onProgress)
                onProgress(e);
        };

        request.open("POST", url);
        request.send(formData);
    }

    /**
     * Performs a POST request.
     */
    public static post(url:string, outputType:string, data:Object, callback?:(response:any)=>void):void {
        new Ajax().post(url, outputType, data, callback);
    }

    /**
     * Performs a POST request accepting json as response.
     */
    public static postJson(url:string, data:Object, callback?:(response:any)=>void):void {
        new Ajax().postJson(url, data, callback);
    }

    /**
     * Performs a POST request accepting html as response.
     */
    public static postHtml(url:string, data:Object, callback?:(response:any)=>void):void {
        new Ajax().postHtml(url, data, callback);
    }

    /**
     * Performs a GET request accepting json as response.
     */
    public static getJson(url:string, callback?:(response:any) => void):void {
        new Ajax().getJson(url, callback);
    }

    /**
     * Performs a GET request accepting html as response.
     */
    public static getHtml(url:string, callback?:(response:any) => void):void {
        new Ajax().getHtml(url, callback);
    }

    /**
     * The actual JQuery object.
     */
    private jqXHR:JQueryXHR;

    constructor() {
        /* Abort the current request if the browser window is closed. */
        jQuery(window).on("beforeunload", () => {
            this.abort();
        });
    }

    /**
     * Performs a POST request.
     */
    public post(url:string, outputType:string, data:any, callback?:(response:any)=>void):void {
        this.request(url, outputType, data, 'POST', callback);
    }

    /**
     * Performs a POST request accepting json as response.
     */
    public postJson(url:string, data:any, callback?:(response:any)=>any):void {
        this.request(url, 'json', data, 'POST', callback);
    }

    /**
     * Performs a POST request accepting html as response.
     */
    public postHtml(url:string, data:any, callback?:(response:any)=>any):void {
        this.request(url, 'html', data, 'POST', callback);
    }

    /**
     * Performs a GET request accepting json as response.
     */
    public getJson(url:string, callback?:(response:any) => any):void {
        this.request(url, "json", null, "GET", callback);
    }

    /**
     * Performs a GET request accepting HTML as response.
     */
    public getHtml(url:string, callback?:(response:any) => any):void {
        this.request(url, "html", null, "GET", callback);
    }

    /**
     * Performs an ajax request and aborts the current request.
     * @param url
     * @param outputType
     *      html,xml,json
     * @param data
     *      Any object will be stringified to a json string.
     * @param type
     *      GET,POST
     * @param callback
     */
    private request(url:string, outputType:string, data:Object, type:string, callback?:(response:any)=>any):void {

        this.abort();

        if (data != null)
            data = {json: JSON.stringify(data)};

        this.jqXHR = <JQueryXHR>$.ajax({
            url: url,
            dataType: outputType,
            async: true,
            type: type,
            data: data,
            success: function (output) {
                if (callback)
                    callback(output);
            }
        }).fail(function (jqXHR:JQueryXHR, textStatus:string, errorThrown:string) {
            if (textStatus == "abort")
                return;
            alert("textStatus: " + textStatus + "\n" + "errorThrown: " + errorThrown + "\n" + jqXHR.responseText);
        });
    }

    /**
     * Aborts the current ajax request.
     */
    private abort():void {
        if (this.jqXHR != null)
            this.jqXHR.abort();
    }
}