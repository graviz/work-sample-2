///<reference path="../definitions/references.d.ts" />

import helper = require("src/helper");
import watcher = require("src/watcher");
import ui = require("src/ui");
import property = require("src/property");
import ws = require("src/ws");
import common = require("src/common");
import formatter = require("src/formatter");

export class UIView implements watcher.KeyboardObserver {

    /**
     * Every view has an element. The element should be the first container of the view.
     */
    private element:JQuery;

    /**
     * Refers to the MongoID
     */
    private id:string;

    /**
     * Indicates if the shift key ist pressed.
     */
    private shiftPressed:boolean = false;

    constructor(element:JQuery) {
        this.element = element;
        this.id = this.Element.data("id");
        this.Element.mousedown((event:JQueryMouseEventObject) => {
            this.select(true);
        });
        watcher.KeyboardWatcher.register(this);
    }

    public onKeyDown(keycode:Number, event:JQueryKeyEventObject):void {
        if (keycode == watcher.KeyboardWatcher.SHIFT)
            this.shiftPressed = true;
    }

    public onKeyUp(keycode:Number, event:JQueryKeyEventObject):void {
        if (keycode == watcher.KeyboardWatcher.SHIFT)
            this.shiftPressed = false;
    }

    get Element():JQuery {
        return this.element;
    }

    get Id():string {
        return this.id;
    }

    get BackgroundColor():ui.UIColor {
        return ui.UIColor.fromString(this.Element.data("backgroundcolor"));
    }

    /**
     * Set the background color and set the element style background-color property.
     */
    set BackgroundColor(color:ui.UIColor) {
        this.Element.data("backgroundcolor", color.toRGBAString());
        this.Element.css("background-color", color.toRGBAString());
    }

    /**
     * Selects the view. Responsible for setting the css class.
     * If dispatchEvent is true, the ViewWatcher get notified about a single
     * selection. A single selection will cause all other selected views
     * to get unselected.
     * @param dispatchEvent
     */
    public select(dispatchEvent:boolean):void {
        if (this.isSelected()) {
            if (this.shiftPressed)
                this.unselect(true);
            return;
        }

        this.Element.addClass(ui.UIKit.selected);
        if (dispatchEvent)
            if (this.shiftPressed)
                watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.AddToSelectionMessage, this);
            else
                watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.SelectedEventMessage, this);
    }

    /**
     * Unselect a view by removing the css class from its element and notifying the ViewWatcher.
     */
    public unselect(dispatchEvent:boolean):void {
        this.Element.removeClass(ui.UIKit.selected);
        if (dispatchEvent)
            watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.UnselectedEventMessage, this);
    }

    /**
     * Notify the ViewChanger that the view has changed.
     */
    public hasChanged():void {
        watcher.ViewWatcher.notifyAll(watcher.ViewWatcher.ChangedEventMessage, this);
    }

    /**
     * If a view is selected is determined if the element
     * has the proper css class.
     */
    public isSelected():boolean {
        return this.Element.hasClass(ui.UIKit.selected);
    }

    /**
     * Converts this view to a json convertable object.
     */
    public toObject():ViewObject {
        return {
            id: this.Id,
            styles: this.getStyles(),
            type: "UIView"
        };
    }

    public getStyles():Object {
        return {
            "background-color": {
                type: "background-color",
                color: this.BackgroundColor.toObject()
            }
        };
    }

    /**
     * Update the view with the data of a ViewObject.
     * @param view
     */
    public updateView(view:ViewObject):void {
        for (var key in view.styles) {
            var style = view.styles[key];

            if (key == "background-color")
                this.BackgroundColor = ui.UIColor.fromRGBA((<BackgroundColorComponent>style).color);
        }
    }
}

export class UISlideView extends UIView implements watcher.SelectionObserver {

    static name:string = "UISlideView";

    /**
     * The thumbnail view of this slide.
     */
    private thumbnail:UISlideThumbnailView;

    /**
     * Number of the slide.
     */
    private index:number;

    /**
     * Array with elements of the slide.
     */
    private content:UISlideElementView[] = [];

    /**
     * Array containing the selected slide elements.
     */
    private selectedElements:UISlideElementView[] = [];

    /**
     * To prevent infinite event looping we must indicate when slide elements are being unselected.
     */
    private unselectingContent:boolean = false;

    constructor(element:JQuery) {
        super(element);

        this.index = parseInt(this.Element.data("index"));

        element.find(".elements").children().each((index:number, element:Element) => {
            var child:JQuery = $(element);
            var role = child.data("role");
            var slideElement:UISlideElementView = this.createElement(child, role);
            if (slideElement != null)
                this.Content.push(slideElement);
        });

        this.thumbnail = new UISlideThumbnailView(this.Index, this);
        this.thumbnail.refresh();

        watcher.SelectionWatcher.register(this);

        this.Element.unbind("mousedown");
    }

    get SelectedElements():UISlideElementView[] {
        return this.selectedElements;
    }

    set SelectedElements(array:UISlideElementView[]) {
        this.selectedElements = array;
    }

    get Index():number {
        return this.index;
    }

    get Content():UISlideElementView[] {
        return this.content;
    }

    get ElementsContainer():JQuery {
        return this.Element.find(".elements");
    }

    /**
     * Set the index of this slide and the Updates the index of the thumbnails.
     * @param index
     * @constructor
     */
    set Index(index:number) {
        this.index = index;
        this.Thumbnail.Index = index;
    }

    get Thumbnail():UISlideThumbnailView {
        return this.thumbnail;
    }

    /**
     * Removes the slide element from the slide.
     * Adjusting the z-index of all slide elements.
     * Removes the element from the content array.
     * Removes the jquery element from the slide.
     */
    public removeElement(element:UISlideElementView):void {
        this.Content.sort((a:UISlideElementView, b:UISlideElementView) => {
            return a.Z > b.Z ? 1 : -1;
        });

        for (var i = element.Z + 1; i < this.Content.length; i++)
            this.Content[i].Z = this.Content[i].Z - 1;

        this.Content.remove(element);

        element.Element.remove();
        this.Thumbnail.refresh();
    }

    /**
     * Returns the slide element by the given id.
     * Returns null if no element was found.
     */
    public getElementById(id:string):UISlideElementView {
        for (var i = 0; i < this.Content.length; i++)
            if (id == this.Content[i].Id)
                return this.Content[i];
        return null;
    }

    /**
     * Removes the slide from the slide container.
     * If dispatchEvent is true, the ViewWatcher get notified about removing this slide.
     * @param dispatchEvent
     */
    public remove(dispatchEvent:boolean):void {
        this.Element.remove();
        this.Thumbnail.Element.remove();
        if (dispatchEvent)
            watcher.ViewWatcher.notifyAll(watcher.ViewWatcher.SlideRemovedMessage, this);
    }

    /**
     * Updates the thumbnail.
     */
    public hasChanged():void {
        super.hasChanged();
        this.Thumbnail.refresh();
    }

    /**
     * Select this slide, meaning to show this slide in the slide container.
     * Also selects the thumbnail view.
     */
    public select(dispatchEvent:boolean):void {
        super.select(dispatchEvent);
        this.Element.show();
        this.Thumbnail.select(dispatchEvent);
        this.enableMultiSelect();
    }

    /**
     * Unselect this slide, by hiding this element.
     * Also unselects every selected element on the slide.
     */
    public unselect():void {
        this.Element.hide();
        this.disableMultiSelect();
        this.Thumbnail.unselect(true);
        this.Content.forEach((element:UISlideElementView)=> {
            if (element.isSelected())
                element.unselect(true);
        })
    }

    /**
     * Enable multiselect support on the slide.
     */
    public enableMultiSelect():void {
        /* Jquery automatically call unselecting when you mousedown on the slide */
        var cssClass = "uiSlideElement";
        ui.UIKit.enableMultiSelectable(
            this.Element,
            (event, ui) => { // selected
                if ($(ui.selected).hasClass(cssClass)) {
                    var view = this.findViewByElement($(ui.selected));
                    view.select(false);
                    watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.AddToSelectionMessage, view);
                }
            },
            (event, ui) => { //selecting
                if ($(ui.selecting).hasClass(cssClass)) {
                    var view = this.findViewByElement($(ui.selecting));
                    view.select(false);
                }
            },
            (event, ui) => { //unselecting
                console.log("unselecting");
                if ($(ui.unselecting).hasClass(cssClass)) {
                    var view = this.findViewByElement($(ui.unselecting));
                    view.unselect(true);
                }
            },
            (event, ui) => { // start
                watcher.ClickWatcher.notifyAll(watcher.ClickWatcher.SlideContainerClicked, this);
            }
        );

    }

    /**
     * Disable multiselect support on the slide.
     */
    public disableMultiSelect():void {
        ui.UIKit.disableMultiSelectable(this.Element);
    }

    /**
     * Returns the UISlideElementView for a jQuery element.
     * Comparison is made with the data-id element.
     */
    public findViewByElement(element:JQuery):UISlideElementView {
        for (var i = 0; i < this.Content.length; i++)
            if (this.Content[i].Id == element.data("id"))
                return this.Content[i];
        return null;
    }

    /**
     * Triggers hasChanged of all selected elements.
     */
    public contentHasChanged():void {
        this.SelectedElements.forEach((element:UISlideElementView)=> {
            element.hasChanged();
        });
    }

    /**
     * Inserts a jquery element into the slide.
     * The role argument specifies the instance to be created.
     */
    public insertElement(element:JQuery, role:string):UISlideElementView {
        var slideElement:UISlideElementView = this.createElement(element, role);

        if (slideElement != null)
            this.Content.push(slideElement);

        this.Thumbnail.refresh();

        return slideElement;
    }

    /**
     * A UITextView was made editable.
     * Deselect all selected elements
     * @param view
     */
    public onStartEditing(view:UISlideElementView):void {
        this.unselectContent(view);
        this.disableMultiSelect();
        this.Element.click((event:JQueryMouseEventObject)=> {
            if (event.target == this.ElementsContainer[0])
                view.unselect(true);
        });
    }

    /**
     * A UITextView stopped being editable.
     */
    public onStopEditing():void {
        this.Element.off("click");
        this.enableMultiSelect();
    }

    public onSelectionMessage(message:String, object:any):void {
        if (!(object instanceof UISlideElementView) || object.Slide != this)
            return;

        switch (message) {
            case watcher.SelectionWatcher.SelectedEventMessage:
                this.unselectContent(object);
                break;
            case watcher.SelectionWatcher.AddToSelectionMessage:
                this.selectedElements.push(object);
                break;
            case watcher.SelectionWatcher.UnselectedEventMessage:
                if (this.unselectingContent)
                    return;
                if (this.selectedElements.length != 0)
                    this.selectedElements.remove(object);
                if (this.selectedElements.length == 0)
                    watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.NoElementSelectedMessage, this);
                break;
        }
    }

    public updateView(view:SlideObject):void {
        super.updateView(view);
        this.Index = view.index;
    }

    public toObject():ViewObject {
        var object = <SlideObject>super.toObject();
        object.index = this.Index;
        object.type = UISlideView.name;
        return object;
    }

    private unselectContent(except:UISlideElementView):void {
        this.unselectingContent = true;
        this.SelectedElements.forEach((element:UISlideElementView)=> {
            if (element.Id != except.Id)
                element.unselect(true);
        });
        this.SelectedElements = [];
        this.SelectedElements.push(except);
        this.unselectingContent = false;
    }

    private createElement(element:JQuery, role:string):UISlideElementView {
        var slideElement:UISlideElementView = null;
        switch (role) {
            case UITextView.role:
                slideElement = new UITextView(element, this);
                break;
            case UIImageView.role:
                slideElement = new UIImageView(element, this);
                break;
            case UIScriptView.role:
                slideElement = new UIScriptView(element, this);
                break;
            case UITableView.role:
                slideElement = new UITableView(element, this);
                break;
        }
        return slideElement;
    }

}

export class UISlideThumbnailView extends UIView implements watcher.SelectionObserver {

    /**
     * The thumbnail element is the div container
     * with the actual content of the slide.
     * The "element" in UIView is the thumbnail container with
     * index number etc.
     */
    private thumbnail:JQuery;

    private index:number;

    /**
     * Indicates if the thumbnail is highlighted.
     */
    private highlighted:boolean;

    private slide:UISlideView;

    public static DefaultWidth:number = 96;

    constructor(index:number, slide:UISlideView) {
        super(helper.SlideHelper.getThumbnailElement(index));
        this.thumbnail = this.Element.find(".thumbnail");
        this.index = index;
        this.slide = slide;
        watcher.SelectionWatcher.register(this);

        var height = UISlideThumbnailView.DefaultWidth / (ui.UISlideContainer.dimension().width / ui.UISlideContainer.dimension().height);
        ui.UIKit.transformScale(
            this.thumbnail,
            height / ui.UISlideContainer.dimension().height,
            UISlideThumbnailView.DefaultWidth / ui.UISlideContainer.dimension().width,
            0,
            0
        );

        this.thumbnail.parent().width(UISlideThumbnailView.DefaultWidth);
        this.thumbnail.parent().height(height);

        this.thumbnail.width(ui.UISlideContainer.dimension().width);
        this.thumbnail.height(ui.UISlideContainer.dimension().height);
    }

    get Slide():UISlideView {
        return this.slide;
    }

    get Index():number {
        return this.index;
    }

    set Index(index:number) {
        this.index = index;
        this.Element.find(".number").html((index + 1).toString());
    }

    public isHighlighted():boolean {
        return this.highlighted;
    }

    set Highlighted(value:boolean) {
        if (value)
            this.Element.addClass("highlight");
        else
            this.Element.removeClass("highlight");

        this.highlighted = value;
    }

    public onSelectionMessage(message:string, object:any):void {
        if (message == watcher.SelectionWatcher.SelectedEventMessage || message == watcher.SelectionWatcher.AddToSelectionMessage)
            this.Highlighted = object == this;
    }

    public select(dispatchEvent:boolean):void {
        super.select(dispatchEvent);
        this.Highlighted = true;
    }

    /**
     * Updates the thumbnail by cloning the content of its slide and removing the unecessary classes.
     */
    public refresh():void {
        var slideElement:JQuery = helper.SlideHelper.getSlideElement(this.Index);
        var slideContent:string = slideElement.html();

        this.thumbnail.html(slideContent);
        this.thumbnail.css('background-color', slideElement.css('background-color'));

        this.thumbnail.find("." + ui.UIKit.draggable).each(function () {
            $(this).removeClass(ui.UIKit.draggable);
        });

        this.thumbnail.find("." + ui.UIKit.dragging).each(function () {
            $(this).removeClass(ui.UIKit.dragging);
        });

        this.thumbnail.find("." + ui.UIKit.resizable).each(function () {
            $(this).removeClass(ui.UIKit.resizable);
        });

        this.thumbnail.find("." + ui.UIKit.selectee).each(function () {
            $(this).removeClass(ui.UIKit.selectee);
        });

        this.thumbnail.find("." + ui.UIKit.selected).each(function () {
            $(this).removeClass(ui.UIKit.selected);
        });

        this.thumbnail.find(UITableView.GRABBER).each(function () {
            $(this).css("opacity", 0);
        });

        this.thumbnail.find(".ui-resizable-handle").remove();
    }

}

/**
 * Every SlideElements has an additional content element where the content of interest is stored.
 */
export class UISlideElementView extends UIView {

    public static TOP = "TOP";         // possible y
    public static BOTTOM = "BOTTOM";   // possible y
    public static LEFT = "LEFT";       // possible x
    public static RIGHT = "RIGHT";     // possible x
    public static MIDDLE = "MIDDLE";   // possible x || y

    /**
     * Container element to display which user has this element selected.
     */
    private selectionsElement:JQuery;

    private slide:UISlideView;
    private z:number;

    constructor(element:JQuery, slide:UISlideView) {
        super(element);
        this.selectionsElement = this.Element.find(".selections");
        this.slide = slide;
        ui.UIKit.enableMultiDraggable(this);
        this.z = parseInt(this.Element.css("z-index"), 10);
    }

    get ContentElement():JQuery {
        return this.Element.children(".content");
    }

    get SlideId():string {
        return this.slide.Id;
    }

    get Slide():UISlideView {
        return this.slide;
    }

    set Alpha(alpha:number) {
        this.ContentElement.css("opacity", alpha);
    }

    get Alpha():number {
        return parseFloat(this.ContentElement.css("opacity"));
    }

    get Z():number {
        return this.z;
    }

    set Z(z:number) {
        this.z = z;
        this.Element.css("z-index", z);
    }

    get X():number {
        return parseInt(this.Element.css("left")) / ui.UIKit.zoomScaleX;
    }

    set X(x:number) {
        this.Element.css("left", x + "px");
    }

    get Y():number {
        return parseInt(this.Element.css("top")) / ui.UIKit.zoomScaleY;
    }

    set Y(y:number) {
        this.Element.css("top", y + "px");
    }

    /**
     * Returns the width + padding
     */
    get Width():number {
        return this.ContentElement.outerWidth();
    }

    /**
     * Set the width. Subtracts the padding of the element from the given width.
     */
    set Width(width:number) {
        var padding = this.Element.innerWidth() - this.Element.width();
        this.Element.width(width - padding);
    }

    /**
     * Returns the height + padding
     */
    get Height():number {
        return this.ContentElement.outerHeight();
    }

    /**
     * Set the height. Subtracts the padding of the element from the given height.
     */
    set Height(height:number) {
        var padding = this.Element.innerHeight() - this.Element.height();
        this.Element.height(height - padding);
    }

    get Bounds():Bounds {
        return {
            x: this.X,
            y: this.Y,
            width: this.Width,
            height: this.Height
        }
    }

    set Bounds(bounds:Bounds) {
        this.X = bounds.x;
        this.Y = bounds.y;
        this.Height = bounds.height;
        this.Width = bounds.width;
    }

    public getRelativeX(position:string):number {
        if (position == UISlideElementView.TOP
            || position == UISlideElementView.LEFT
            || position == UISlideElementView.BOTTOM)
            return this.X;

        if (position == UISlideElementView.MIDDLE)
            return this.X + this.Width / 2;

        if (position == UISlideElementView.RIGHT)
            return this.X + this.Width;

        return null;
    }

    public getRelativeY(position):number {
        if (position == UISlideElementView.TOP
            || position == UISlideElementView.LEFT
            || position == UISlideElementView.RIGHT)
            return this.Y;

        if (position == UISlideElementView.MIDDLE)
            return this.Y + this.Height / 2;

        if (position == UISlideElementView.BOTTOM)
            return this.Y + this.Height;

        return null;
    }

    public toObject():ViewObject {
        var object = <SlideElementViewObject>super.toObject();
        object.bounds = this.Bounds;
        object.z = this.Z;
        object.alpha = this.Alpha;
        return object;
    }

    public updateView(view:SlideElementViewObject):void {
        super.updateView(<ViewObject>view);
        this.Bounds = view.bounds;
        this.Z = view.z;
        this.Alpha = view.alpha;
    }

    /**
     * Removes the element from its slide
     * @param dispatchEvent If true, the ViewWatcher gets notified.
     */
    public remove(dispatchEvent:boolean):void {
        this.Slide.removeElement(this);
        if (dispatchEvent)
            watcher.ViewWatcher.notifyAll(watcher.ViewWatcher.SlideElementRemovedMessage, this);
    }

    public select(dispatchEvent:boolean):void {
        if (!this.isSelected())
            this.addUserToSelectionList(USER_ID);
        super.select(dispatchEvent);
    }

    public unselect(dispatchEvent:boolean):void {
        super.unselect(dispatchEvent);
        this.removeUserFromSelectionList(USER_ID);
    }

    /**
     * Another user selected this element.
     * Put a new element to the selection element.
     */
    public addUserToSelectionList(userId:string):void {
        if (this.userExist(userId))
            return;

        var user = ws.UserPool.get(userId);
        var c = user.color;
        var color = new ui.UIColor(c.r, c.g, c.b, 1);
        this.selectionsElement.append("<div title='" + user.name + "' data-user='" + user.id + "' style='background-color: " + color.toRGBString() + ";' class='userSelection'></div>");
    }

    /**
     * A user has deselected this element.
     * @param userId
     */
    public removeUserFromSelectionList(userId:string):void {
        this.selectionsElement.find(".userSelection[data-user=" + userId + "]").remove();
    }

    private userExist(userId:string):boolean {
        return this.selectionsElement.find(".userSelection[data-user=" + userId + "]").length != 0;

    }

}

export class AbstractUITextView extends UISlideElementView {

    constructor(element, slide:UISlideView) {
        super(element, slide);

        ui.UIKit.enableResizable(this, "e, w");

        this.ContentElement.dblclick((event:JQueryMouseEventObject) => {
            if (this.isEditable())
                return;

            this.startEditing(element);
            var caretRange = getCaretRange(event);
            selectRange(caretRange);
        });

    }

    /**
     * Format the text.
     * @param format
     * @param value
     */
    public format(format:string, value:string):void {
        /**
         * If the element is currently edited we format the selection,
         * else the format gets applied to the element.
         */
        if (this.isEditable())
            formatter.TextFormatter.formatSelection(format, value);
        else {
            ui.UIKit.disableResizable(this.Element);
            formatter.TextFormatter.formatElement(this.ContentElement, format, value);
            ui.UIKit.enableResizable(this, "e, w");
        }
        this.hasChanged();
    }

    public unselect(dispatchEvent:boolean):void {
        super.unselect(dispatchEvent);

        if (this.isEditable())
            this.stopEditing();
    }

    public isEditable():boolean {
        return this.ContentElement.hasClass("editable");
    }

    /**
     * Returns the text of this element
     */
    public getText():string {
        return this.ContentElement.html();
    }

    /**
     * Set the text of the element.
     * @param text
     */
    public setText(text):void {
        this.ContentElement.html(text);
    }

    public stopEditing():void {
        var content = this.ContentElement;
        var html = content.html();
        content.html(html);
        content.focusout(null);
        content.blur();

        ui.UIKit.enableResizable(this, "e,w");
        ui.UIKit.enableMultiDraggable(this);
        ui.UIKit.disableEditable(content);
        this.Slide.onStopEditing();
        this.hasChanged();
    }

    public updateView(view:AbstractTextViewObject):void {
        super.updateView(<SlideElementViewObject>view);
        this.setText(view.text);
    }

    public toObject():ViewObject {
        var object:TextViewObject = <TextViewObject>super.toObject();
        object.type = "abstract";
        object.text = this.getText();
        return object;
    }

    private startEditing(element:JQuery):void {
        ui.UIKit.disableResizable(element);
        ui.UIKit.disableDraggable(element);
        ui.UIKit.enableEditable(this.ContentElement);
        // the focus is important so that the carret can be set at the clicked position.
        this.ContentElement.focus(0);
        this.Slide.onStartEditing(this);
    }

}

export class UIScriptView extends AbstractUITextView implements watcher.KeyboardObserver {

    static displayName:string = "Quellcode";
    static role:string = "script";

    /**
     * The display name of the language.
     */
    private langName:string;

    /**
     * The value of the language.
     */
    private langValue:string;

    /**
     * The css class of the language.
     */
    private langCss:string;

    constructor(element, slide:UISlideView) {
        super(element, slide);
        this.langName = this.Element.data("langname");
        this.langValue = this.Element.data("langvalue");
        this.langCss = this.Element.data("langcss");
        this.Element.css("height", "auto");
        this.highlight();
    }

    get LangValue():string {
        return this.langValue;
    }

    public setLang(langName:string, langValue:string, langCss:string):void {
        this.ContentElement.removeClass(this.langCss);
        this.langCss = langCss;
        this.ContentElement.addClass(this.langCss);

        this.langValue = langValue;
        this.langName = langName;

        this.highlight();
    }

    public stopEditing():void {
        super.stopEditing();
        this.highlight();
    }

    /**
     * Applies the syntax highlighting for the script language.
     */
    private highlight():void {
        this.ContentElement.find("span").contents().unwrap();
        this.ContentElement.removeClass("hljs");
        this.ContentElement.each(function (i, e) {
            hljs.highlightBlock(e)
        });
    }

    /**
     * Insert a tab html entity when tab was pressed.
     */
    public onKeyDown(keyCode:number, event:JQueryKeyEventObject):void {
        if (keyCode == watcher.KeyboardWatcher.TAB && this.isEditable()) {
            event.preventDefault();
            document.execCommand("InsertHTML", false, "&#09;");
        } else
            super.onKeyDown(keyCode, event);
    }

    public updateView(view:ScriptViewObject):void {
        super.updateView(<AbstractTextViewObject>view);
        this.setLang(view.langName, view.langValue, view.langCss);
        this.Element.css("height", "auto");
    }

    public toObject():ViewObject {
        var object:ScriptViewObject = <ScriptViewObject>super.toObject();
        object.type = UIScriptView.role;
        object.text = this.toString();
        object.langValue = this.langValue;
        return object;
    }

    private toString():string {
        this.ContentElement.find("span").contents().unwrap();
        var text = this.ContentElement.html();
        this.highlight();
        return text;
    }

}

export class UITextView extends AbstractUITextView {
    static displayName:string = "Textfeld";
    static role:string = "text";

    constructor(element, slide:UISlideView) {
        super(element, slide);
    }

    public toObject():ViewObject {
        var object:TextViewObject = <AbstractTextViewObject>super.toObject();
        object.type = UITextView.role;
        return object;
    }

}

export class UIImageView extends UISlideElementView {

    static displayName:string = "Bild";
    static role:string = "image";

    constructor(element, slide:UISlideView) {
        super(element, slide);
        ui.UIKit.enableResizable(this, "n, e, s, w, ne, se, sw, nw");
    }

    get OriginalHeight():number {
        return this.Element.data("height");
    }

    get OriginalWidth():number {
        return this.Element.data("width");
    }

    public isPortrait():boolean {
        return this.OriginalHeight > this.OriginalWidth;
    }

    get AspectRatio():number {
        return this.OriginalWidth / this.OriginalHeight;
    }

    get Width():number {
        return this.Element.innerWidth();
    }

    set Width(width:number) {
        var padding = this.Element.innerWidth() - this.Element.width();
        this.Element.width(width - padding);
    }

    /**
     * Returns the height + padding
     */
    get Height():number {
        return this.Element.innerHeight();
    }

    /**
     * Set the height. Subtracts the padding of the element from the given height.
     */
    set Height(height:number) {
        var padding = this.Element.innerHeight() - this.Element.height();
        this.Element.height(height - padding);
    }

    public toObject():ViewObject {
        var object:ImageViewObject = <ImageViewObject>super.toObject();
        object.type = UIImageView.role;
        return object;
    }
}

class UITableRow {

    public static ROW = ".content-row";

    private cells:UITableCell[] = [];

    private element:JQuery;

    constructor(element:JQuery) {
        this.element = element;

        this.Element.find(UITableCell.CELL).each((index:number, element:Element)=> {
            this.cells.push(new UITableCell($(element), this));
        });
    }

    get Cells():UITableCell[] {
        return this.cells;
    }

    get Element():JQuery {
        return this.element;
    }

    public getMinHeight():number {
        var height = 0;

        this.Cells.forEach((cell:UITableCell)=> {
            height = Math.max(height, cell.ContentHeight);
        });

        return Math.max(height, UITableCell.CELL_MIN_HEIGHT);
    }

    public toObject():TableRow {
        var tableRow:TableRow = <TableRow>{};
        tableRow.cells = [];

        this.Cells.forEach((cell:UITableCell)=> {
            tableRow.cells.push(cell.toObject());
        });

        return tableRow;
    }

}

class UITableCell {

    private element:JQuery;
    private row:UITableRow;

    private onSelected:()=>void;
    private onEditing:()=>void;

    public static CELL = ".content-cell";
    public static CONTENT = ".content";

    public static ALIGN_MIDDLE:string = "align_middle";
    public static ALIGN_TOP:string = "align_top";
    public static ALIGN_BOTTOM:string = "align_bottom";

    public static CELL_MIN_HEIGHT = 25;
    public static CELL_MIN_WIDTH = 30;

    constructor(element:JQuery, row:UITableRow) {
        this.element = element;
        this.row = row;

        this.Element.mousedown(()=> {
            if (this.isSelected())
                return;

            this.Element.addClass(ui.UIKit.selected);

            if (!this.isEditable())
                this.onSelected();
        });

        this.Element.dblclick((event) => {
            if (this.isEditable())
                return;

            this.Element.addClass(ui.UIKit.editable);
            var cellContent = this.Element.find(UITableCell.CONTENT);
            cellContent.attr("contenteditable", "true");
            cellContent.focus(0);
            selectRange(getCaretRange(event));

            this.onEditing();
        });
    }

    public isSelected():boolean {
        return this.Element.hasClass(ui.UIKit.selected);
    }

    public unselect():void {
        this.Element.removeClass(ui.UIKit.selected);
        this.stopEditing();
    }

    set OnSelected(p:()=>void) {
        this.onSelected = p;
    }

    set OnEditing(p:()=>void) {
        this.onEditing = p;
    }

    get Row():UITableRow {
        return this.row;
    }

    get Element():JQuery {
        return this.element;
    }

    get ContentHeight():number {
        return this.Element.find(UITableCell.CONTENT).height();
    }

    get ContentElement():JQuery {
        return this.Element.find(UITableCell.CONTENT);
    }

    get Height():number {
        return parseInt(this.Element.css("height"), 10);
    }

    set Height(height:number) {
        this.Element.css({
            height: height + "px"
        });
    }

    get Width():number {
        return parseInt(this.Element.css("width"), 10);
    }

    set Width(width:number) {
        this.Element.css({
            width: width + "px"
        });
    }

    public stopEditing():void {
        if (!this.isEditable())
            return;

        this.Element.removeClass(ui.UIKit.editable);
        var cellContent = this.Element.find(UITableCell.CONTENT);
        cellContent.attr("contenteditable", "false");
        // removes the blinking cursor
        cellContent.blur();
    }

    public isEditable():boolean {
        return this.Element.hasClass(ui.UIKit.editable);
    }

    private getStyles():Object {
        return {
            "background-color": <BackgroundColorComponent>{
                type: "background-color",
                color: ui.UIColor.fromString(this.Element.css("background-color")).toObject()
            },
            "border": <BorderComponent>{
                type: "border",
                width: parseInt(this.Element.css("border-right-width"), 10),
                color: ui.UIColor.fromString(this.Element.css("border-right-color")).toObject(),
                style: this.Element.css("border-right-style")
            },
            "color": <FontColorComponent>{
                type: "color",
                color: ui.UIColor.fromString(this.Element.css("color")).toObject()
            },
            "font-size": <FontSizeComponent>{
                type: "font-size",
                fontSize: parseInt(this.Element.css("font-size"), 10)
            }
        }
    }

    public alignContent():void {

    }

    get Value():string {
        return this.Element.find(UITableCell.CONTENT).html();
    }

    set Value(value:string) {
        this.Element.find(UITableCell.CONTENT).html(value);
    }

    public toObject():TableCell {
        return <TableCell>{
            value: this.Value,
            styles: this.getStyles(),
            dimension: {
                height: this.Height,
                width: this.Width
            }

        };
    }

}

export class UITableView extends UISlideElementView {

    public static displayName:string = "Table";
    public static role:string = "table";
    public static Tooltip:JQuery = $("body").find(".resizeTooltip");

    public static GRABBER_ROW = ".grabber-row";
    public static ROW_GRABBER = ".row-grabber";
    public static COL_GRABBER = ".col-grabber";
    public static GRABBER = ".grabber";

    private handleTarget:JQuery = null;
    private borderWidth:number = 1;
    private table:JQuery = null;
    private selectedCell:UITableCell = null;
    private rows:UITableRow[] = [];

    constructor(element:JQuery, slide:UISlideView) {
        super(element, slide);

        this.table = this.ContentElement.find("table");

        this.ContentElement.find(UITableRow.ROW).each((index:number, element:Element) => {
            this.rows.push(new UITableRow($(element)));
        });

        this.enableCellResizable(this.ContentElement.find(UITableCell.CELL));

        this.rows.forEach((row:UITableRow)=> {
            row.Cells.forEach((cell:UITableCell)=> {
                this.bindCell(cell);
            })
        });

        this.bindGrabber();

        // Set the col grabber widht's to col width
        var widths = [];
        this.ContentElement.find(UITableRow.ROW).first().find(UITableCell.CELL).each((index:number, element:Element) => {
            widths.push($(element).width());
        });
        this.ContentElement.find(UITableView.GRABBER_ROW).find(UITableView.COL_GRABBER).each((index:number, element:Element) => {
            $(element).width(widths[index]);
        });

        // set the row grabber heights to row heights
        this.ContentElement.find(UITableRow.ROW).each((index:number, element:Element)=> {
            var jqElement = $(element);
            var height = jqElement.find(UITableCell.CELL).first().height();
            jqElement.find(UITableView.ROW_GRABBER).height(height);
        });

        this.updateTableSize();

        this.rows.forEach((row:UITableRow)=> {
            row.Cells.forEach((cell:UITableCell)=> {
                this.alignCellVerticalMiddle(cell);
            })
        });

        this.ContentElement.find(UITableView.ROW_GRABBER).each((index, element) => {
            this.layoutRowGrabberCell($(element));
        });
    }

    public select(dispatchEvent:boolean):void {
        super.select(dispatchEvent);
    }

    public unselect(dispatchEvent:boolean):void {
        super.unselect(dispatchEvent);
        this.ContentElement.find(UITableCell.CELL).removeClass(ui.UIKit.selected);
        this.stopCellEditing();
    }

    public isEditing():boolean {
        if (this.selectedCell == null)
            return false;

        return this.selectedCell.isEditable();
    }

    /**
     * .grabber element
     * @param cell
     */
    private layoutRowGrabberCell(cell:JQuery):void {
        var innerCell = cell.find(".inner");

        var spanElement = innerCell.find(".label");
        var top = (innerCell.height() - spanElement.height()) / 2;
        spanElement.css("top", top + "px");
    }

    private enableCellResizable(cell:JQuery):void {
        cell.resizable({
            handles: "e, s",
            minWidth: UITableCell.CELL_MIN_WIDTH,
            start: (event:Event, ui:JQueryUI.ResizableUIParams) => {
                this.handleTarget = $((<any>event).originalEvent.target);
                this.stopCellEditing();
                UITableView.Tooltip.show();
            },
            resize: (event:Event, ui:JQueryUI.ResizableUIParams) => {
                this.onResize(event, ui);
            },
            stop: (event:Event, ui:JQueryUI.ResizableUIParams) => {
                this.onResized(event, ui);
                UITableView.Tooltip.hide();
            }
        });

        var that:UITableView = this;

        this.ContentElement.find(UITableCell.CELL + " .ui-resizable-handle").hover(
            function () { // in
                that.handleResizeHover($(this), true);
            },
            function () { // out
                that.handleResizeHover($(this), false);
            }
        );
    }

    private updateTableSize():void {
        // Adjust the width of the table root element
        var width = 0;
        this.ContentElement.find(UITableRow.ROW).first().find(".cell").each(function () {
            width += $(this).outerWidth(true);
        });

        this.Element.css({
            width: width
        });
    }

    private onResize(event:Event, ui:JQueryUI.ResizableUIParams):void {
        var rowIndex = ui.element.closest(UITableRow.ROW).index() - 1; // first row = grabber row
        var columnIndex = ui.element.closest(UITableCell.CELL).index() - 1; // first element = grabber col

        /** Resize width */
        if (this.handleTarget.hasClass("ui-resizable-e")) {

            // Adjust cell width
            this.rows.forEach((row:UITableRow)=> {
                var cell:UITableCell = row.Cells[columnIndex];
                cell.Element.width(ui.size.width);
                this.layoutRow(cell);
                this.alignCellVerticalMiddle(cell);
            });

            this.updateTableSize();

            // Adjust grabber width
            var selector = UITableView.GRABBER_ROW + " " + UITableView.COL_GRABBER + ":nth-child(" + (columnIndex + 2) + ")";
            this.ContentElement.find(selector).each((index:number, element:Element) => {
                $(element).width(ui.size.width);
            });

            UITableView.Tooltip.html(ui.size.width);

            this.ContentElement.find(UITableCell.CELL).resizable("option", "minHeight", UITableCell.CELL_MIN_HEIGHT);
        }

        /** Resize height */
        if (this.handleTarget.hasClass('ui-resizable-s')) {

            var row = this.rows[rowIndex];

            row.Cells.forEach((cell:UITableCell)=> {
                cell.Element.height(ui.size.height);
                this.alignCellVerticalMiddle(cell);
            });

            this.updateRowGrabberHeight(rowIndex, ui.size.height);

            UITableView.Tooltip.html(ui.size.height);

            this.ContentElement.find(UITableCell.CELL).resizable("option", "minHeight", row.getMinHeight());
        }

        UITableView.Tooltip.css({
            left: common.System.Mouse.x - UITableView.Tooltip.width() / 2,
            top: common.System.Mouse.y + UITableView.Tooltip.height() / 2
        });
    }

    private updateRowGrabberHeight(index, height):void {
        var selector = UITableRow.ROW + ":nth-child(" + (index + 2) + ") " + UITableView.GRABBER;
        this.ContentElement.find(selector).each((index, element) => {
            var grabber:JQuery = $(element);
            grabber.height(height);
            this.layoutRowGrabberCell(grabber);
        });
    }

    private onResized(event:Event, ui:JQueryUI.ResizableUIParams):void {
        this.onResize(event, ui);
        this.hasChanged();
    }

    private columnSize():number {
        return this.rows[0].Cells.length;
    }

    private rowSize():number {
        return this.rows.length;
    }

    private addRow(addElement:JQuery):void {
        this.beforeTableSizeChange();
        //TODO:: implement
        this.onTableChangedSize();
    }

    private removeRow(removeElement:JQuery):void {
        if (this.rowSize() == 1)
            return;

        this.beforeTableSizeChange();
        //TODO:: implement
        this.onTableChangedSize();
    }

    private addCol(addElement):void {
        this.beforeTableSizeChange();
        //TODO:: implement
        this.onTableChangedSize();
    }

    private removeCol(removeElement):void {
        if (this.columnSize() == 1)
            return;

        this.beforeTableSizeChange();
        //TODO:: implement
        this.onTableChangedSize();
    }

    private bindGrabber():void {
        this.ContentElement.find(".grabber .inner .button").unbind();
        this.ContentElement.find(".grabber").unbind();

        var that:UITableView = this;

        this.ContentElement.find(UITableView.ROW_GRABBER + " .inner .add").click(function () {
            that.addRow($(this));
        });

        this.ContentElement.find(UITableView.ROW_GRABBER + " .inner .remove").click(function () {
            that.removeRow($(this));
        });

        this.ContentElement.find(UITableView.COL_GRABBER + " .inner .add").click(function () {
            that.addCol($(this));
        });

        this.ContentElement.find(UITableView.COL_GRABBER + " .inner .remove").click(function () {
            that.removeCol($(this));
        });
    }

    /**
     * .content-cell element
     * @param cell
     */
    private bindCell(cell:UITableCell):void {

        cell.OnSelected = () => {
            if (this.ContentElement.find(UITableCell.CELL + " .ui-resizable-handle.hover").length > 0) {
                this.ContentElement.find(UITableCell.CELL).removeClass(ui.UIKit.selected);
                return;
            }

            if (this.selectedCell != null) {
                if (this.selectedCell.isEditable())
                    this.stopCellEditing();

                this.selectedCell.unselect();
            }

            this.selectedCell = cell;
        };

        cell.OnEditing = () => {
            this.table.addClass(ui.UIKit.editable);
            var cellContent = this.selectedCell.ContentElement;

            var oldHeight = cellContent.height();
            cellContent.keyup(() => {
                if (oldHeight != cellContent.height()) {
                    this.layoutRow(cell);
                    oldHeight = cellContent.height();
                }
                this.alignCellVerticalMiddle(cell);
                this.ContentElement.find(UITableCell.CELL).resizable("option", "minHeight", cell.Row.getMinHeight());
            });

            ui.UIKit.disableDraggable(this.Element);

            this.Slide.onStartEditing(this);
        };

    }

    private stopCellEditing():void {
        if (this.selectedCell == null)
            return;

        this.selectedCell.stopEditing();
        this.table.removeClass(ui.UIKit.editable);
        this.alignCellVerticalMiddle(this.selectedCell);
        ui.UIKit.enableMultiDraggable(this);
        this.hasChanged();
        this.Slide.onStopEditing();
    }

    private beforeTableSizeChange():void {
        this.stopCellEditing();
    }

    private onTableChangedSize():void {
        this.ContentElement.find(UITableCell.CELL).removeClass(ui.UIKit.selected);

        this.ContentElement.find(UITableRow.ROW).each(function (index, element) {
            $(element).find(UITableView.ROW_GRABBER + " .label").html("" + index + 1);
        });

        this.ContentElement.find(UITableView.GRABBER_ROW + " " + UITableView.COL_GRABBER).each((index, element) => {
            var alphabeticIndex = this.numberToChar(index + 1);
            $(element).find(".label").html(alphabeticIndex);
        });

        this.bindGrabber();

        this.hasChanged();
    }

    private numberToChar(n):string {
        var mod = n % 26;
        var pow = n / 26 | 0;
        var out = mod ? String.fromCharCode(64 + mod) : (pow--, 'Z');
        return pow ? this.numberToChar(pow) + out : out;
    }

    private handleResizeHover(handlerElement:JQuery, show:boolean):void {
        if (handlerElement.hasClass("ui-resizable-e")) {
            var columnIndex = handlerElement.closest(UITableCell.CELL).index() + 1;
            this.ContentElement.find(UITableRow.ROW).each(function (index, element) {
                var handler = $(element).find(UITableCell.CELL + ":nth-child(" + columnIndex + ") .ui-resizable-e");

                if (show)
                    handler.addClass("hover");
                else
                    handler.removeClass("hover");
            });
        }

        if (handlerElement.hasClass("ui-resizable-s")) {
            var row = handlerElement.closest(UITableRow.ROW);
            var handler = row.find(".ui-resizable-s");

            if (show)
                handler.addClass("hover");
            else
                handler.removeClass("hover");
        }
    }

    private layoutRow(cell:UITableCell):void {
        var row:UITableRow = cell.Row;

        var bestHeight = 0;

        row.Cells.forEach((cell:UITableCell)=> {
            if (cell.Height < cell.ContentHeight)
                bestHeight = Math.max(cell.ContentHeight, bestHeight);
        });

        if (bestHeight != 0) {
            row.Cells.forEach((cell:UITableCell)=> {
                cell.Height = bestHeight;
                this.alignCellVerticalMiddle(cell);
            });

            this.updateRowGrabberHeight(row.Element.index() - 1, bestHeight);
            this.ContentElement.find(UITableCell.CELL).resizable("option", "minHeight", row.getMinHeight());
            this.layoutRowGrabberCell(row.Element.find(UITableView.ROW_GRABBER));
        }

    }

    private alignCellVerticalMiddle(cell:UITableCell):void {
        var contentHeight = cell.ContentHeight;
        var offset = (cell.Height - contentHeight) / 2;

        if (offset < 0)
            offset = 0;

        cell.ContentElement.css({
            top: offset + "px"
        });
    }

    public toObject():ViewObject {
        var rows:TableRow[] = [];

        this.rows.forEach((row:UITableRow)=> {
            rows.push(row.toObject());
        })

        var object:TableViewObject = <TableViewObject>super.toObject();
        object.type = UITableView.role;
        object.rows = rows;
        return object;
    }

    public updateView(view:SlideElementViewObject):void {
        super.updateView(view);
        var tableView = <TableViewObject>view;
        //TODO:: update table content
    }
}