///<reference path="../definitions/references.d.ts" />

/**
 * Module is responsible to show detailed information - "properties" - of selected slide elements.
 */

import view = require("src/view");
import ui = require("src/ui");
import watcher = require("src/watcher");
import helper = require("src/helper");
import net = require("src/net");
import ws = require("src/ws");

/**
 * Panel to control the properties of a selected UISlideElementView.
 * Dynamically displays the right properties for the concrete UISlideElementView class.
 */
export class SlideElementPropertyPanel implements watcher.SelectionObserver, watcher.ViewObserver {

    private window:JQuery = $(".window.element");
    private boundsPanel:BoundsWidget = new BoundsWidget();
    private appearancePanel:AppearanceWidget = new AppearanceWidget();
    private imageViewPanel:ImageViewWidget = new ImageViewWidget();
    private scriptViewPanel:ScriptViewWidget = new ScriptViewWidget();
    private selectedView:view.UISlideElementView;

    constructor() {
        watcher.SelectionWatcher.register(this);
        watcher.ViewWatcher.register(this);

        ws.Socket.on(ws.inc.OnSlideElementRemoved, (message:ws.IncomingMessage)=> {
            if (this.selectedView == null)
                return;

            if ((<ws.OnSlideElementRemovedMessage>message).elementId == this.selectedView.Id)
                this.window.hide()
        });
    }

    /**
     * Set the name of the property panel
     */
    set Title(title:string) {
        this.window.find(".title .name").html(title);
    }

    /**
     * Set the css class to display the icon of the selected slide element.
     */
    set Icon(cssClass:string) {
        var icon = this.window.find(".title .elementIcon");
        icon.removeClass(view.UIImageView.role);
        icon.removeClass(view.UITextView.role);
        icon.addClass(cssClass);
    }

    /**
     * Hide the panel, if the selected slide element or slide was removed.
     * Currently only the selected element can be deleted so we do not need to check ids here.
     */
    public update(message:string, object:any):void {
        switch (message) {
            case watcher.ViewWatcher.SlideElementRemovedMessage:
            case watcher.ViewWatcher.SlideRemovedMessage:
                this.window.hide();
                break;
        }
    }

    /**
     * If a SlideElement was selected, we update the panel and display the right properties.
     */
    public onSelectionMessage(message:string, object:any):void {
        if (message == watcher.SelectionWatcher.SelectedEventMessage || message == watcher.SelectionWatcher.AddToSelectionMessage) {

            if (object instanceof view.UISlideElementView) {
                this.selectedView = object;
                this.window.show();
            }

            this.boundsPanel.HeightSpinnerEnabled = true;
            this.boundsPanel.MinWidth = null;

            if (object instanceof view.UITextView)
                this.showTextViewProperties();
            else if (object instanceof view.UIImageView)
                this.showImageViewProperties();
            else if (object instanceof view.UIScriptView)
                this.showScriptView();

        }

        if (message == watcher.SelectionWatcher.NoElementSelectedMessage)
            this.window.hide();

        $("input").each(function () {
            if ($(this).is(":focus"))
                $(this).blur();
        });
    }

    /**
     * Display an UIScriptView.
     */
    private showScriptView():void {
        this.Title = view.UIScriptView.displayName;
        this.Icon = view.UIScriptView.role;
        this.boundsPanel.HeightSpinnerEnabled = false;
    }

    /**
     * Display an UITextView.
     */
    private showTextViewProperties():void {
        this.Title = view.UITextView.displayName;
        this.Icon = view.UITextView.role;
        this.boundsPanel.HeightSpinnerEnabled = false;
        this.boundsPanel.MinWidth = 72;
    }

    /**
     * Display an UIImageView.
     */
    private showImageViewProperties():void {
        this.Title = view.UIImageView.displayName;
        this.Icon = view.UIImageView.role;
    }
}

/**
 * Panel to control the background color of a slide.
 */
export class SlidePropertyPanel {

    private slideContainer:ui.UISlideContainer;

    /**
     * Element, which shows the current background color of the current slide.
     */
    private backgroundColorField:ColorField;

    constructor(slideContainer:ui.UISlideContainer) {
        this.slideContainer = slideContainer;

        this.backgroundColorField = new ColorField($(".window.slide .ui-color-field .color"), false);

        this.backgroundColorField.onChange((c:ui.UIColor)=> {
            this.slideContainer.SelectedSlide.BackgroundColor = c;
        });

        this.backgroundColorField.onStop((c:ui.UIColor)=> {
            this.slideContainer.SelectedSlide.BackgroundColor = c;
            this.slideContainer.SelectedSlide.hasChanged();
        });

    }

    public update():void {
        this.backgroundColorField.updateColor(this.slideContainer.SelectedSlide.BackgroundColor);
    }
}

/**
 * Panel, that shows all connected users.
 */
export class UIConnectionPanel {

    private element:JQuery = $(".connectedUsersPanel");
    private users:UserInfoWidget[] = [];

    constructor() {
        for (var i = 0; i < ws.UserPool.users().length; i++)
            this.users.push(new UserInfoWidget(ws.UserPool.users()[i], this.UserPanel));

        ws.Socket.on(ws.inc.UserJoined, (message:ws.IncomingMessage)=> {
            this.userJoined(<ws.UserJoinedMessage>message);
        });

        ws.Socket.on(ws.inc.UserLeft, (message:ws.IncomingMessage)=> {
            this.userLeft(<ws.UserLeftMessage>message);
        });
    }

    get UserPanel():JQuery {
        return this.element.find(".users");
    }

    /**
     * Returns an UserInfoWidget for a given id, or null if no user was found.
     */
    private getUserInfo(userId:string):UserInfoWidget {
        for (var i = 0; i < this.users.length; i++)
            if (this.users[i].Id == userId)
                return this.users[i];
        return null;
    }

    /**
     * Add an UserInfoWidget to the connected users container.
     */
    private userJoined(message:ws.UserJoinedMessage):void {
        if (this.getUserInfo(message.user.id) == null)
            this.users.push(new UserInfoWidget(message.user, this.UserPanel));
    }

    /**
     * Removes an UserInfoWidget from the connected users container.
     */
    private userLeft(message:ws.UserLeftMessage):void {
        var userInfo:UserInfoWidget = this.getUserInfo(message.userId);
        userInfo.remove();
        this.users.remove(userInfo);
    }

}

/**
 * Handle file drop on the slide container and queued upload.
 * Display information about file uploads.
 */
export class UploadPanel {

    private dropOverlay:JQuery = $(".uiDropoverlay");

    private element:JQuery = $(".uiSlideContainer .uploads");
    private speedLabel:JQuery;
    private slideContainer:ui.UISlideContainer;

    /** Queued uploads. */
    private uploads:UploadBarWidget[] = [];

    constructor(slideContainer:ui.UISlideContainer) {
        this.slideContainer = slideContainer;
        this.speedLabel = this.element.find(".speed");

        helper.SlideHelper.slideContainer.on("dragenter", (e) => {
            e.stopPropagation();
            e.preventDefault();
        });

        helper.SlideHelper.slideContainer.on("dragover", (e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dropOverlay.addClass("dragover");
        });

        helper.SlideHelper.slideContainer.on("dragleave", (e) => {
            e.stopPropagation();
            e.preventDefault();
        });

        this.dropOverlay.on("drop", (e:JQueryEventObject) => {
            e.preventDefault();
            this.dropOverlay.removeClass("dragover");
            this.onDrop(e);
        });

        $(document).on("dragenter", (e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dropOverlay.addClass("dragtarget");
            this.dropOverlay.removeClass("dragover");
        });

        $(document).on("dragover", (e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dropOverlay.addClass("dragtarget");
        });

        $(document).on("dragleave", (e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dropOverlay.removeClass("dragtarget");
        });

        $(document).on("drop", (e) => {
            e.stopPropagation();
            e.preventDefault();
            this.dropOverlay.removeClass("dragtarget");
        });
    }

    /**
     * Uploads a given array of File objects.
     */
    public uploadFiles(files:File[]) {
        this.element.show();

        var slideId = this.slideContainer.SelectedSlide.Id;
        for (var i = 0; i < files.length; i++)
            if (this.checkFile(files[i]))
                this.uploads.push(new UploadBarWidget(files[i], this.element, slideId));
        this.uploadNext();
    }

    /**
     * Something was dropped on the slide container.
     */
    private onDrop(e):void {
        var files = <File[]>(<any>(e.originalEvent)).dataTransfer.files;
        this.uploadFiles(files);
    }

    /**
     * Uploads the next file.
     */
    private uploadNext():void {
        if (this.uploads.length == 0) {
            this.element.hide();
            return;
        }

        var upload = this.uploads[0];
        upload.startUpload(() => {
            ui.Flash.make("Upload completed: " + upload.Filename);
            this.uploads.remove(upload);
            this.uploadNext();
        });
    }

    /**
     * Currently only images are allowed for upload.
     */
    private checkFile(file:File):boolean {
        return net.FileManager.isImage(file);
    }

}

/**
 * Widget that represents the upload of one file.
 * Displays a upload bar and size/progress information.
 */
class UploadBarWidget {

    /**
     * Corresponding File which should be uploaded.
     */
    private file:File;

    /**
     * Root element.
     */
    private element:JQuery;

    /**
     * The id of the slide, where the file is being uploaded.
     */
    private slideId:string;

    constructor(file:File, parent:JQuery, slideId:string) {
        this.file = file;
        this.slideId = slideId;
        this.element = parent.find(".clone.upload").clone(false);
        this.element.removeClass("clone");
        parent.append(this.element);
        this.element.show();
        this.element.find(".barwrapper").hide();
        this.element.find(".filename").html(file.name);
        this.element.find(".totalsize").html(this.toKb(file.size));
    }

    /**
     * Starts the upload of the file.
     */
    public startUpload(finished:()=>void):void {
        this.element.find(".barwrapper").show();

        net.FileManager.uploadSlideFile(
            this.file,
            this.slideId,
            (response:Object) => {
                this.element.find(".uploadedsize").html(this.toKb(this.file.size));
                this.element.find(".bar").css("width", "100%");
                this.element.remove();
                finished();
            },
            (progress:number, loaded:number) => {
                this.element.find(".uploadedsize").html(this.toKb(loaded));
                this.element.find(".bar").css("width", progress + "%");
            }
        );
    }

    get Filename() {
        return this.file.name;
    }

    /**
     * Converts byte number to kilobyte string.
     */
    private toKb(byte:number):string {
        return Math.round(byte / 1000).toString();
    }

}

/**
 * Widget that displays a connected user with their color in the connection info panel
 */
export class UserInfoWidget {

    private element:JQuery;
    private user:ws.User;

    constructor(user:ws.User, parent:JQuery) {
        this.element = $(user.view).appendTo(parent);
        this.user = user;
    }

    public remove():void {
        this.element.remove();
    }

    /**
     * Returns the id of the user.
     */
    get Id():string {
        return this.user.id;
    }
}

/**
 * Wrapper class for JQueryUI spinner.
 */
class PropertySpinnerWidget {

    /**
     * Root element.
     */
    private element:JQuery;

    /**
     * Indicates if a spin was started.
     */
    private spinStart:boolean;

    constructor(element:JQuery, onChange:Function, onStop:Function) {
        this.element = element;
        this.element.spinner({
                stop: (event, ui) => {
                    if (this.spinStart)
                        onStop(this.getValue(event, ui));
                    this.spinStart = false;
                },
                spin: (event, ui) => {
                    this.spinStart = true;
                    onChange(this.getValue(event, ui));
                },
                change: (event, ui) => {
                    if (!this.spinStart)
                        onStop(this.getValue(event, ui));
                }
            }
        );

    }

    private getValue(event, ui):number {
        var value = event.target.value;

        if (typeof ui.value != "undefined")
            value = ui.value;
        return value;
    }

    /**
     * Set the minimum value of the spinner.
     */
    set Min(min:number) {
        this.element.spinner("option", "min", min);
    }

    /**
     * Set the maximum value of the spinner.
     */
    set Max(max:number) {
        this.element.spinner("option", "max", max);
    }

    /**
     * Set the value of the spinner.
     */
    set Value(value:number) {
        this.element.spinner("value", value);
    }

    /**
     * Get the current value of the spinner.
     */
    get Value():number {
        return <any>this.element.spinner("value");
    }

    /**
     * Enable the spinner.
     */
    public enable():void {
        this.element.spinner("enable");
    }

    /**
     * Disable the spinner.
     */
    public disable():void {
        this.element.spinner("disable");
    }
}

/**
 * Base class for all slide element property widgets.
 * Also responsible for updating the shown properties.
 */
class ElementPropertyWidget implements watcher.ViewObserver, watcher.SelectionObserver {

    /**
     * Root panel element.
     */
    private panel:JQuery;

    /**
     * SlideElement to display the properties for.
     */
    private selectedView:view.UISlideElementView;

    /**
     * Field, to indicate if the values of jQueryUI widgets are currently updated.
     * Necessary, to prevent event looping with the changed method from jQuery.
     */
    public updatingValues:boolean;

    constructor(element) {
        this.Panel = element;
        watcher.ViewWatcher.register(this);
        watcher.SelectionWatcher.register(this);

        ws.Socket.on(ws.inc.OnSlideElementChanged, (message:ws.IncomingMessage)=> {
            if (this.SelectedView == null)
                return;

            if ((<ws.OnSlideElementChangedMessage>message).view.id == this.SelectedView.Id) {
                this.updatingValues = true;
                this.updateFromConnection(<ws.OnSlideElementChangedMessage>message);
                this.updatingValues = false;
            }
        });
    }

    public onSelectionMessage(message:String, object:any):void {
        if (object instanceof view.UISlideElementView)
            if (message == watcher.SelectionWatcher.SelectedEventMessage || message == watcher.SelectionWatcher.AddToSelectionMessage)
                this.show(<view.UISlideElementView>object);

        if (message == watcher.SelectionWatcher.NoElementSelectedMessage)
            this.hide();
    }

    public update(message:string, object:any):void {
        if (this.SelectedView == null)
            return;

        if (object instanceof view.UISlideElementView)
            if (message == watcher.ViewWatcher.ChangedEventMessage && object.Id == this.SelectedView.Id)
                this.updateValues();
    }

    get Panel():JQuery {
        return this.panel;
    }

    set Panel(element:JQuery) {
        this.panel = element;
    }

    /**
     * Returns the selected view
     */
    get SelectedView():view.UISlideElementView {
        return this.selectedView;
    }

    /**
     * Set the view, which values should be displayed.
     */
    set SelectedView(view:view.UISlideElementView) {
        this.selectedView = view;
    }

    /**
     * Hides the panel.
     */
    public hide():void {
        this.Panel.hide();
    }

    /**
     * Shows the property panel and update the values of the property elements.
     */
    public show(view:view.UISlideElementView):void {
        this.SelectedView = view;
        this.updateValues();
        this.Panel.show();
    }

    /**
     * Update the values of the property elements.
     * Called when a different SlideElement was selected, of a element has changed. (example: it was dragged)
     */
    public updateValues():void {
        this.updatingValues = true;
        this.setValues();
        this.updatingValues = false;
    }

    /**
     * <<Abstract Method>>
     * Set the correct values to the property elements.
     * When this method is called, updatingValues is true.
     */
    public setValues():void {
        console.error("Abstract Method");
    }

    /**
     * <<Abstract Method>>
     * Update the values for the selected SlideElement with values from an OnSlideElementChangedMessage.
     * When this method is called, updatingValues is true.
     */
    public updateFromConnection(message:ws.OnSlideElementChangedMessage):void {
        console.error("Abstract Method");
    }
}

/**
 * Control the appearance of the selected UISlideElementView.
 * Controls the opacity and background color for the selected element.
 */
class AppearanceWidget extends ElementPropertyWidget {

    /**
     * Spinner element to control the opacity of the selected element.
     */
    private propertyAlphaSpinner:PropertySpinnerWidget;

    /**
     * Element, which shows the current background color of the selected element.
     */
    private backgroundColorField:ColorField;

    constructor() {
        super($(".panel.appearance"));

        this.backgroundColorField = new ColorField(this.Panel.find(".ui-color-field .color"), true);

        this.backgroundColorField.onChange((c:ui.UIColor)=> {
            this.SelectedView.BackgroundColor = c;
        });

        this.backgroundColorField.onStop((c:ui.UIColor)=> {
            this.SelectedView.BackgroundColor = c;
            this.SelectedView.hasChanged();
        });

        this.propertyAlphaSpinner = new PropertySpinnerWidget(
            this.Panel.find(".propertyAlphaSpinner"),
            (value) => { // change
                this.alphaSpinnerChanged(this.SelectedView, value);
            },
            (value) => { // stop
                if (this.updatingValues)
                    return;
                this.alphaSpinnerChanged(this.SelectedView, value);
                this.SelectedView.hasChanged();
            }
        );

        this.propertyAlphaSpinner.Max = 100;
        this.propertyAlphaSpinner.Min = 0;
    }

    private alphaSpinnerChanged(view:view.UISlideElementView, value) {
        view.Alpha = value / 100;
    }

    public setValues():void {
        this.updateWidget(this.SelectedView.BackgroundColor, this.SelectedView.Alpha);
    }

    public updateFromConnection(message:ws.OnSlideElementChangedMessage):void {
        for (var key in message.view.styles)
            if (key == "background-color")
                this.updateWidget(ui.UIColor.fromRGBA((<BackgroundColorComponent>message.view.styles[key]).color), message.view.alpha);
    }

    private updateWidget(color:ui.UIColor, alpha) {
        this.backgroundColorField.updateColor(color);
        this.propertyAlphaSpinner.Value = alpha * 100;
    }

}

/**
 * Control position and size of the selected UISlideElementView.
 */
class BoundsWidget extends ElementPropertyWidget {

    private propertyWidthSpinner:PropertySpinnerWidget;
    private propertyHeightSpinner:PropertySpinnerWidget;
    private propertyXSpinner:PropertySpinnerWidget;
    private propertyYSpinner:PropertySpinnerWidget;

    public widthEnabled:boolean = true;
    public heightEnabled:boolean = true;
    public xEnabled:boolean = true;
    public yEnabled:boolean = true;

    constructor() {
        super($(".bounds.panel"));

        this.propertyWidthSpinner = new PropertySpinnerWidget(
            this.Panel.find(".propertyWidthSpinner"),
            (value) => { // change
                this.onWidthChange(this.SelectedView, value);
            },
            (value) => { // stop
                if (this.updatingValues)
                    return;
                this.onWidthChange(this.SelectedView, value);
                this.SelectedView.hasChanged();
            }
        );

        this.propertyHeightSpinner = new PropertySpinnerWidget(
            this.Panel.find(".propertyHeightSpinner"),
            (value) => { // change
                this.onHeightChange(this.SelectedView, value);
            },
            (value) => { // stop
                if (this.updatingValues)
                    return;
                this.onHeightChange(this.SelectedView, value);
                this.SelectedView.hasChanged();
            }
        );

        this.propertyXSpinner = new PropertySpinnerWidget(
            this.Panel.find(".propertyXSpinner"),
            (value) => { // change
                this.onXChange(this.SelectedView, value);
            },
            (value) => { // stop
                if (this.updatingValues)
                    return;
                this.onXChange(this.SelectedView, value);
                this.SelectedView.hasChanged();
            }
        );

        this.propertyYSpinner = new PropertySpinnerWidget(
            this.Panel.find(".propertyYSpinner"),
            (value) => { // change
                this.onYChange(this.SelectedView, value);
            },
            (value) => { // stop
                if (this.updatingValues)
                    return;
                this.onYChange(this.SelectedView, value);
                this.SelectedView.hasChanged();
            }
        );

        $(".ui-number-spinner").unbind("keydown");

    }

    set MinWidth(value:number) {
        this.propertyWidthSpinner.Min = value;
    }

    set MinHeight(value:number) {
        this.propertyHeightSpinner.Min = value;
    }

    set HeightSpinnerEnabled(enabled:boolean) {
        this.heightEnabled = enabled;
        if (enabled)
            this.propertyHeightSpinner.enable();
        else
            this.propertyHeightSpinner.disable();
    }

    private onWidthChange(view:view.UISlideElementView, value):void {
        if (!this.widthEnabled)
            return;
        view.Width = value;
    }

    private onHeightChange(view:view.UISlideElementView, value):void {
        if (!this.heightEnabled)
            return;
        view.Height = value;
    }

    private onXChange(view:view.UISlideElementView, value):void {
        if (!this.xEnabled)
            return;
        view.X = value;
    }

    private onYChange(view:view.UISlideElementView, value):void {
        if (!this.yEnabled)
            return;
        view.Y = value;
    }

    public updateFromConnection(message:ws.OnSlideElementChangedMessage):void {
        this.updateWidget(message.view.bounds.x, message.view.bounds.y, message.view.bounds.width, message.view.bounds.height);
    }

    public setValues():void {
        this.updateWidget(this.SelectedView.X, this.SelectedView.Y, this.SelectedView.Width, this.SelectedView.Height);
    }

    private updateWidget(x, y, width, height) {
        this.propertyWidthSpinner.Value = width;
        this.propertyHeightSpinner.Value = height;
        this.propertyXSpinner.Value = x;
        this.propertyYSpinner.Value = y;
    }

}

/**
 * Display the original size of the UIImageView.
 */
class ImageViewWidget extends ElementPropertyWidget {

    private originalWidthLabel:JQuery;
    private originalHeightLabel:JQuery;

    constructor() {
        super($(".imageView.panel"));
        this.originalWidthLabel = this.Panel.find("span.originalWidth");
        this.originalHeightLabel = this.Panel.find("span.originalHeight");
    }

    public setValues():void {
        this.originalHeightLabel.html(<any>(<view.UIImageView>this.SelectedView).OriginalHeight);
        this.originalWidthLabel.html(<any>(<view.UIImageView>this.SelectedView).OriginalWidth);
    }

    public updateFromConnection(message:ws.OnSlideElementChangedMessage):void {
    }

    public onSelectionMessage(message:String, object:any):void {
        if (object instanceof view.UIImageView)
            super.onSelectionMessage(message, object);
        else
            this.hide();
    }

}

/**
 * Control the script language of the UIScriptView
 */
class ScriptViewWidget extends ElementPropertyWidget {

    private langSelector:JQuery;

    constructor() {
        super($(".scriptView.panel"));

        this.langSelector = this.Panel.find("select.language");

        this.langSelector.change(() => {
            if (this.updatingValues)
                return;
            var selectedElement = this.langSelector.find("option:selected");
            var langValue = selectedElement.attr("value");
            var langName = selectedElement.html();
            var langCss = selectedElement.data("css");
            (<view.UIScriptView>this.SelectedView).setLang(langName, langValue, langCss);
            this.SelectedView.hasChanged();
        });
    }

    public updateFromConnection(message:ws.OnSlideElementChangedMessage):void {
        this.updateWidget((<ScriptViewObject>message.view).langValue);
    }

    public setValues():void {
        this.updateWidget((<view.UIScriptView>this.SelectedView).LangValue);
    }

    private updateWidget(lang:string) {
        this.langSelector.find("option[value=" + lang + "]").attr("selected", "selected");
    }

    public onSelectionMessage(message:String, object:any):void {
        if (object instanceof view.UIScriptView)
            super.onSelectionMessage(message, object);
        else
            this.hide();
    }

    public update(message:string, object:any):void {
    }

}

class ColorField implements watcher.ClickObserver {

    private element:JQuery;
    private sliderPlaceholder:JQuery;

    private onchange:(c:ui.UIColor)=>void;
    private onstop:(c:ui.UIColor)=>void;

    private colorSlider:RGBSliderWidget;

    constructor(element:JQuery, rgba:boolean) {
        this.element = element;
        this.sliderPlaceholder = this.element.parent().find(".placeholder");

        if (rgba)
            this.colorSlider = new RGBASliderWidget();
        else
            this.colorSlider = new RGBSliderWidget($(".ui-color-slider.rgb"));

        this.element.click((event:JQueryEventObject)=> {
            event.stopPropagation();

            watcher.ClickWatcher.notifyAll(watcher.ClickWatcher.ColorFieldClicked, this);

            if (this.colorSlider.Open)
                this.colorSlider.close();
            else {
                this.colorSlider.open();
                this.colorSlider.Element.detach();
                this.sliderPlaceholder.append(this.colorSlider.Element);
            }

        });

        $(document).click(() => {
            if (this.colorSlider.Open)
                this.colorSlider.close();
        });

        this.colorSlider.OnChange = (c:ui.UIColor) => {
            this.element.css("background-color", c.toRGBAString());
            this.onchange(c);
        };

        this.colorSlider.OnStop = (c:ui.UIColor)=> {
            this.element.css("background-color", c.toRGBAString());
            this.onstop(c);
        };

        watcher.ClickWatcher.register(this);
    }

    public onClick(message:string, o:Object) {
        if (message == watcher.ClickWatcher.ColorFieldClicked)
            if (o != this)
                this.colorSlider.close();
    }

    public updateColor(c:ui.UIColor):void {
        this.element.css("background-color", c.toRGBAString());
        this.colorSlider.updateColor(c);
    }

    public onChange(callback:(c:ui.UIColor)=>void) {
        this.onchange = callback;
    }

    public onStop(callback:(c:ui.UIColor)=>void) {
        this.onstop = callback;
    }

}


/**
 * Widget that displays 3 slider to create a RGB color.
 */
class RGBSliderWidget implements watcher.ClickObserver {

    /**
     * Root element.
     */
    private element:JQuery;

    /**
     * Slider for the R component.
     */
    private rSlider:GradientSliderWidget;

    /**
     * Slider for the G component.
     */
    private gSlider:GradientSliderWidget;

    /**
     * Slider for the B component.
     */
    private bSlider:GradientSliderWidget;

    /**
     * The current color.
     */
    private color:ui.UIColor;

    private isOpen:boolean = false;

    private onChange:(color:ui.UIColor) => void;
    private onStop:(color:ui.UIColor) => void;

    constructor(element:JQuery) {
        this.element = element;
        this.color = new ui.UIColor(0, 0, 0, 0);

        this.element.click((e:JQueryEventObject) => {
            e.stopPropagation();
        });

        this.rSlider = new GradientSliderWidget(this.element.find(".red"), 0, 255, 1);
        this.rSlider.OnChange = (value:number) => {
            this.color.r = value / 255;
            this.drawGradient();
            this.onChange(this.color);
        };
        this.rSlider.OnStop = (value:number) => {
            this.color.r = value / 255;
            this.drawGradient();
            this.onStop(this.color);
        };

        this.gSlider = new GradientSliderWidget(this.element.find(".green"), 0, 255, 1);
        this.gSlider.OnChange = (value:number) => {
            this.color.g = value / 255;
            this.drawGradient();
            this.onChange(this.color);
        };
        this.gSlider.OnStop = (value:number) => {
            this.color.g = value / 255;
            this.drawGradient();
            this.onStop(this.color);
        };

        this.bSlider = new GradientSliderWidget(this.element.find(".blue"), 0, 255, 1);
        this.bSlider.OnChange = (value:number) => {
            this.color.b = value / 255;
            this.drawGradient();
            this.onChange(this.color);
        };
        this.bSlider.OnStop = (value:number) => {
            this.color.b = value / 255;
            this.drawGradient();
            this.onStop(this.color);
        };

        watcher.ClickWatcher.register(this);

    }

    public onClick(message:string, o:Object) {
        if (message == watcher.ClickWatcher.SlideContainerClicked)
            this.close();
    }

    set Open(isOpen:boolean) {
        this.isOpen = isOpen;
    }

    get Open() {
        return this.isOpen;
    }

    get Element():JQuery {
        return this.element;
    }

    /**
     * Returns the current color.
     */
    get Color():ui.UIColor {
        return this.color;
    }

    /**
     * Set the current color.
     */
    set Color(color:ui.UIColor) {
        this.color = color;
    }

    /**
     * Update sliders for the new color.
     */
    public updateColor(color:ui.UIColor):void {
        this.color = color;
        this.rSlider.Value = this.color.r * 255;
        this.gSlider.Value = this.color.g * 255;
        this.bSlider.Value = this.color.b * 255;
        this.drawGradient();
    }

    /**
     * Draw the gradient background on the sliders, indicating how the color will change,
     * if the slider is dragged into this position.
     */
    public drawGradient():void {
        var redLeftColor = new ui.UIColor(0, this.color.g, this.color.b, this.color.a);
        var redRightColor = new ui.UIColor(1, this.color.g, this.color.b, this.color.a);
        this.rSlider.setGradientBackground(redLeftColor, redRightColor);

        var greenLeftColor = new ui.UIColor(this.color.r, 0, this.color.b, this.color.a);
        var greenRightColor = new ui.UIColor(this.color.r, 1, this.color.b, this.color.a);
        this.gSlider.setGradientBackground(greenLeftColor, greenRightColor);

        var blueLeftColor = new ui.UIColor(this.color.r, this.color.g, 0, this.color.a);
        var blueRightColor = new ui.UIColor(this.color.r, this.color.g, 1, this.color.a);
        this.bSlider.setGradientBackground(blueLeftColor, blueRightColor);
    }

    public open():void {
        this.Element.show();
        this.Open = true;
    }

    public close():void {
        this.Element.hide();
        this.Element.detach();
        $("body").append(this.Element);
        this.Open = false;
    }

    /**
     * Set the on change callback.
     */
    set OnChange(change:any) {
        this.onChange = change;
    }

    /**
     * Get the callback function for on change.
     */
    get OnChange() {
        return this.onChange;
    }

    /**
     * Set the on stop callback.
     */
    set OnStop(stop:any) {
        this.onStop = stop;
    }

    /**
     * Get the on stop callback function.
     */
    get OnStop() {
        return this.onStop;
    }

}

/**
 * Extends the RGBSliderWidget to create a alpha slider
 */
class RGBASliderWidget extends RGBSliderWidget {

    private aSlider:GradientSliderWidget;

    constructor() {
        super($(".ui-color-slider.rgba"));

        this.aSlider = new GradientSliderWidget(this.Element.find(".alpha"), 0, 100, 1);

        this.aSlider.OnChange = (value:number) => {
            this.Color.a = value / 100;
            this.drawGradient();
            this.OnChange(this.Color);
        };

        this.aSlider.OnStop = (value:number) => {
            this.Color.a = value / 100;
            this.drawGradient();
            this.OnStop(this.Color);
        };
    }

    public drawGradient():void {
        super.drawGradient();
        var alphaLeftColor = new ui.UIColor(this.Color.r, this.Color.g, this.Color.b, 0);
        var alphaRightColor = new ui.UIColor(this.Color.r, this.Color.g, this.Color.b, 1);
        this.aSlider.setGradientBackground(alphaLeftColor, alphaRightColor);
    }

    public updateColor(color:ui.UIColor):void {
        super.updateColor(color);
        this.aSlider.Value = Math.round(color.a * 100);
    }

}

/**
 * Wrapper class for JQueryUI slider for the ColorSlider Widget
 */
class GradientSliderWidget {

    /**
     * The canvas element to draw the gradient background on.
     */
    private canvas:JQuery;

    /**
     * JQuery slider element.
     */
    private slider:JQuery;

    /**
     * Input field of the slider.
     * !!! currently only read only but no effect on write !!!
     */
    private input:JQuery;

    private onChange:(value:number)=>void;
    private onStop:(value:number)=>void;

    constructor(element:JQuery, minValue:number, maxValue:number, step:number) {
        this.slider = element.find(".slider").slider({
            min: minValue,
            max: maxValue,
            step: step,
            slide: (even, ui) => {
                this.input.val(ui.value);
                this.onChange(ui.value);
            },
            stop: (event, ui)=> {
                this.input.val(ui.value);
                this.onStop(ui.value);
            }
        });
        this.input = element.find("input");
        this.canvas = element.find("canvas");

        this.input.val(0);
    }

    /**
     * Set the value of the slider, adjusting the slider position and input field.
     */
    set Value(value:number) {
        value = Math.abs(value);
        this.Input.val(value);
        this.Slider.slider("option", "value", value);
    }

    /**
     * Draw the gradient background.
     */
    public setGradientBackground(leftColor:ui.UIColor, rightColor:ui.UIColor):void {
        var context = this.Context;
        context.clearRect(0, 0, 160, 10);
        context.rect(0, 0, 160, 10);
        var grd = context.createLinearGradient(0, 0, 160, 10);
        grd.addColorStop(0, leftColor.toRGBAString());
        grd.addColorStop(1, rightColor.toRGBAString());
        context.fillStyle = grd;
        context.fill();
    }

    /**
     * Set the on change callback.
     */
    set OnChange(onChange:(value:number)=>void) {
        this.onChange = onChange;
    }

    /**
     * Set the on stop callback.
     */
    set OnStop(onStop:(value:number)=>void) {
        this.onStop = onStop;
    }

    /**
     * Retrieve the CanvasRenderingContext2D from a canvas element.
     */
    get Context():CanvasRenderingContext2D {
        return (<any>this.canvas[0]).getContext("2d");
    }

    get Slider():JQuery {
        return this.slider;
    }

    get Input():JQuery {
        return this.input;
    }
}