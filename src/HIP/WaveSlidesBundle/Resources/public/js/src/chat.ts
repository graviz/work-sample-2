///<reference path="../definitions/references.d.ts" />

import watcher = require("src/watcher")
import ui = require("src/ui")
import audio = require("src/audio");
import ws = require("src/ws");

/**
 * Chat module represents a chat widget.
 */
export class ChatWidget implements watcher.KeyboardObserver {

    private static MINIMIZED:string = "minimized";
    private static MUTED:string = "muted";
    private static MESSAGE_CONTAINER:string = ".message-container";
    private static CHAT_MESSAGE_SELECTOR:string = ".chatMessage";
    private static NOTIFICATION_SOUND:string = "chatNotification.mp3";

    /** Top level container of the chat window. */
    private chatWindow = $(".chatContainer");

    /** Element which holds the chat message elements. */
    private conversations:JQuery;

    /** Clickable top bar element of the chat window. */
    private topBar:JQuery;

    /** Element which hold the unread message count. */
    private messageCountField:JQuery;

    /** Hideable element containing chat conversations and input bar. */
    private chatBox:JQuery;

    /** <input> element */
    private input:JQuery;

    /** Element to control the mute state */
    private muteButton:JQuery;

    /** Number of unread messages while the chat is minimized. */
    private unreadMessageCount = 0;

    /** Set the chat to mute. */
    private muted = false;

    constructor() {

        this.conversations = this.chatWindow.find(".conversations");
        this.topBar = this.chatWindow.find(".chatTitleBar");
        this.messageCountField = this.chatWindow.find("#unreadMessageCount");
        this.chatBox = this.chatWindow.find(".chatBox");
        this.input = this.chatWindow.find(".chatInput input");
        this.muteButton = this.chatWindow.find(".muteButton");

        this.topBar.click(() => {
            this.toggleChat();
        });

        this.muteButton.click((e) => {
            e.stopPropagation();
            this.muted = !this.muted;
            this.muteButton.toggleClass(ChatWidget.MUTED);
        });

        watcher.KeyboardWatcher.register(this);

        ws.Socket.on(ws.bi.Chat, (message:ws.IncomingMessage)=> {
            this.onChatMessage(<ws.ChatMessage>message);
        });

        ui.Workbench.onWorkbenchLoaded(()=> {
            this.chatWindow.show();
        })
    }

    public init(messages:string[]):void {
        if (messages == null)
            return;

        messages.forEach((message:string)=> {
            this.conversations.append(message);
        });
    }

    public onKeyDown(keycode:Number):void {
        if (keycode == watcher.KeyboardWatcher.ENTER && this.sendRequirements()) {
            ui.Workbench.get().Socket.sendChatMessage(this.input.val());
            this.input.val("");
        }
    }

    public onKeyUp(keycode:Number):void {
    }

    /**
     * On ChatMessage: Append chat message to the conversations element.
     * Play notification sound and show notification blink if minimized and sound enabled.
     */
    private onChatMessage(message:ws.ChatMessage):void {
        var newMessage = $(message.view);

        var lastMessage = this.conversations.find(ChatWidget.CHAT_MESSAGE_SELECTOR).last();

        if (lastMessage.data("sender") == message.userId) {
            console.log(lastMessage);
            var textElement = newMessage.find(ChatWidget.MESSAGE_CONTAINER).children();
            lastMessage.find(ChatWidget.MESSAGE_CONTAINER).append(textElement);
        } else {
            newMessage.appendTo(this.conversations);
        }

        this.conversations.scrollTop(this.conversations.prop("scrollHeight"));

        if (this.isMinimized()) {
            if (!this.muted)
                audio.AudioManager.playSound(ChatWidget.NOTIFICATION_SOUND);
            this.unreadMessageCount++;
            this.messageCountField.html("(" + this.unreadMessageCount + ")");
            this.animateColorToGrey();
        }
    }

    /**
     * Toggles the chat box.
     * If went visible it resets the unread message count and focus the input element.
     */
    private toggleChat():void {
        this.stopBlinkAnimation();
        this.chatWindow.toggleClass(ChatWidget.MINIMIZED);

        if (this.chatBox.is(":visible")) {
            this.unreadMessageCount = 0;
            this.messageCountField.html("");
            this.input.focus(0);
        }
    }

    /**
     * Stop the animation of the top bar.
     */
    private stopBlinkAnimation():void {
        this.topBar.stop(true, false);
        this.topBar.css({backgroundColor: ui.UIKit.uiRedColor});
    }

    /**
     * Returns true, if the chat window has the class .minimized
     * @returns boolean
     */
    private isMinimized():boolean {
        return this.chatWindow.hasClass(ChatWidget.MINIMIZED);
    }

    /**
     * Change color to grey blink part.
     */
    private animateColorToGrey():void {
        this.topBar.animate({
            backgroundColor: ui.UIKit.uiStateSelectedColor
        }, 900, () => {
            this.animateColorToRed();
        });
    }

    /**
     * Change color to red blink part.
     */
    private animateColorToRed():void {
        this.topBar.animate({
            backgroundColor: ui.UIKit.uiRedColor
        }, 900, () => {
            this.animateColorToGrey();
        });
    }

    /**
     * Returns true, if the input value is not empty and the input element is focused.
     * @returns boolean
     */
    private sendRequirements():boolean {
        return this.input.val() != "" && this.input.is(":focus");
    }

}