///<reference path="../definitions/references.d.ts" />

import view = require("src/view")

/**
 * Helper class for DOM operations for Slides and Slide Thumbnails.
 */
export class SlideHelper {

    /**
     * Element holding all UISlideView elements.
     */
    public static slideContainer:JQuery;

    /**
     * Element holding all UISlideThumbnailView elements.
     */
    public static thumbnailContainer:JQuery;

    /**
     * Returns the UISlideView JQuery element at the index.
     * Index starting at 0
     */
    public static getSlideElement(index:number):JQuery {
        return $(SlideHelper.slideContainer.children(".uiSlide")[index]);
    }

    /**
     * Returns the UISlideThumbnailView JQuery element.
     * Index starting at 0
     */
    public static getThumbnailElement(index:number):JQuery {
        return $(SlideHelper.thumbnailContainer.children(".uiSlideThumbnail")[index]);
    }

    /**
     * Inserts a html string as a UISlideView a the index.
     */
    public static insertSlide(html:string, index:number):JQuery {
        var previousSlide = SlideHelper.getSlideElement(index - 1);
        $(html).insertAfter(previousSlide);
        return SlideHelper.getSlideElement(index);
    }

    /**
     * Inserts a html string as a UISlideThumbnail a the index.
     */
    public static insertThumbnail(html:string, index:number):void {
        var previousThumbnail = SlideHelper.getThumbnailElement(index - 1);
        $(html).insertAfter(previousThumbnail);
    }

    /**
     * Inserts a JQuery element as a UISlideThumbnail a the index.
     */
    public static insertThumbnailElement(element:JQuery, index):void {
        if (index >= 1) {
            var previousThumbnail = SlideHelper.getThumbnailElement(index - 1);
            element.insertAfter(previousThumbnail);
        }
        if (index == 0) {
            var previousThumbnail = SlideHelper.getThumbnailElement(index);
            element.insertBefore(previousThumbnail);
        }
    }

}

/**
 * Helper class to align and distribute selected slide elements in different kinds
 */
export class UIAlignHelper {

    private views:view.UISlideElementView[];

    static alignLeft:string = "alignLeft";
    static alignHorizontalCenter:string = "alignHorizontalCenter";
    static alignRight:string = "alignRight";
    static alignTop:string = "alignTop";
    static alignVerticalCenter:string = "alignVerticalCenter";
    static alignBottom:string = "alignBottom";

    static distributeHorizontalLeft:string = "distributeLeft";
    static distributeHorizontalCenter:string = "distributeHorizontalCenter";
    static distributeHorizontalRight:string = "distributeRight";
    static distributeVerticalTop:string = "distributeTop";
    static distributeVerticalCenter:string = "distributeVerticalCenter";
    static distributeVerticalBottom:string = "distributeBottom";

    static distributeHorizontalEqual:string = "distributeHorizontalEqual";
    static distributeVerticalEqual:string = "distributeVerticalEqual";

    /**
     * Align slide elements by a given option.
     */
    public align(option:string, views:view.UISlideElementView[]) {
        this.views = views;

        if (this.views.length <= 1)
            return;

        switch (option) {
            case UIAlignHelper.alignLeft:
                this.alignLeft();
                break;
            case UIAlignHelper.alignHorizontalCenter:
                this.alignHorizontalCenter();
                break;
            case UIAlignHelper.alignRight:
                this.alignRight();
                break;
            case UIAlignHelper.alignTop:
                this.alignTop();
                break;
            case UIAlignHelper.alignVerticalCenter:
                this.alignVerticalCenter();
                break;
            case UIAlignHelper.alignBottom:
                this.alignBottom();
                break;
        }

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].hasChanged();

    }

    /**
     * Distribute the space between slide elements by a given option.
     */
    public distribute(option:string, views:view.UISlideElementView[]) {
        this.views = views;

        if (this.views.length <= 2)
            return;

        switch (option) {
            case UIAlignHelper.distributeHorizontalLeft:
                this.distributeHorizontalLeft();
                break;
            case UIAlignHelper.distributeHorizontalCenter:
                this.distributeHorizontalCenter();
                break;
            case UIAlignHelper.distributeHorizontalRight:
                this.distributeHorizontalRight();
                break;
            case UIAlignHelper.distributeVerticalTop:
                this.distributeVerticalTop();
                break;
            case UIAlignHelper.distributeVerticalCenter:
                this.distributeVerticalCenter();
                break;
            case UIAlignHelper.distributeVerticalBottom:
                this.distributeVerticalBottom();
                break;
            case UIAlignHelper.distributeHorizontalEqual:
                this.distributeHorizontalEqual();
                break;
            case UIAlignHelper.distributeVerticalEqual:
                this.distributeVerticalEqual();
                break;
        }

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].hasChanged();
    }

    /**
     * Distribute the space between the elements horizontal relative to their left anchor point.
     */
    private distributeHorizontalLeft():void {
        this.sortXAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].X - mostLeftElement.X;
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].X = mostLeftElement.X + (i * spaceBetweenElements);
    }

    /**
     * Distribute the space between the elements horizontal relative to their middle anchor point.
     */
    private distributeHorizontalCenter():void {
        this.sortXCenterAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].getRelativeX(view.UISlideElementView.MIDDLE) - mostLeftElement.getRelativeX(view.UISlideElementView.MIDDLE);
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].X = mostLeftElement.getRelativeX(view.UISlideElementView.MIDDLE) + (i * spaceBetweenElements) - (this.views[i].Width / 2);
    }

    /**
     * Distribute the space between the elements horizontal relative to their right anchor point.
     */
    private distributeHorizontalRight():void {
        this.sortXRightAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].getRelativeX(view.UISlideElementView.RIGHT) - mostLeftElement.getRelativeX(view.UISlideElementView.RIGHT);
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].X = mostLeftElement.getRelativeX(view.UISlideElementView.RIGHT) + (i * spaceBetweenElements) - this.views[i].Width;
    }

    /**
     * Distribute the space between the elements vertical relative to their top anchor point.
     */
    private distributeVerticalTop():void {
        this.sortYAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].Y - mostLeftElement.Y;
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].Y = mostLeftElement.Y + (i * spaceBetweenElements);
    }

    /**
     * Distribute the space between the elements vertical relative to their middle anchor point.
     */
    private distributeVerticalCenter():void {
        this.sortYCenterAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].getRelativeY(view.UISlideElementView.MIDDLE) - mostLeftElement.getRelativeY(view.UISlideElementView.MIDDLE);
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].Y = mostLeftElement.getRelativeY(view.UISlideElementView.MIDDLE) + (i * spaceBetweenElements) - (this.views[i].Height / 2);
    }

    /**
     * Distribute the space between the elements vertical relative to their bottom anchor point.
     */
    private distributeVerticalBottom():void {
        this.sortYBottomAscending();

        var mostLeftElement = this.views[0];
        var usableSpace = this.views[this.views.length - 1].getRelativeY(view.UISlideElementView.BOTTOM) - mostLeftElement.getRelativeY(view.UISlideElementView.BOTTOM);
        var spaceBetweenElements = Math.round(usableSpace / (this.views.length - 1));

        for (var i:number = 1; i < this.views.length - 1; i++)
            this.views[i].Y = mostLeftElement.getRelativeY(view.UISlideElementView.BOTTOM) + (i * spaceBetweenElements) - this.views[i].Height;
    }

    /**
     * Distribute the space between the elements horizontal equally.
     */
    private distributeHorizontalEqual():void {
        this.sortXAscending();
        var usableSpace = this.views[this.views.length - 1].X - this.views[0].getRelativeX(view.UISlideElementView.RIGHT);

        for (var i = 1; i < this.views.length - 1; i++)
            usableSpace -= this.views[i].Width;

        if (usableSpace < 0)
            return;

        usableSpace = Math.round(usableSpace / (this.views.length - 1));

        for (var i = 1; i < this.views.length - 1; i++)
            this.views[i].X = this.views[i - 1].getRelativeX(view.UISlideElementView.RIGHT) + usableSpace;
    }

    /**
     * Distribute the space between the elements vertical equally.
     */
    private distributeVerticalEqual():void {
        this.sortYAscending();
        var usableSpace = this.views[this.views.length - 1].Y - this.views[0].getRelativeY(view.UISlideElementView.BOTTOM);

        for (var i = 1; i < this.views.length - 1; i++)
            usableSpace -= this.views[i].Height;

        if (usableSpace < 0)
            return;

        usableSpace = Math.round(usableSpace / (this.views.length - 1));

        for (var i = 1; i < this.views.length - 1; i++)
            this.views[i].Y = this.views[i - 1].getRelativeY(view.UISlideElementView.BOTTOM) + usableSpace;
    }

    /**
     * Sorts the elements by their x value relative to their left anchor point.
     */
    private sortXAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.X < b.X)
                return -1;
            if (a.X > b.X)
                return 1;
            return 0;
        });
    }

    /**
     * Sorts the elements by their x value relative their the middle anchor point.
     */
    private sortXCenterAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.getRelativeX(view.UISlideElementView.MIDDLE) < b.getRelativeX(view.UISlideElementView.MIDDLE))
                return -1;
            if (a.getRelativeX(view.UISlideElementView.MIDDLE) > b.getRelativeX(view.UISlideElementView.MIDDLE))
                return 1;
            return 0;
        });
    }

    /**
     * Sorts the elements by their x value relative their the right anchor point.
     */
    private sortXRightAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.getRelativeX(view.UISlideElementView.RIGHT) < b.getRelativeX(view.UISlideElementView.RIGHT))
                return -1;
            if (a.getRelativeX(view.UISlideElementView.RIGHT) > b.getRelativeX(view.UISlideElementView.RIGHT))
                return 1;
            return 0;
        });
    }

    /**
     * Sorts the elements by their y value relative their the top anchor point.
     */
    private sortYAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.Y < b.Y)
                return -1;
            if (a.Y > b.Y)
                return 1;
            return 0;
        });
    }

    /**
     * Sorts the elements by their y value relative their the middle anchor point.
     */
    private sortYCenterAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.getRelativeY(view.UISlideElementView.MIDDLE) < b.getRelativeY(view.UISlideElementView.MIDDLE))
                return -1;
            if (a.getRelativeY(view.UISlideElementView.MIDDLE) > b.getRelativeY(view.UISlideElementView.MIDDLE))
                return 1;
            return 0;
        });
    }

    /**
     * Sorts the elements by their y value relative their the bottom anchor point.
     */
    private sortYBottomAscending():void {
        this.views.sort((a:view.UISlideElementView, b:view.UISlideElementView) => {
            if (a.getRelativeY(view.UISlideElementView.BOTTOM) < b.getRelativeY(view.UISlideElementView.BOTTOM))
                return -1;
            if (a.getRelativeY(view.UISlideElementView.BOTTOM) > b.getRelativeY(view.UISlideElementView.BOTTOM))
                return 1;
            return 0;
        });
    }

    /**
     * Set the x value of all slide elements to the most left element indicated by its left anchor point.
     */
    private alignLeft():void {
        var lowestXValue:number = 999999;

        for (var i:number = 0; i < this.views.length; i++)
            if (this.views[i].X < lowestXValue)
                lowestXValue = this.views[i].X;

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].X = lowestXValue;
    }

    /**
     * Set the x value of all slide elements, so that they are all positioned in the center of the most left
     * and most right placed element indicated by their middle anchor point.
     */
    private alignHorizontalCenter():void {
        var lowestX:number = 9999;
        var highestX:number = -1;

        for (var i:number = 0; i < this.views.length; i++) {

            if (this.views[i].X + this.views[i].Width > highestX)
                highestX = this.views[i].getRelativeX(view.UISlideElementView.RIGHT);

            if (this.views[i].X < lowestX)
                lowestX = this.views[i].X;
        }

        var middle:number = (lowestX + highestX) / 2;

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].X = middle - this.views[i].Width / 2;
    }

    /**
     * Set the x value of all slide elements to the most right element indicated by its right anchor point.
     */
    private alignRight():void {
        var highestXValue:number = -1;

        for (var i:number = 0; i < this.views.length; i++)
            if (this.views[i].X + this.views[i].Width > highestXValue)
                highestXValue = this.views[i].getRelativeX(view.UISlideElementView.RIGHT);

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].X = highestXValue - this.views[i].Width;
    }

    /**
     * Set the y value of all slide elements to the most top element indicated by its top anchor point.
     */
    private alignTop():void {
        var lowestYValue:number = 999999;

        for (var i:number = 0; i < this.views.length; i++)
            if (this.views[i].Y < lowestYValue)
                lowestYValue = this.views[i].Y;

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].Y = lowestYValue;
    }

    /**
     * Set the y value of all slide elements, so that they are all positioned in the center of the most top
     * and most bottom placed element indicated by their middle anchor point.
     */
    private alignVerticalCenter():void {
        var lowestY:number = 9999;
        var highestY:number = -1;

        for (var i:number = 0; i < this.views.length; i++) {
            if (this.views[i].Y + this.views[i].Height > highestY)
                highestY = this.views[i].getRelativeY(view.UISlideElementView.BOTTOM);

            if (this.views[i].Y < lowestY)
                lowestY = this.views[i].Y;
        }

        var middle:number = (lowestY + highestY) / 2;

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].Y = middle - this.views[i].Height / 2;
    }

    /**
     * Set the y value of all slide elements to the most bottom element indicated by its bottom anchor point.
     */
    private alignBottom():void {
        var highestYValue:number = -1;

        for (var i:number = 0; i < this.views.length; i++)
            if (this.views[i].Y + this.views[i].Height > highestYValue)
                highestYValue = this.views[i].getRelativeY(view.UISlideElementView.BOTTOM);

        for (var i:number = 0; i < this.views.length; i++)
            this.views[i].Y = highestYValue - this.views[i].Height;
    }

}