///<reference path="../definitions/references.d.ts" />

export class ClickWatcher {

    public static SlideContainerClicked:string = "SlideContainerClicked";
    public static ColorFieldClicked:string = "ColorFieldClicked";

    private static instance:ClickWatcher = new ClickWatcher();

    private observers:ClickObserver[] = [];

    public static notifyAll(message:string, object:any):void {
        console.log(message, object);
        ClickWatcher.instance.observers.forEach((observer:ClickObserver)=> {
            observer.onClick(message, object);
        });
    }

    public static register(o:ClickObserver):void {
        ClickWatcher.instance.observers.push(o);
    }
}

/**
 * Notifies registered SelectionObserver for selection messages.
 */
export class SelectionWatcher {

    public static SelectedEventMessage:string = "SelectedEventMessage";
    public static AddToSelectionMessage:string = "AddToSelectionMessage";
    public static UnselectedEventMessage:string = "UnelectedEventMessage";
    public static NoElementSelectedMessage:string = "NoElementSelectedMessage";
    public static ImageSelectedMessage:string = "ImageSelectedMessage";

    private static instance:SelectionWatcher = new SelectionWatcher();

    private observers:SelectionObserver[] = [];

    public static notifyAll(message:string, object:any):void {
        console.log(message, object);
        SelectionWatcher.instance.observers.forEach((observer:SelectionObserver)=> {
            observer.onSelectionMessage(message, object);
        });
    }

    public static register(o:SelectionObserver):void {
        SelectionWatcher.instance.observers.push(o);
    }

}

/**
 * Notifies registered KeyboardObserver for keyup and keydown events.
 * Must be initialised once.
 */
export class KeyboardWatcher {

    public static ARROW_DOWN:number = 40;
    public static ARROW_UP:number = 38;
    public static ARROW_LEFT:number = 37;
    public static ARROW_RIGHT:number = 39;
    public static DELETE:number = 46;
    public static SHIFT:number = 16;
    public static ENTER:number = 13;
    public static TAB:number = 9;

    private static listener:KeyboardObserver[] = [];

    public static init() {
        $(window).on("keydown", (e:JQueryKeyEventObject) => {
            for (var i = 0; i < KeyboardWatcher.listener.length; i++)
                KeyboardWatcher.listener[i].onKeyDown(e.which, e);
        });

        $(window).on("keyup", (e:JQueryKeyEventObject)=> {
            for (var i = 0; i < KeyboardWatcher.listener.length; i++)
                KeyboardWatcher.listener[i].onKeyUp(e.which, e);
        });

    }

    public static register(listener:KeyboardObserver) {
        KeyboardWatcher.listener.push(listener);
    }

}

/**
 * Notifies registered ViewObserver when views are changed or removed.
 */
export class ViewWatcher {

    public static ChangedEventMessage:string = "ChangedEventMessage";
    public static SlideRemovedMessage:string = "SlideRemovedMessage";
    public static SlideElementRemovedMessage:string = "SlideElementRemovedMessage";

    private static instance:ViewWatcher = new ViewWatcher();

    private observers:ViewObserver[] = [];

    public static notifyAll(message:string, object:any):void {
        console.log(message, object);
        ViewWatcher.instance.observers.forEach((observer:ViewObserver)=> {
            observer.update(message, object);
        });
    }

    public static register(o:ViewObserver):void {
        ViewWatcher.instance.observers.push(o);
    }

}

export interface ClickObserver {
    onClick(message:string, o:Object);
}

export interface SelectionObserver {
    /**
     * @param message Name of the SelectionMessage.
     * @param object The object which triggered the message.
     */
    onSelectionMessage(message:string, object:any):void;
}

export interface ViewObserver {
    /**
     * @param message The name of the Message.
     * @param object The object which triggered the message.
     */
    update(message:string, object:any):void;
}

export interface KeyboardObserver {
    onKeyDown(keycode:number, event:JQueryKeyEventObject):void;
    onKeyUp(keycode:number, event:JQueryKeyEventObject):void;
}