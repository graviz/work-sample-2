///<reference path="../definitions/references.d.ts" />

import net = require("src/net");
import helper = require("src/helper");
import property = require("src/property");
import watcher = require("src/watcher");
import view = require("src/view");
import chat = require("src/chat");
import common = require("src/common");
import ws = require("src/ws");

export class Workbench {

    private socket:ws.Socket;
    private chat:chat.ChatWidget;
    private slideContainer:UISlideContainer;
    private toolbar:UIToolbar;
    private alignHelper:helper.UIAlignHelper = new helper.UIAlignHelper();

    private static listener:Function[] = [];

    private static instance:Workbench;

    constructor() {
        ws.Socket.on(ws.inc.Authentication, (message:ws.AuthenticationMessage) => {
            this.setup(<ws.AuthenticationMessage>message);
        });
    }

    get AlignHelper():helper.UIAlignHelper {
        return this.alignHelper;
    }

    get SlideContainer():UISlideContainer {
        return this.slideContainer;
    }

    get Socket():ws.Socket {
        return this.socket;
    }

    set Socket(socket:ws.Socket) {
        this.socket = socket;
    }

    get Chat():chat.ChatWidget {
        return this.chat;
    }

    set Chat(chat:chat.ChatWidget) {
        this.chat = chat;
    }

    public static get():Workbench {
        if (Workbench.instance == null)
            Workbench.instance = new Workbench();

        return Workbench.instance;
    }

    public static onWorkbenchLoaded(callback:()=>void) {
        Workbench.listener.push(callback);
    }

    public setup(message:ws.AuthenticationMessage) {
        if (!message.success)
            return;

        // 0.the waveeditor must be visible to compute needed dimensions and positions.
        $(".waveloader").hide();
        $("html, body").removeAttr("style");
        $(".waveeditor").show();

        var editorSidebar = $(".uiSlideSidebar");

        // 1. insert thumbnails into dom
        message.deck.thumbnails.forEach((thumbnail:string)=> {
            editorSidebar.append(thumbnail);
        });

        // 2. init the thumbnail scrollbar. content must be set
        UIKit.initScrollbar();

        // 3. init the slide container with every slide content
        this.slideContainer = new UISlideContainer(message.deck.format);
        this.slideContainer.initSlides(message.deck.slides);

        // 4. init toolbar bindings
        this.toolbar = new UIToolbar();

        // 5. init the user pool with current users
        ws.UserPool.init(message.users);

        // 6. init the keyboard watcher to start listing for keyboard events
        watcher.KeyboardWatcher.init();

        // 7. init property views
        new property.SlideElementPropertyPanel();
        new property.UIConnectionPanel();

        this.Chat.init(message.chats);
        UIKit.resizeScrollbar();

        Workbench.listener.forEach((callback:Function)=> {
            callback();
        });
    }

}

/**
 * Access to all toolbar buttons.
 */
export class UIToolbar implements watcher.SelectionObserver {

    public static addSlideButton:JQuery = $(".ui-toolbar #newSlide");
    public static playButton:JQuery = $(".ui-toolbar #play");

    public static textButton:JQuery = $(".ui-toolbar #newTextView");
    public static scriptButton:JQuery = $(".ui-toolbar #newCode");
    public static imageButton:JQuery = $(".ui-toolbar #newImage");
    public static tableButton:JQuery = $(".ui-toolbar #newTable");

    public static toFrontButton:JQuery = $(".ui-toolbar #pushToFront");
    public static toBackButton:JQuery = $(".ui-toolbar #pushToBack");

    public static textFormatButton:JQuery = $(".textFormatButton button, .textAlignButton button");
    public static fontSizeButton:JQuery = $(".fontFormatButton #textFontSize");
    public static alignButton:JQuery = $(".objectAlignButton button");
    public static distributeButton:JQuery = $(".objectDistributeButton button");

    constructor() {
        watcher.SelectionWatcher.register(this);

        var fileChooser = $("#fileChooser");
        UIToolbar.imageButton.click((e:JQueryMouseEventObject)=> {
            fileChooser.trigger("click");
        });

        fileChooser.change(() => {
            var files:File[] = fileChooser.prop('files');
            var images:File[] = [];
            for (var i = 0; i < files.length; i++)
                if (net.FileManager.isImage(files[i]))
                    images.push(files[i]);
            watcher.SelectionWatcher.notifyAll(watcher.SelectionWatcher.ImageSelectedMessage, images);
            fileChooser.replaceWith(fileChooser = fileChooser.clone(true));
        });

        UIToolbar.addSlideButton.click(() => {
            Workbench.get().Socket.createSlide();
        });

        UIToolbar.textButton.click(() => {
            Workbench.get().Socket.createSlideElement(view.UITextView.role, Workbench.get().SlideContainer.SelectedSlide.Id);
        });

        UIToolbar.scriptButton.click(() => {
            Workbench.get().Socket.createSlideElement(view.UIScriptView.role, Workbench.get().SlideContainer.SelectedSlide.Id)
        });

        UIToolbar.tableButton.click(() => {
            Workbench.get().Socket.createSlideElement(view.UITableView.role, Workbench.get().SlideContainer.SelectedSlide.Id)
        });

        UIToolbar.textFormatButton.click(function () {
            if ($(this).hasClass(UIKit.disabled))
                return;

            var format = $(this).attr("data-format");
            var value = $(this).attr("data-value");

            Workbench.get().SlideContainer.SelectedElements.forEach((element:view.UISlideElementView)=> {
                if (element instanceof view.AbstractUITextView)
                    (<view.AbstractUITextView>element).format(format, value);
            });
        });

        UIToolbar.fontSizeButton.change(function () {
            if ($(this).hasClass(UIKit.disabled))
                return;

            var size = $(this).val();
            Workbench.get().SlideContainer.SelectedElements.forEach((element:view.UISlideElementView)=> {
                if (element instanceof view.AbstractUITextView)
                    (<view.AbstractUITextView>element).format("fontSize", size);
            });
        });

        UIToolbar.alignButton.click(function () {
            Workbench.get().AlignHelper.align($(this).attr("id"), Workbench.get().SlideContainer.SelectedElements);
        });

        UIToolbar.distributeButton.click(function () {
            Workbench.get().AlignHelper.distribute($(this).attr("id"), Workbench.get().SlideContainer.SelectedElements);
        });
    }

    /**
     * Disable buttons when selection changes.
     */
    public onSelectionMessage(message:string, object:view.UIView):void {
        switch (message) {
            case watcher.SelectionWatcher.NoElementSelectedMessage:
                this.disableObjectAlignButtons();
                this.disableObjectDistributeButtons();
                this.disableTextFormattingButtons();
                break;
            case watcher.SelectionWatcher.SelectedEventMessage:
                if (object instanceof view.UITextView)
                    this.enableTextFormattingButtons();
                else
                    this.disableTextFormattingButtons();
                break;
            case watcher.SelectionWatcher.AddToSelectionMessage:
                this.enableObjectAlignButtons();
                this.enableObjectDistributeButtons();
                break;
        }
    }

    public static enableToFrontButton():void {
        UIToolbar.toFrontButton.removeClass(UIKit.disabled);
    }

    public static disableToFrontButton():void {
        UIToolbar.toFrontButton.addClass(UIKit.disabled);
    }

    public static enableToBackButton():void {
        UIToolbar.toBackButton.removeClass(UIKit.disabled);
    }

    public static disableToBackButton():void {
        UIToolbar.toBackButton.addClass(UIKit.disabled);
    }

    private enableTextFormattingButtons():void {
        UIToolbar.textFormatButton.removeClass(UIKit.disabled);
    }

    private  disableTextFormattingButtons():void {
        UIToolbar.textFormatButton.addClass(UIKit.disabled);
    }

    private enableObjectAlignButtons():void {
        UIToolbar.alignButton.removeClass(UIKit.disabled);
    }

    private disableObjectAlignButtons():void {
        UIToolbar.alignButton.addClass(UIKit.disabled);
    }

    private enableObjectDistributeButtons():void {
        UIToolbar.distributeButton.removeClass(UIKit.disabled);
    }

    private disableObjectDistributeButtons():void {
        UIToolbar.distributeButton.addClass(UIKit.disabled);
    }

}

/**
 * Container for slides.
 */
export class UISlideContainer implements watcher.SelectionObserver, watcher.ViewObserver, watcher.KeyboardObserver {

    public static FORMAT_4_3 = "_4_3";
    public static FORMAT_16_10 = "_16_10";
    public static FORMAT_16_9 = "_16_9";

    /**
     * All slides for this deck. The array is sorted ascending by it's index.
     */
    private slides:view.UISlideView[] = [];

    /**
     * The visible slide.
     */
    private selectedSlide:view.UISlideView;

    /**
     * Panel to control the display the slide properties.
     */
    private slidePropertyPanel:property.SlidePropertyPanel;

    /**
     * Upload panel to handle file upload from the <input> of type file.
     */
    private uploadPanel:property.UploadPanel;

    private format:string;

    private static dim:Dimension;

    public static dimension(format?:string):Dimension {
        if (format != null) {
            switch (format) {
                case UISlideContainer.FORMAT_4_3:
                    UISlideContainer.dim = {width: 800, height: 600};
                    break;
                case UISlideContainer.FORMAT_16_10:
                    UISlideContainer.dim = {width: 800, height: 500};
                    break;
                case UISlideContainer.FORMAT_16_9:
                    UISlideContainer.dim = {width: 800, height: 450};
                    break;
            }
        }
        return UISlideContainer.dim;
    }

    constructor(format:string) {
        this.format = format;

        UISlideContainer.dimension(this.format);
        helper.SlideHelper.slideContainer = $(".uiSlideContainer");
        helper.SlideHelper.slideContainer.css("width", UISlideContainer.dimension(this.format).width);
        helper.SlideHelper.slideContainer.css("height", UISlideContainer.dimension(this.format).height);

        watcher.SelectionWatcher.register(this);
        watcher.ViewWatcher.register(this);
        watcher.KeyboardWatcher.register(this);

        this.slidePropertyPanel = new property.SlidePropertyPanel(this);
        this.uploadPanel = new property.UploadPanel(this);

        UIToolbar.toBackButton.click(()=> {
            this.bringElementToBack();
        });

        UIToolbar.toFrontButton.click(()=> {
            this.bringElementToFront();
        });

        ws.Socket.on(ws.inc.SlideElementCreated, (message:ws.IncomingMessage)=> {
            this.onElementCreated(<ws.ViewCreatedMessage>message);
        });

        ws.Socket.on(ws.inc.OnSlideCreated, (message:ws.IncomingMessage)=> {
            this.onSlideCreated(<ws.SlideCreatedMessage>message);
        });

        ws.Socket.on(ws.inc.OnSlideElementChanged, (message:ws.IncomingMessage)=> {
            this.onViewChanged(<ws.OnSlideElementChangedMessage>message);
        });

        ws.Socket.on(ws.inc.OnSlideRemoved, (message:ws.IncomingMessage)=> {
            this.onSlideRemoved(<ws.OnSlideRemovedMessage>message);
        });

        ws.Socket.on(ws.inc.OnSlideChanged, (message:ws.IncomingMessage)=> {
            this.onSlideChanged(<ws.OnSlideChangedMessage>message);
        });

        ws.Socket.on(ws.inc.OnSlideElementRemoved, (message:ws.IncomingMessage)=> {
            this.onSlideElementRemoved(<ws.OnSlideElementRemovedMessage>message);
        });

        ws.Socket.on(ws.bi.SlideElementSelected, (message:ws.IncomingMessage)=> {
            var slide = this.getSlideById((<ws.SlideElementSelectedMessage>message).slideId);
            var element = slide.getElementById((<ws.SlideElementSelectedMessage>message).elementId);
            element.addUserToSelectionList((<ws.SlideElementSelectedMessage>message).userId);
        });

        ws.Socket.on(ws.bi.SlideElementUnselected, (message:ws.IncomingMessage)=> {
            var slide = this.getSlideById((<ws.SlideElementSelectedMessage>message).slideId);
            var element = slide.getElementById((<ws.SlideElementSelectedMessage>message).elementId);
            element.removeUserFromSelectionList((<ws.SlideElementSelectedMessage>message).userId);
        });
    }

    /**
     * Returns the current visible slide.
     * @returns UISlideView
     * @constructor
     */
    get SelectedSlide():view.UISlideView {
        return this.selectedSlide;
    }

    /**
     * Returns an array with selected slide elements of the visible slide.
     * @returns UISlideElementView[]
     * @constructor
     */
    get SelectedElements():view.UISlideElementView[] {
        return this.selectedSlide.SelectedElements;
    }

    /**
     * Returns all slides.
     * @returns UISlideView[]
     * @constructor
     */
    get Slides():view.UISlideView[] {
        return this.slides;
    }

    /**
     * When a slide element was changed, we refresh the thumbnail.
     * When a slide was removed we have to remove it from this class.
     */
    public update(message:String, object):void {
        switch (message) {
            case watcher.ViewWatcher.ChangedEventMessage:
                if (object instanceof view.UISlideElementView)
                    object.Slide.Thumbnail.refresh();
                break;
            case watcher.ViewWatcher.SlideRemovedMessage:
                this.removeSlide(<view.UISlideView>object, true);
                break;
        }
    }

    /**
     * Listen for arrow keys and move the selected elements.
     * Listen for the esc key to delete the selected elements.
     */
    public onKeyDown(keycode:number, event:JQueryKeyEventObject):void {
        if (this.SelectedElements.length >= 1 && !this.anyElementIsEditing() && this.isArrowKey(keycode))
            event.preventDefault();

        switch (keycode) {
            case watcher.KeyboardWatcher.ARROW_DOWN:
                this.onArrowDown();
                break;
            case watcher.KeyboardWatcher.ARROW_LEFT:
                this.onArrowLeft();
                break;
            case watcher.KeyboardWatcher.ARROW_RIGHT:
                this.onArrowRight();
                break;
            case watcher.KeyboardWatcher.ARROW_UP:
                this.onArrowUp();
                break;
            case watcher.KeyboardWatcher.DELETE:
                this.onDeleteKey();
                break;
        }
    }

    /**
     * When one arrow key was released and no element is editing, we tell the selected
     * elements that they have changed.
     */
    public onKeyUp(keycode:number):void {
        if (this.SelectedElements.length == 0)
            return;

        switch (keycode) {
            case watcher.KeyboardWatcher.ARROW_DOWN:
            case watcher.KeyboardWatcher.ARROW_LEFT:
            case watcher.KeyboardWatcher.ARROW_RIGHT:
            case watcher.KeyboardWatcher.ARROW_UP:
                if (!this.anyElementIsEditing())
                    for (var i = 0; i < this.SelectedElements.length; i++)
                        this.SelectedElements[i].hasChanged();
                break;
        }
    }

    /**
     * When a slide thumbnail was selected, we change the visible slide.
     * When a slide element was selected, we update the UIToolbar buttons.
     * When a image was selected from the <input> of type file we upload the files.
     * When no more elements are selected we disable to "to front" and "to back" buttons from the toolbar.
     */
    public onSelectionMessage(message:string, object:any):void {
        switch (message) {
            case watcher.SelectionWatcher.SelectedEventMessage:
                if (object instanceof view.UISlideThumbnailView)
                    this.changeSlide((<view.UISlideThumbnailView>object).Slide);
                else if (object instanceof view.UISlideElementView)
                    this.updateToolbarButtons(<view.UISlideElementView>object);
                break;
            case watcher.SelectionWatcher.ImageSelectedMessage:
                this.uploadPanel.uploadFiles(<File[]>object);
                break;
            case watcher.SelectionWatcher.NoElementSelectedMessage:
                UIToolbar.disableToBackButton();
                UIToolbar.disableToFrontButton();
                break;
        }
    }

    /**
     * Insert the html string array into the container element.
     * Makes the thumbnail container sortable.
     * Insert the slide elements sorted into the slides array.
     * @param slides string[] array of html strings
     */
    public initSlides(slides:string[]):void {
        helper.SlideHelper.thumbnailContainer.sortable({
            axis: "y",
            stop: () => {
                this.onSlideMoved(this.SelectedSlide);
            }
        });

        for (var i:number = 0; i < slides.length; i++)
            helper.SlideHelper.slideContainer.append(slides[i]);

        helper.SlideHelper.slideContainer.children(".uiSlide").each((index, element) => {
            var slide = new view.UISlideView($(element));

            if (index != 0)
                slide.Element.hide();

            this.Slides.addSorted(slide, function (slide:view.UISlideView) {
                return slide.Index;
            });
        });


        this.selectSlide(0);
    }

    /**
     * Returns true, if any selected element is instance of AbstractUITextView is currently editable.
     * @returns boolean
     */
    private anyElementIsEditing():boolean {
        for (var i = 0; i < this.SelectedElements.length; i++) {
            var element:view.UISlideElementView = this.SelectedElements[i];
            if (element instanceof view.AbstractUITextView && (<view.AbstractUITextView>element).isEditable())
                return true;
        }
        return false;
    }

    /**
     * Select a slide by a given index.
     */
    private selectSlide(index:number):void {
        this.selectedSlide = this.Slides[index];
        this.selectedSlide.select(true);
        this.slidePropertyPanel.update();
    }

    /***
     * A slide thumbnail was moved to a different position in the slide sidebar.
     * Get the new index and shift the slides right or left wether the new index is lower
     * or higher then before.
     */
    private onSlideMoved(slide:view.UISlideView):void {
        var newIndex = -1;
        helper.SlideHelper.thumbnailContainer.children().each((index:number, element:Element) => {
            if ($(element).data("id") == slide.Id) {
                newIndex = index;
                return false;
            }
            return true;
        });

        if (newIndex == slide.Index)
            return;

        if (newIndex > slide.Index) // shift left
            for (var i = slide.Index; i <= newIndex; i++)
                this.Slides[i].Index -= 1;
        else if (newIndex < slide.Index) // shift right
            for (var i = newIndex; i < slide.Index; i++)
                this.Slides[i].Index += 1;

        /* Reorder the slide element within the slide container */
        slide.Element.detach();
        if (newIndex == 0)
            helper.SlideHelper.getSlideElement(newIndex).before(slide.Element); // insert as first element
        else
            helper.SlideHelper.getSlideElement(newIndex - 1).after(slide.Element); // get prev element and insert after that

        /* Sort the slides array now that we changed the index. */
        slide.Index = newIndex;
        this.Slides.sort((a:view.UISlideView, b:view.UISlideView) => {
            return a.Index > b.Index ? 1 : -1;
        });
        slide.hasChanged();
    }

    /**
     * Return the slide by a given id.
     */
    private getSlideById(id:string):view.UISlideView {
        for (var i = 0; i < this.Slides.length; i++)
            if (this.Slides[i].Id == id)
                return this.Slides[i];
        return null;
    }

    /**
     * Increases the index of all slides by one starting at the given index.
     */
    private shiftSlidesRight(index:number):void {
        if (index > this.Slides.length)
            return;
        for (var i = index; i < this.Slides.length; i++)
            this.Slides[i].Index += 1;
    }

    /**
     * Decrease the index of all slides by one starting at the given index.
     */
    private shiftSlidesLeft(index:number):void {
        for (var i = index; i < this.Slides.length; i++)
            this.Slides[i].Index -= 1;
    }

    /**
     * If one slide element is selected and it's z index is not already 0, it's z index will be reduced by one.
     * The z index of the slide element behind the selected slide element will by increased by one.
     * Both slide elements switch their positions.
     */
    private bringElementToBack():void {
        if (this.SelectedElements.length != 1)
            return;

        var element:view.UISlideElementView = this.SelectedElements[0];
        if (element.Z == 0) // element is already the farthest
            return;

        // bring the prev to front and the current to back
        var prevElement:view.UISlideElementView = this.getPreviousElement(element.Z);
        prevElement.Z = prevElement.Z + 1;
        prevElement.hasChanged();

        element.Z = element.Z - 1;
        element.hasChanged();

        this.updateToolbarButtons(element);
    }

    /**
     * If one slide element is selected and it's z index is not already the number of slide elements - 1,
     * it's z index will be increased by one. The z index of the slide element before the selected slide element
     * will by reduced by one - Both slide elements switch their positions.
     */
    private bringElementToFront():void {
        if (this.SelectedElements.length != 1)
            return;

        var element:view.UISlideElementView = this.SelectedElements[0];
        if (element.Z == this.SelectedSlide.Content.length - 1) // element is not the first one
            return;

        // bring the next element to back and the current to front
        var nextElement:view.UISlideElementView = this.getNextElement(element.Z);
        nextElement.Z = nextElement.Z - 1;
        nextElement.hasChanged();

        element.Z = element.Z + 1;
        element.hasChanged();

        this.updateToolbarButtons(element);
    }

    /**
     * Get the slide element from the selected slide with z index = z-1.
     * @returns UISlideElementView
     */
    private getPreviousElement(z:number):view.UISlideElementView {
        for (var i = 0; i < this.SelectedSlide.Content.length; i++)
            if (z - 1 == this.SelectedSlide.Content[i].Z)
                return this.SelectedSlide.Content[i];
        return null;
    }

    /**
     * Get the slide element from the selected slide with z index = z+1.
     * @returns UISlideElementView
     */
    private getNextElement(z:number):view.UISlideElementView {
        for (var i = 0; i < this.SelectedSlide.Content.length; i++)
            if (z + 1 == this.SelectedSlide.Content[i].Z)
                return this.SelectedSlide.Content[i];
        return null;
    }

    /**
     * Move all selected slide elements down 1px when the element is not editable.
     */
    private onArrowDown():void {
        for (var i = 0; i < this.SelectedElements.length; i++) {
            var element:view.UISlideElementView = this.SelectedElements[i];
            if (element instanceof view.AbstractUITextView)
                if ((<view.AbstractUITextView>element).isEditable())
                    continue;
            element.Y = element.Y + 1;
        }
    }

    /**
     * Move all selected slide elements down 1px when the element is not editable.
     */
    private onArrowLeft():void {
        for (var i = 0; i < this.SelectedElements.length; i++) {
            var element:view.UISlideElementView = this.SelectedElements[i];
            if (element instanceof view.AbstractUITextView)
                if ((<view.AbstractUITextView>element).isEditable())
                    continue;
            element.X = element.X - 1;
        }
    }

    /**
     * Move all selected slide elements right 1px when the element is not editable.
     */
    private onArrowRight():void {
        for (var i = 0; i < this.SelectedElements.length; i++) {
            var element:view.UISlideElementView = this.SelectedElements[i];
            if (element instanceof view.AbstractUITextView)
                if ((<view.AbstractUITextView>element).isEditable())
                    continue;
            element.X = element.X + 1;
        }
    }

    /**
     * Move all selected slide elements up 1px when the element is not editable.
     */
    private onArrowUp():void {
        for (var i = 0; i < this.SelectedElements.length; i++) {
            var element:view.UISlideElementView = this.SelectedElements[i];
            if (element instanceof view.AbstractUITextView)
                if ((<view.AbstractUITextView>element).isEditable())
                    continue;
            element.Y = element.Y - 1;
        }
    }

    /**
     * If the thumbnail is highlighted, we remove the highlighted slide.
     * Else we remove all selected slide elements.
     */
    private onDeleteKey():void {
        if (this.SelectedSlide.Thumbnail.isHighlighted())
            if (this.Slides.length >= 2)
                this.SelectedSlide.remove(true);

        for (var i = 0; i < this.SelectedElements.length; i++)
            if (!this.elementIsEditing(this.SelectedElements[i]))
                this.SelectedElements[i].remove(true);

        this.SelectedSlide.SelectedElements = [];
    }

    private elementIsEditing(element:view.UISlideElementView):boolean {
        if (element instanceof view.AbstractUITextView)
            if ((<view.AbstractUITextView>element).isEditable())
                return true;

        if (element instanceof view.UITableView)
            if ((<view.UITableView>element).isEditing())
                return true;
        return false;
    }

    /**
     * Check if the toolbar buttons should be displayed or not.
     */
    private updateToolbarButtons(element:view.UISlideElementView):void {
        UIToolbar.enableToBackButton();
        UIToolbar.enableToFrontButton();

        if (element.Z == this.SelectedSlide.Content.length - 1)
            UIToolbar.disableToFrontButton();

        if (element.Z == 0)
            UIToolbar.disableToBackButton();
    }

    /**
     * Returns true if the keycode is an arrow key.
     */
    private isArrowKey(keycode:number):boolean {
        //TODO:: maybe better somewhere else?
        switch (keycode) {
            case watcher.KeyboardWatcher.ARROW_DOWN:
            case watcher.KeyboardWatcher.ARROW_LEFT:
            case watcher.KeyboardWatcher.ARROW_RIGHT:
            case watcher.KeyboardWatcher.ARROW_UP:
                return true;
            default:
                return false;
        }
    }

    /**
     * A slide was created via WebSocket.
     */
    private onSlideCreated(message:ws.SlideCreatedMessage):void {
        var insertIndex = $(message.createdView).data("index");

        this.shiftSlidesRight(insertIndex);

        helper.SlideHelper.insertThumbnail(message.thumbnail, insertIndex);
        var element = helper.SlideHelper.insertSlide(message.createdView, insertIndex);

        var slide:view.UISlideView = new view.UISlideView(element);
        this.slides.addSorted(slide, function (slide:view.UISlideView):number {
            return slide.Index;
        });
    }

    /**
     * A slide element was created via WebSocket.
     * If the slide element is a image we do some resizing to fit the image to the slide and center it.
     */
    private onElementCreated(message:ws.ViewCreatedMessage):void {
        var slide:view.UISlideView = this.getSlideById(message.slideId);
        var slideElement:view.UISlideElementView = slide.insertElement($(message.createdView).appendTo(slide.Element), message.viewType);

        if (slideElement instanceof view.UIImageView) {
            if (message.sender != CON_ID)
                return;

            var uiImageView:view.UIImageView = <view.UIImageView>slideElement;
            var ratio = uiImageView.AspectRatio;
            var width = -1;
            var height = -1;

            if (uiImageView.isPortrait()) {
                height = slide.Element.height() / 2;
                width = Math.round(height * ratio);
            } else {
                width = slide.Element.width() / 2;
                height = Math.round(width / ratio);
            }

            uiImageView.Width = width;
            uiImageView.Height = height;
            uiImageView.X = slide.Element.width() / 2 - width / 2;
            uiImageView.Y = slide.Element.height() / 2 - height / 2;
            uiImageView.Z = slide.Content.length - 1;
            uiImageView.hasChanged();
            this.updateToolbarButtons(uiImageView);
        }

    }

    /**
     * A slide element was changed via WebSocket.
     * Update the values of the element and refresh the thumbnail.
     */
    private onViewChanged(message:ws.OnSlideElementChangedMessage):void {
        var slide:view.UISlideView = this.getSlideById(message.slideId);
        var slideElementView:view.UISlideElementView = slide.getElementById(message.view.id);
        slideElementView.updateView(message.view);
        this.updateToolbarButtons(slideElementView);
        slide.Thumbnail.refresh();
    }

    /**
     * A slide was removed via WebSocket.
     */
    private onSlideRemoved(message:ws.OnSlideRemovedMessage):void {
        var slide = this.getSlideById(message.slideId);
        slide.remove(false);
        this.removeSlide(slide, false);
    }

    /**
     * A slide was changed via WebSocket.
     * Update values and refresh the property view.
     */
    private onSlideChanged(message:ws.OnSlideChangedMessage):void {
        var slide = this.getSlideById(message.view.id);

        if (slide.Index != message.view.index) {
            var slideThumbnailElement = helper.SlideHelper.getThumbnailElement(slide.Index);
            slideThumbnailElement.detach();
            helper.SlideHelper.insertThumbnailElement(slideThumbnailElement, message.view.index);
            this.onSlideMoved(slide);
        }

        slide.updateView(message.view);
        slide.Thumbnail.refresh();
        this.slidePropertyPanel.update();
    }

    /**
     * A slide element was removed via WebSocket.
     */
    private onSlideElementRemoved(message:ws.OnSlideElementRemovedMessage):void {
        var slide = this.getSlideById(message.slideId);
        var slideElement:view.UISlideElementView = slide.getElementById(message.elementId);
        slideElement.remove(false);
    }

    /**
     * Reorder the indexes of the slides and removes it from the slides array.
     * Then the previous slide gets selected.
     */
    private removeSlide(slide:view.UISlideView, select:boolean):void {
        var index = slide.Index;
        this.shiftSlidesLeft(index);
        this.Slides.remove(slide);
        if (index != 0)
            index--;

        if (select)
            this.selectSlide(index);
    }

    /**
     * Display the given slide and update the slide properties.
     */
    private changeSlide(selectingSlide:view.UISlideView):void {
        if (selectingSlide != this.selectedSlide) {
            this.selectedSlide.unselect();
            this.selectedSlide = selectingSlide;
            this.selectedSlide.select(false);
            this.slidePropertyPanel.update();
        }
    }
}

/**
 * Wrapper class for various JQuery/Jquery UI functions.
 * CSS class constants, color, Styling with JQuery animations.
 */
export class UIKit {

    /* CSS classes */
    public static UITexView = "uiTextView";
    public static UIImageView = "uiImageView";
    public static UIScriptView = "uiScriptView";
    public static UITableView = "uiTableView";
    public static UISlide = "uiSlide";
    public static UISlideElement = "uiSlideElement";

    /* Not really used yet. */
    public static zoomScaleX:number = 1;
    public static zoomScaleY:number = 1;
    public static aspectRatio = 4 / 3;

    /* JQuery UI classes */
    public static selected:string = "ui-selected";
    public static draggable:string = "ui-draggable";
    public static dragging:string = "ui-draggable-dragging";
    public static resizable:string = "ui-resizable";
    public static selectee:string = "ui-selectee";
    public static disabled:string = "ui-state-disabled";

    public static transparent:string = "transparent";
    public static editable:string = "editable";

    /* Color definitions */
    public static uiStateSelectedColor:string = "rgb(86, 86, 86)";
    public static uiStateActiveColor:string = "rgb(145, 145, 145)";
    public static uiStateHoverColor:string = "rgb(170, 170, 170)";
    public static uiStateNormalColor:string = "rgb(196, 196, 196)";
    public static uiRedColor:string = "rgb(231, 58, 76)";
    public static uiRedColorLighter:string = "rgb(237, 107, 107)";

    /* Slide thumbnail side bar related */
    private static sidebarTimer:number;
    private static dragBarTimer:number;
    private static dragger:JQuery;
    private static dragbar:JQuery;
    private static sidebar:JQuery;

    private static toolbarHeight:number = 0;

    constructor() {

        var toolBarBottomButtonsSelector = ".ui-toolbar .bottom .ui-button";
        var toolBarTopButtonsSelector = ".ui-toolbar .top .ui-button";
        var animationSpeed = 90;

        $(toolBarBottomButtonsSelector).mouseenter(function () {
            if ($(this).hasClass(UIKit.disabled))
                return;

            $(this).animate({
                backgroundColor: UIKit.uiStateHoverColor
            }, animationSpeed);
        });

        $(toolBarBottomButtonsSelector).mouseleave(function () {
            $(this).animate({
                backgroundColor: UIKit.uiStateNormalColor
            }, animationSpeed);
        });

        $(toolBarTopButtonsSelector).mouseenter(function () {
            if ($(this).hasClass(UIKit.disabled))
                return;

            $(this).find(".icon").animate({
                backgroundColor: UIKit.uiRedColorLighter
            }, animationSpeed);
        });

        $(toolBarTopButtonsSelector).mouseleave(function () {
            $(this).find(".icon").animate({
                backgroundColor: UIKit.uiRedColor
            }, animationSpeed);
        });

    }

    /**
     * Make element contenteditable.
     */
    public static enableEditable(element:JQuery):void {
        element.attr("contenteditable", "true");
        element.addClass("editable");
    }

    /**
     * Disable contenteditable of an element.
     */
    public static disableEditable(element:JQuery) {
        element.attr("contenteditable", "false");
        element.removeClass("editable");
    }


    /**
     * Enable multi selectable on element using JQuery UI selectable.
     */
    public static enableMultiSelectable(element:JQuery, selected:(event, ui)=>void, selecting:(event, ui)=>void, unselecting:(event, ui)=>void, start:(event, ui)=>void):void {
        element.selectable({
            filter: "div.uiSlideElement",
            selected: (event, ui) => {
                selected(event, ui);
            },
            selecting: (event, ui)  => {
                selecting(event, ui);
            },
            unselecting: (event, ui) => {
                unselecting(event, ui);
            },
            start: (event, ui) => {
                start(event, ui);
            }
        });
    }

    /**
     * Disable multi selectable support
     */
    public static disableMultiSelectable(element:JQuery) {
        element.selectable();
        element.selectable("destroy");
    }

    /**
     * Enable drag support for multiple selected slide element.
     */
    public static enableMultiDraggable(view:view.UISlideElementView):void {

        var positionLeft:number = 0;
        var positionTop:number = 0;

        var maxX:number = 0;
        var maxY:number = 0;
        var minX:number = 0;
        var minY:number = 0;

        var element:JQuery = view.Element;
        var gridSize:number = 1;
        element.draggable({
            grid: [gridSize, gridSize],
            stop: function () {
                view.Slide.contentHasChanged();
            },
            start: onStart,
            drag: onDrag
        });

        /**
         * Determines the max and min values for x and y.
         * Source: http://gungfoo.wordpress.com/2013/02/15/jquery-ui-resizabledraggable-with-transform-scale-set/
         */
        function onStart(event, ui:JQueryUI.DraggableEventUIParams) {
            maxX = view.Slide.Element.offset().left + view.Slide.Element.outerWidth();
            maxY = view.Slide.Element.offset().top + view.Slide.Element.outerHeight();
            minX = view.Slide.Element.offset().left;
            minY = view.Slide.Element.offset().top;
            positionLeft = Math.round(ui.position.left / UIKit.zoomScaleX);
            positionTop = Math.round(ui.position.top / UIKit.zoomScaleY);
            ui.position.left = 0;
            ui.position.top = 0;
        }

        /**
         * Calculates the drag distance for x and y coordinate and sets it for all selected slide elements.
         */
        function onDrag(event, ui:JQueryUI.DraggableEventUIParams) {

            if (event.pageX > maxX)
                return false;

            if (event.pageY > maxY)
                return false;

            if (event.pageX < minX)
                return false;

            if (event.pageY < minY)
                return false;

            var changeLeft = ui.position.left - ui.originalPosition.left; // find change in left
            var newLeft = Math.round(ui.originalPosition.left + changeLeft / UIKit.zoomScaleX); // adjust new left by our zoomScale

            var changeTop = ui.position.top - ui.originalPosition.top; // find change in top
            var newTop = Math.round(ui.originalPosition.top + changeTop / UIKit.zoomScaleY); // adjust new top by our zoomScale

            var diffX = Math.round(ui.position.left / UIKit.zoomScaleX) - positionLeft;
            var diffY = Math.round(ui.position.top / UIKit.zoomScaleY) - positionTop;

            positionLeft = Math.round(ui.position.left / UIKit.zoomScaleX);
            positionTop = Math.round(ui.position.top / UIKit.zoomScaleY);

            $(".uiSlideElement." + UIKit.selected).each(function () {
                if ($(this) != event.target) {
                    $(this).css({
                        'top': "+=" + diffY,
                        'left': "+=" + diffX
                    });
                }
            });

            ui.position.left = newLeft;
            ui.position.top = newTop;
            return true;
        }
    }

    public static disableDraggable(element:JQuery) {
        element.draggable('destroy');
    }

    /**
     * Enable resizeable support for slide element.
     * @param handles n, e, s, w, ne, se, sw, nw, all
     * @param view
     */
    public static enableResizable(view:view.UISlideElementView, handles:string) {
        view.Element.resizable({
            minHeight: 4 / UIKit.zoomScaleY,
            minWidth: 4 / UIKit.zoomScaleX,
            handles: handles,
            stop: function () {
                view.hasChanged();
            }
        });
    }

    public static disableResizable(element:JQuery) {
        element.resizable("destroy");
    }

    /**
     * Scales an element using CSS3 transform
     */
    public static transformScale(selector:JQuery, scaleX:number, scaleY:number, originX:number, originY:number) {
        selector.css("transform-origin", originX + "px " + originY + "px 0px");
        selector.css("-webkit-transform-origin", originX + "px " + originY + "px 0px");
        selector.css("-o-transform-origin", originX + "px " + originY + "px 0px");
        selector.css("transform", "scale(" + scaleX + "," + scaleY + ")");
        selector.css("-ms-transform", "scale(" + scaleX + "," + scaleY + ")");
        selector.css("-webkit-transform", "scale(" + scaleX + "," + scaleY + ")");
    }

    /**
     * Requests fullscreen on an element
     */
    public static requestFullScreen(element:Element) {
        var prefixes = ["webkit", "moz", "ms", "o", ""];
        var method = "RequestFullScreen";

        for (var i = 0; i < prefixes.length; i++) {
            var prefixedMethod = prefixes[i] + method;
            var t = typeof element[prefixedMethod];
            if (t != 'undefined')
                element[prefixedMethod]();
        }
    }

    /**
     * Exit the fullscreen mode.
     */
    public static exitFullScreen() {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
    }

    /**
     * Init the slide thumbnail using the mCustomScrollbar plugin
     * and simulates an OSX like scroll bar.
     */
    public static initScrollbar() {

        UIKit.sidebar = $(".uiSlideSidebar");

        UIKit.sidebar.mCustomScrollbar({
            scrollButtons: {
                enable: false
            },
            theme: "dark",
            advanced: {
                updateOnContentResize: true
            },
            scrollInertia: 200,
            mouseWheelPixels: 40
        });


        UIKit.dragger = $(".mCSB_dragger");
        UIKit.dragbar = $(".mCSB_dragger_bar");

        var onDrag:boolean = false;

        UIKit.dragger.mousedown(function () {
            onDrag = true;
        });

        $("body").mouseup(function () {
            if (onDrag) {
                onDrag = false;
                UIKit.shrinkDragbar();
                UIKit.hideSidebar();
            }
        });

        UIKit.dragbar.mouseleave(() => {
            if (UIKit.dragbar.parent().hasClass("mCSB_dragger_onDrag"))
                return;
            UIKit.shrinkDragbar();
        });

        UIKit.dragbar.mouseover(() => {
            window.clearTimeout(UIKit.dragBarTimer);
            UIKit.expandDragbar();
        });

        UIKit.sidebar.mouseover(() => {
            window.clearTimeout(UIKit.sidebarTimer);
            UIKit.showSidebar();
        });

        UIKit.sidebar.mouseleave(() => {
            if ($(".mCSB_dragger").hasClass("mCSB_dragger_onDrag"))
                return;
            UIKit.hideSidebar();
        });

        UIKit.dragger.hide();

        $(window).resize(() => {
            UIKit.resizeScrollbar();
        });

        helper.SlideHelper.thumbnailContainer = UIKit.sidebar.find(".mCSB_container");
    }

    public static resizeScrollbar():void {
        if (UIKit.toolbarHeight == 0) {
            var toolbar = $(".ui-toolbar");
            UIKit.toolbarHeight = toolbar.height();
            UIKit.sidebar.css("min-height", $(".ui-main-view").height() + "px");
        }

        UIKit.sidebar.height($(window).height() - UIKit.toolbarHeight);
    }

    private static showSidebar() {
        $(".mCSB_dragger").fadeIn('fast');
    }

    private static hideSidebar() {
        UIKit.sidebarTimer = setTimeout(() => {
            $(".mCSB_dragger").fadeOut('fast');
        }, 500);
    }

    private static expandDragbar() {
        $(UIKit.dragbar).animate({
            width: 8,
            right: -1
        }, 200);
    }

    private static shrinkDragbar() {
        UIKit.dragBarTimer = setTimeout(function () {
            $(UIKit.dragbar).animate({
                width: 4,
                right: -3
            }, 200);
        }, 500);
    }
}

/**
 * Represents a RGB or RGBA color.
 * Can be created from a css string and can produce css styles.
 * Can be converted to an object literal of type RGBA.
 */
export class UIColor {
    public r:number;
    public g:number;
    public b:number;
    public a:number;

    /**
     * @param r 0-1
     * @param g 0-1
     * @param b 0-1
     * @param a 0-1
     */
    constructor(r:number, g:number, b:number, a:number) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    /**
     * Generates an instance from an object literal.
     */
    public static fromRGBA(c:RGBA):UIColor {
        return new UIColor(c.r, c.g, c.b, c.a);
    }

    /**
     * Generates and instance from an css style string.
     */
    public static fromString(c:string):UIColor {
        var rgba = c.split(",");
        var red = rgba[0];

        var r:number = parseInt(red.substring(red.indexOf("(") + 1, red.length), 10);
        var g:number = parseInt(rgba[1], 10);
        var b:number = parseInt(rgba[2], 10);
        var a:number = 1;

        if (rgba.length == 4)
            a = parseFloat(rgba[3].substr(0, rgba[3].length - 1));

        return new UIColor(r / 255, g / 255, b / 255, a);
    }

    /**
     * Example: rgba(70, 70, 70, 0.5)
     */
    public toRGBAString() {
        return "rgba(" + Math.round(255 * this.r) + ", " + Math.round(255 * this.g) + ", " + Math.round(255 * this.b) + ", " + this.a + ")";
    }

    /**
     * Example: rgb(70, 70, 70)
     */
    public toRGBString() {
        return "rgb(" + Math.round(255 * this.r) + ", " + Math.round(255 * this.g) + ", " + Math.round(255 * this.b) + ")";
    }

    /**
     * Converts color to an object literal.
     */
    public toObject() {
        return <RGBA>{
            r: this.r,
            g: this.g,
            b: this.b,
            a: this.a
        }
    }
}

export class Flash {

    private static container = $(".flash-container");
    private static duration = 2000;
    private static isAnimating:boolean = false;

    private static queue:string[] = [];

    public static make(message:string):void {
        if (Flash.isAnimating) {
            Flash.queue.push(message);
            return;
        }

        Flash.isAnimating = true;
        Flash.container.find(".message").html(message);
        Flash.container.animate({
            top: 0
        }).delay(Flash.duration).animate({
            top: "-40px"
        }, () => {
            Flash.isAnimating = false;
            Flash.clear();

            if (Flash.queue.length > 0)
                Flash.make(Flash.queue.shift())
        });

    }

    private static clear():void {
        Flash.container.find(".message").html("");
    }

}