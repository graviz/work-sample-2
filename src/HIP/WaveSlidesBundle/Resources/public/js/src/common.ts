///<reference path="../definitions/references.d.ts" />

interface MousePosition {
    x: number;
    y: number;
}

export class System {

    public static WINDOWS:string = "windows";
    public static MAC:string = "mac";
    public static UNIX:string = "unix";
    public static LINUX:string = "linux";
    public static UNKNOWN_OS:string = "unknownOS";

    public static OPERA:string = "opera";
    public static SAFARI:string = "safari";
    public static FIREFOX:string = "firefox";
    public static CHROME:string = "chrome";
    public static IE:string = "ie";

    private static os:string = null;
    private static browser:string = null;

    public static Mouse:MousePosition = null;

    constructor(){
        var body = $("body");
        body.addClass(System.getOs());
        body.addClass(System.browserName());

        System.Mouse = {x: 0, y: 0};

        document.addEventListener('mousemove', function (e) {
            System.Mouse.x = e.clientX || e.pageX;
            System.Mouse.y = e.clientY || e.pageY
        }, false);
    }

    public static getOs() {
        if (System.os != null)
            return System.os;

        System.os = System.UNKNOWN_OS;
        if (navigator.appVersion.indexOf("Win") != -1) System.os = System.WINDOWS;
        if (navigator.appVersion.indexOf("Mac") != -1) System.os = System.MAC;
        if (navigator.appVersion.indexOf("X11") != -1) System.os = System.UNIX;
        if (navigator.appVersion.indexOf("Linux") != -1) System.os = System.LINUX;
        return System.os;
    }

    public static browserName() {

        if (System.browser != null)
            return System.browser;

        var nVer = navigator.appVersion;
        var nAgt = navigator.userAgent;
        var browserName = navigator.appName;
        var fullVersion = '' + parseFloat(navigator.appVersion);
        var majorVersion = parseInt(navigator.appVersion, 10);
        var nameOffset, verOffset, ix;

        // In Opera, the true version is after "Opera" or after "Version"
        if ((verOffset = nAgt.indexOf("Opera")) != -1) {
            browserName = System.OPERA;
            fullVersion = nAgt.substring(verOffset + 6);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
        // In MSIE, the true version is after "MSIE" in userAgent
        else if ((verOffset = nAgt.indexOf("MSIE")) != -1) {
            browserName = System.IE;
            fullVersion = nAgt.substring(verOffset + 5);
        }
        // In Chrome, the true version is after "Chrome"
        else if ((verOffset = nAgt.indexOf("Chrome")) != -1) {
            browserName = System.CHROME;
            fullVersion = nAgt.substring(verOffset + 7);
        }
        // In Safari, the true version is after "Safari" or after "Version"
        else if ((verOffset = nAgt.indexOf("Safari")) != -1) {
            browserName = System.SAFARI;
            fullVersion = nAgt.substring(verOffset + 7);
            if ((verOffset = nAgt.indexOf("Version")) != -1)
                fullVersion = nAgt.substring(verOffset + 8);
        }
        // In Firefox, the true version is after "Firefox"
        else if ((verOffset = nAgt.indexOf("Firefox")) != -1) {
            browserName = System.FIREFOX;
            fullVersion = nAgt.substring(verOffset + 8);
        }
        // In most other browsers, "name/version" is at the end of userAgent
        else if ((nameOffset = nAgt.lastIndexOf(' ') + 1) <
            (verOffset = nAgt.lastIndexOf('/'))) {
            browserName = nAgt.substring(nameOffset, verOffset);
            fullVersion = nAgt.substring(verOffset + 1);
            if (browserName.toLowerCase() == browserName.toUpperCase()) {
                browserName = navigator.appName;
            }
        }

        // trim the fullVersion string at semicolon/space if present
        if ((ix = fullVersion.indexOf(";")) != -1)
            fullVersion = fullVersion.substring(0, ix);
        if ((ix = fullVersion.indexOf(" ")) != -1)
            fullVersion = fullVersion.substring(0, ix);

        majorVersion = parseInt('' + fullVersion, 10);
        if (isNaN(majorVersion)) {
            fullVersion = '' + parseFloat(navigator.appVersion);
            majorVersion = parseInt(navigator.appVersion, 10);
        }

        System.browser = browserName.toLowerCase();

        return System.browser;
    }

}
