///<reference path="../definitions/references.d.ts" />

/**
 * Play sounds using Audio objects.
 */
export class AudioManager {

    private static dir:string = "/bundles/hipwaveslides/audio/";

    public static playSound(file:string) {
        new Audio(AudioManager.dir + file).play();
    }

}