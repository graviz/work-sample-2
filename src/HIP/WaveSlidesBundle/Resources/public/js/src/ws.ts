///<reference path="../definitions/references.d.ts" />

import watcher = require("src/watcher");
import view = require("src/view");
import ui = require("src/ui");

/**
 * Stores user data of all connected users.
 */
export class UserPool {

    private connectedUsers:User[] = [];
    private static instance:UserPool;

    constructor(users:User[]) {
        this.connectedUsers = users;

        Socket.on(inc.UserJoined, (message:IncomingMessage)=> {
            this.userJoined(<UserJoinedMessage>message);
        });

        Socket.on(inc.UserLeft, (message:IncomingMessage)=> {
            this.userLeft(<UserLeftMessage>message);
        });
    }

    public static init(users:User[]):void {
        UserPool.instance = new UserPool(users);
    }

    public static users():User[] {
        return UserPool.instance.connectedUsers;
    }

    public static get(userId:string):User {
        for (var i = 0; i < UserPool.instance.connectedUsers.length; i++)
            if (UserPool.instance.connectedUsers[i].id == userId)
                return UserPool.instance.connectedUsers[i];
        return null;
    }

    private userJoined(message:UserJoinedMessage):void {
        this.connectedUsers.push(message.user);
        ui.Flash.make("User " + message.user.name + " joined the presentation.");
    }

    private userLeft(message:UserLeftMessage):void {
        var user:User = null;
        for (var i = 0; i < this.connectedUsers.length; i++)
            if (this.connectedUsers[i].id == message.userId)
                user = this.connectedUsers[i];

        ui.Flash.make("User " + user.name + " left the presentation.");
        this.connectedUsers.remove(user);
    }

}

/**
 * Build outgoing messages.
 */
export class MessageBuilder {

    public static buildSelectedMessage(slideId:string, elementId:string):SlideElementSelectedMessage {
        return <SlideElementSelectedMessage>{
            type: bi.SlideElementSelected,
            slideId: slideId,
            elementId: elementId,
            userId: USER_ID
        };
    }

    public static buildUnselectedMessage(slideId:string, elementId:string):SlideElementUnselectedMessage {
        return <SlideElementSelectedMessage>{
            type: bi.SlideElementUnselected,
            slideId: slideId,
            elementId: elementId,
            userId: USER_ID
        };
    }

    public static buildChatMessage(text):ChatMessage {
        return <ChatMessage> {
            type: bi.Chat,
            text: text
        };
    }

    public static buildHandshakeMessage():HandshakeMessage {
        return <HandshakeMessage>{
            type: out.Handshake,
            session: SESSION_ID,
            deck: DECK_ID
        };
    }

    public static buildCreateSlideMessage():CreateSlideMessage {
        return <CreateSlideMessage>{
            type: out.CreateSlide
        };
    }

    public static buildCreateSlideElementMessage(viewType:string, slideId:string):CreateSlideElementMessage {
        return <CreateSlideElementMessage>{
            type: out.CreateSlideElement,
            slideId: slideId,
            viewType: viewType
        };
    }

    public static buildSlideElementChangedMessage(view:view.UISlideElementView):SlideElementChangedMessage {
        return <SlideElementChangedMessage>{
            type: out.SlideElementChanged,
            view: view.toObject(),
            slideId: view.SlideId
        };
    }

    public static buildChangeActiveSlideMessage(slideId:string):ChangeActiveSlideMessage {
        return <ChangeActiveSlideMessage>{
            type: out.ChangeActiveSlide,
            slideId: slideId
        };
    }

    public static buildSlideRemovedMessage(slideId:string):SlideRemovedMessage {
        return <SlideRemovedMessage>{
            type: out.SlideRemoved,
            slideId: slideId
        };
    }

    public static buildSlideChangedMessage(slide:view.UISlideView):SlideChangedMessage {
        return <SlideChangedMessage>{
            type: out.SlideChanged,
            view: slide.toObject()
        }
    }

    public static buildSlideElementRemovedMessage(elementId:string, slideId:string):SlideElementRemovedMessage {
        return <SlideElementRemovedMessage>{
            type: out.SlideElementRemoved,
            elementId: elementId,
            slideId: slideId
        };
    }
}

/**
 * WebSocket connection wrapper class.
 */
export class Socket implements watcher.ViewObserver,watcher.SelectionObserver {

    private ws:WebSocket = null;

    private static MessageListeners = {};

    /* When the connection is open, a HandshakeMessage will be sent. */
    constructor(url:string) {

        Socket.on(inc.Authentication, (message:IncomingMessage)=> {
            this.onAuthenticated(<AuthenticationMessage>message);
        });

        try {
            this.ws = new WebSocket(url);

            this.ws.onopen = () => {
                console.log("Connection open");
                this.send(MessageBuilder.buildHandshakeMessage());
            };

            this.ws.onmessage = (e) => {
                var message = JSON.parse(e.data);
                Socket.MessageListeners[message.type].forEach((callback)=> {
                    callback(message);
                });
            };

            this.ws.onclose = () => {
                console.log("Connection onclose");
            };

            this.ws.onerror = (e) => {
                console.log(e.type);
            };

        } catch (e) {
            console.error(e.message);
        }

        watcher.ViewWatcher.register(this);
        watcher.SelectionWatcher.register(this);
    }

    public static on(type:string, callback:(message:IncomingMessage)=>void) {
        if (Socket.MessageListeners[type] == null)
            Socket.MessageListeners[type] = [];

        Socket.MessageListeners[type].push(callback);
    }

    /**
     * Sends a Message object.
     */
    public send(message:Message):void {
        if (!this.isConnected())
            return;

        try {
            var stringMessage = JSON.stringify(message);
            console.log("SEND: ", message);
            console.log("Length: ", stringMessage.length);

            this.ws.send(stringMessage);
        } catch (e) {
            console.error(e.message);
        }
    }

    public isConnected():boolean {
        return this.ws.readyState == WebSocket.OPEN;
    }

    /**
     * Closes the WebSocket connection.
     */
    public close():void {
        this.ws.close();
    }

    public onSelectionMessage(message:string, object:any):void {
        switch (message) {
            case watcher.SelectionWatcher.SelectedEventMessage:
                if (object instanceof view.UISlideThumbnailView)
                    this.slideSelectionChanged((<view.UISlideThumbnailView>object).Id);
                if (object instanceof view.UISlideElementView)
                    this.sendSelectedMessage(object);
                break;
            case watcher.SelectionWatcher.AddToSelectionMessage:
                if (object instanceof view.UISlideElementView)
                    this.sendSelectedMessage(object);
                break;
            case watcher.SelectionWatcher.UnselectedEventMessage:
                if (object instanceof view.UISlideElementView)
                    this.sendUnselectedMessage(object);
                break;
        }
    }

    public update(message:string, object:any):void {
        switch (message) {
            case watcher.ViewWatcher.ChangedEventMessage:
                if (object instanceof view.UISlideView)
                    this.onSlideChanged(object);
                else
                    this.onSlideElementChanged(<view.UISlideElementView>object);
                break;
            case watcher.ViewWatcher.SlideRemovedMessage:
                this.onSlideRemoved((<view.UISlideView>object).Id);
                break;
            case watcher.ViewWatcher.SlideElementRemovedMessage:
                this.removeSlideElement(<view.UISlideElementView>object);
                break;
        }
    }

    /**
     * Send a chat message.
     */
    public sendChatMessage(text:string):void {
        this.send(MessageBuilder.buildChatMessage(text));
    }

    /**
     * Send a message that the user want to create a slide.
     */
    public createSlide():void {
        this.send(MessageBuilder.buildCreateSlideMessage());
    }

    /**
     * Send a message that the user want to create a slide element.
     */
    public createSlideElement(viewType:string, slideId:string) {
        this.send(MessageBuilder.buildCreateSlideElementMessage(viewType, slideId));
    }

    /**
     * When the user unselects a slide element.
     */
    private sendUnselectedMessage(element:view.UISlideElementView):void {
        this.send(MessageBuilder.buildUnselectedMessage(element.SlideId, element.Id));
    }

    /**
     * When the user selects a slide element.
     */
    private sendSelectedMessage(element:view.UISlideElementView):void {
        this.send(MessageBuilder.buildSelectedMessage(element.SlideId, element.Id));
    }

    /**
     * Send a message that the user has deleted a slide element.
     */
    private removeSlideElement(element:view.UISlideElementView):void {
        this.send(MessageBuilder.buildSlideElementRemovedMessage(element.Id, element.SlideId));
    }

    /**
     * Send a message that the user has changed a slide element.
     */
    private onSlideElementChanged(slideElement:view.UISlideElementView):void {
        this.send(MessageBuilder.buildSlideElementChangedMessage(slideElement));
    }

    /**
     * Send a message that the user has changed a slide.
     */
    private onSlideChanged(slide:view.UISlideView):void {
        this.send(MessageBuilder.buildSlideChangedMessage(slide));
    }

    /**
     * Send a message that the user has selected a slide.
     */
    private slideSelectionChanged(slideId:string):void {
        this.send(MessageBuilder.buildChangeActiveSlideMessage(slideId));
    }

    /**
     * Send a message that the user has removed a slide.
     */
    private onSlideRemoved(slideId:string):void {
        this.send(MessageBuilder.buildSlideRemovedMessage(slideId));
    }

    private onAuthenticated(message:AuthenticationMessage):void {
        if (message.success) {
            CON_ID = message.sender;
        } else {
            switch (message.error) {
                case error.ERROR_SESSION_INVALID:
                    alert("Network.Message.ERROR_SESSION_INVALID");
                    break;
            }
        }
    }

}

class error {
    public static ERROR_SESSION_INVALID = 100;
}

/**
 * Message types that can be send and received.
 */
export class bi {
    public static SlideElementSelected = "SlideElementSelectedMessage";
    public static SlideElementUnselected = "SlideElementUnselectedMessage";
    public static Chat = "ChatMessage";
}

/**
 * Message types of incoming messages.
 */
export class inc {
    public static Authentication = "AuthenticationMessage";
    public static SlideElementCreated = "SlideElementCreatedMessage";
    public static OnSlideCreated = "OnSlideCreatedMessage";
    public static OnSlideElementChanged = "OnSlideElementChangedMessage";
    public static OnSlideRemoved = "OnSlideRemovedMessage";
    public static OnSlideChanged = "OnSlideChangedMessage";
    public static OnSlideElementRemoved = "OnSlideElementRemovedMessage";
    public static UserJoined = "UserJoinedMessage";
    public static UserLeft = "UserLeftMessage";
}

/**
 * Message types of outgoing messages.
 */
export class out {
    public static Handshake = "HandshakeMessage";
    public static CreateSlide = "CreateSlideMessage";
    public static CreateSlideElement = "CreateSlideElementMessage";
    public static SlideElementChanged = "SlideElementChangedMessage";
    public static ChangeActiveSlide = "ChangeActiveSlideMessage";
    public static SlideRemoved = "SlideRemovedMessage";
    public static SlideChanged = "SlideChangedMessage";
    public static SlideElementRemoved = "SlideElementRemovedMessage";
}

export interface Message {
    type: string;
    sender: string;
}

export interface BidirectionalMessage extends Message {
}

export interface ChatMessage extends BidirectionalMessage {
    text:string;
    userId:string;
    view:string;
}

export interface SelectionMessage extends BidirectionalMessage {
    elementId:string;
    slideId:string;
    userId:string;
}

export interface SlideElementSelectedMessage extends SelectionMessage {
}

export interface SlideElementUnselectedMessage extends SelectionMessage {
}

export interface OutgoingMessage extends Message {
}

export interface HandshakeMessage extends OutgoingMessage {
    session: string;
    deck: string;
}

export interface CreateSlideMessage extends OutgoingMessage {
}

export interface CreateSlideElementMessage extends OutgoingMessage {
    viewType:string;
    slideId:string;
}

export interface ChangeActiveSlideMessage extends OutgoingMessage {
    slideId: string;
}

export interface SlideElementChangedMessage extends OutgoingMessage {
    view: ViewObject;
    slideId: string;
}

export interface SlideElementRemovedMessage extends OutgoingMessage {
    elementId:string;
    slideId:string;
}

export interface SlideChangedMessage extends OutgoingMessage {
    view: SlideObject;
}

export interface SlideRemovedMessage extends OutgoingMessage {
    slideId: string;
}

export interface IncomingMessage extends Message {
}

export interface ViewCreatedMessage extends IncomingMessage {
    createdView: string;
    slideId: string;
    viewType: string;
}

export interface SlideCreatedMessage extends ViewCreatedMessage {
    thumbnail: string;
}

export interface AuthenticationMessage extends IncomingMessage {
    error:number;
    success:boolean;
    users:User[];
    chats:string[];
    deck:{
        format:string;
        slides:string[];
        thumbnails:string[];
    };
}

export interface OnSlideRemovedMessage extends IncomingMessage {
    slideId:string;
}

export interface OnSlideChangedMessage extends IncomingMessage {
    view: SlideObject;
}

export interface OnSlideElementRemovedMessage extends IncomingMessage {
    elementId: string;
    slideId: string;
}

export interface OnSlideElementChangedMessage extends IncomingMessage {
    view: SlideElementViewObject;
    slideId: string;
}

export interface UserLeftMessage extends IncomingMessage {
    userId:string;
}

export interface UserJoinedMessage extends IncomingMessage {
    user:User;
}

export interface User {
    name:string;
    id:string;
    color:RGB;
    view:string;
}