/// <reference path="EditorApp.ts" />

require.config({
    baseUrl: "/bundles/hipwaveslides/js"
});

require(
    ['EditorApp'],
    (main) => {
        new main.EditorApp();
    }
);