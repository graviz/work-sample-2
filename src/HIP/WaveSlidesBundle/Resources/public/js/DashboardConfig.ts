/// <reference path="DashboardApp.ts" />

require.config({
    baseUrl: "/bundles/hipwaveslides/js"
});

require([
    'DashboardApp'
],
    (main) => {
        new main.DashboardApp();
    }
);