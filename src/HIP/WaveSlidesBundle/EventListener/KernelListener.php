<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesBundle\EventListener;

use Doctrine\ODM\MongoDB\DocumentManager;
use HIP\WaveSlidesCoreBundle\Document\Account\SessionData;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\SecurityContext;

class KernelListener {

    /** @var SecurityContext */
    private $context;

    /** @var UrlGeneratorInterface */
    private $router;

    /** @var DocumentManager */
    private $dm;

    public function __construct(SecurityContext $context, UrlGeneratorInterface $router, DocumentManager $dm) {
        $this->context = $context;
        $this->router = $router;
        $this->dm = $dm;
    }

    public function onKernelRequest(GetResponseEvent $event) {

        $token = $this->context->getToken();

        if ($token == null)
            return;

        /** @var WaveUser $user */
        $user = $token->getUser();

        if ($user instanceof WaveUser) {
            if ($user->getSession() == null)
                $user->setSession(SessionData::generate());
            else
                $user->getSession()->refresh();

            $this->dm->flush();
        }

    }

}