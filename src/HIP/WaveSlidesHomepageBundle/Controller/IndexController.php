<?php

namespace HIP\WaveSlidesHomepageBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

/**
 * Class IndexController
 * @package HIP\WaveSlidesHomepageBundle\Controller
 */
class IndexController extends Controller {

    /**
     * @Sensio\Route("/", name="homepage_index")
     * @Sensio\Template()
     */
    public function indexAction() {
        return array();
    }
}
