<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\ZMQ;

use HIP\WaveSlidesCoreBundle\Common\Json as JsonMapper;
use HIP\WaveSlidesSocketBundle\WebSocket\Server;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * @JSON\JsonTypeInfo(use=JSON\JsonTypeInfo::ID_NAME, include=JSON\JsonTypeInfo::AS_PROPERTY, property="type")
 * @JSON\JsonSubTypes({
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesCoreBundle\ZMQ\ImageUploadedMessage", name="ImageUploadedMessage"),
 * })
 */
abstract class ZMQMessage {

    const TYPE = 'HIP\WaveSlidesCoreBundle\ZMQ\ZMQMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="sender", type="string")
     */
    public $sender;

    /**
     * @var string
     * @JSON\JsonProperty(name="type", type="string")
     */
    public $type;

    public function send() {
        /** @noinspection PhpUndefinedMethodInspection */
        ZMQHelper::createConnection()->send(JsonMapper::encode($this));
    }

    public abstract function handle(Server $server);

} 