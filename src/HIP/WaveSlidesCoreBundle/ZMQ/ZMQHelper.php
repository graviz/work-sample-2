<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\ZMQ;

use HIP\WaveSlidesCoreBundle\Common\Json;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use React\EventLoop\LoopInterface;

/** @noinspection PhpUndefinedClassInspection */
class ZMQHelper {

    const ID = 'waveslides_push';

    const ZMQ_ADDRESS = 'zmq_address';

    /**
     * @return \ZMQSocket
     */
    public static function createConnection() {
        /** @noinspection PhpUndefinedClassInspection */
        $context = new \ZMQContext();
        /** @noinspection PhpUndefinedClassInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, ZMQHelper::ID);
        /** @noinspection PhpUndefinedMethodInspection */
        $socket->connect(ServiceHelper::parameter(self::ZMQ_ADDRESS));

        return $socket;
    }

    /**
     * @param $loop
     * @return \ZMQSocket
     */
    public static function createServer(LoopInterface $loop) {
        $context = new \React\ZMQ\Context($loop);
        /** @noinspection PhpUndefinedClassInspection */
        /** @noinspection PhpUndefinedMethodInspection */
        $socket = $context->getSocket(\ZMQ::SOCKET_PULL);
        /** @noinspection PhpUndefinedMethodInspection */
        $socket->bind(ServiceHelper::parameter(ZMQHelper::ZMQ_ADDRESS));

        return $socket;
    }

    /**
     * @param $json
     * @return ZMQMessage
     */
    public static function decode($json) {
        return Json::decode($json, ZMQMessage::TYPE);
    }
} 