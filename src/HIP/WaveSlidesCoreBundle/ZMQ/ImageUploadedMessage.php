<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\ZMQ;

use HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\SlideElementCreatedMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Server;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;


class ImageUploadedMessage extends ZMQMessage {

    const Role = 'ImageUploadedMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="viewType", type="string")
     */
    public $viewType = ImageElement::Role;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    /**
     * @var string
     * @JSON\JsonProperty(name="createdView", type="string")
     */
    public $createdView;

    public function handle(Server $server) {
        $connection = $server->connection($this->sender);
        $slideElementCreatedMessage = new SlideElementCreatedMessage($connection);
        $slideElementCreatedMessage->viewType = $this->viewType;
        $slideElementCreatedMessage->createdView = $this->createdView;
        $slideElementCreatedMessage->slideId = $this->slideId;
        $slideElementCreatedMessage->sendToAll();
    }
}