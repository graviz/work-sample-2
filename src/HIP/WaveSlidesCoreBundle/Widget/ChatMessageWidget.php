<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Widget;


use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Color;

class ChatMessageWidget {

    /**
     * @param WaveUser $user
     * @param Color $c
     * @param $text
     * @return string
     */
    public static function render(WaveUser $user, $text, \DateTime $date, Color $c = null) {
        if ($c == null)
            $c = Color::make(1, 1, 1, 1);

        $color = Color::make($c->r, $c->g, $c->b, 0.3)->toCSSRGBA();
        return ServiceHelper::templating()->render('@HIPWaveSlidesCore/Widget/chat_message.html.twig', [
            'sender' => $user,
            'time' => $date->format('H:i'),
            'text' => $text,
            'color' => $color,
            'image' => '/bundles/hipwaveslides/images/profile_dummy.jpg',
        ]);
    }

}