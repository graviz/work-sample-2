<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Widget;


use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesCoreBundle\Document\Slide;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;

class SlideWidget {

    /**
     * @param Slide $slide
     * @param bool $renderElements
     * @param string $imagePath
     * @param callable $p
     * @return string
     */
    public static function render(Slide $slide, $renderElements = true, $imagePath = null, callable $p = null) {
        if ($imagePath == null)
            $imagePath = ServiceHelper::parameter('slide_image_path_web');

        if ($renderElements) {
            $elements = '';
            foreach ($slide->getElements() as $element) /** @var SlideElement $element */
                $elements .= SlideElementWidgetFactory::render($element, $imagePath, $p);
        } else {
            $elements = null;
        }

        return ServiceHelper::templating()->render('@HIPWaveSlidesCore/Widget/slide_widget.html.twig', [
            'view' => $slide,
            'elements' => $elements
        ]);
    }

    /**
     * @param ArrayCollection $slides
     * @param bool $renderElements
     * @param string $imagePath
     * @param callable $p
     * @return string
     */
    public static function renderAll($slides, $renderElements = true, $imagePath = null, callable $p = null) {
        $slides = Sorter::SortSlidesAsc($slides);

        $view = '';

        foreach ($slides as $slide)
            $view .= self::render($slide, $renderElements, $imagePath, $p);

        return $view;
    }


}