<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Widget;


use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Slide;

class SlideThumbnailWidget {

    public static function render(Slide $slide) {
        return ServiceHelper::templating()->render('@HIPWaveSlidesCore/Widget/slide_thumbnail_widget.html.twig', [
            'view' => $slide,
        ]);
    }

} 