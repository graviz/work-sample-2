<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Widget;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TextElement;

class SlideElementWidgetFactory {

    const VIEW = 'view';
    const IMAGE_PATH = 'imagePath';

    private static $templates = [
        'text' => 'HIPWaveSlidesCoreBundle:Widget/SlideElements:text_element.html.twig',
        'table' => 'HIPWaveSlidesCoreBundle:Widget/SlideElements:table_element.html.twig',
        'script' => 'HIPWaveSlidesCoreBundle:Widget/SlideElements:script_element.html.twig',
        'image' => 'HIPWaveSlidesCoreBundle:Widget/SlideElements:image_element.html.twig',
    ];

    /**
     * @param SlideElement $slideElement
     * @param string $imagePath
     * @param callable $p
     * @return string
     */
    public static function render(SlideElement $slideElement, $imagePath = null, callable $p = null) {
        $parameters = [
            self::VIEW => $slideElement
        ];

        if ($slideElement->getType() == ImageElement::Role) {
            if ($imagePath == null)
                $imagePath = ServiceHelper::parameter('slide_image_path_web');

            $parameters[self::IMAGE_PATH] = $imagePath;
        }

        if ($p != null)
            $p($slideElement);

        return ServiceHelper::templating()->render(self::template($slideElement->getType()), $parameters);;
    }

    public static function template($elementType) {
        return self::$templates[$elementType];
    }

} 