<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Locale;

use HIP\WaveSlidesCoreBundle\Document\System;

class Strings {

    /**
     * @param $key
     * @return string
     */
    public static function get($key) {
        return System::instance()->getStrings()[$key];
    }

}