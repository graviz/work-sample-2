<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * @ODM\EmbeddedDocument
 */
class Bounds {

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="width", type="int")
     */
    public $width;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="height", type="int")
     */
    public $height;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="x", type="int")
     */
    public $x;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="y", type="int")
     */
    public $y;

    /**
     * @param int $width
     * @param int $height
     * @param int $x
     * @param int $y
     * @return Bounds
     */
    public static function  make($width, $height, $x, $y) {
        $bounds = new Bounds();
        $bounds->height = $height;
        $bounds->width = $width;
        $bounds->x = $x;
        $bounds->y = $y;
        return $bounds;
    }


} 