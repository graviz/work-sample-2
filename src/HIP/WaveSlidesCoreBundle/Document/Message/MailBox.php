<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Message;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\AbstractDocument;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;

/** @ODM\Document(collection="mail_boxes") */
class MailBox extends AbstractDocument {

    /** @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\WaveUser", cascade="all") */
    protected $user;
    
    /** @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Message\Message") */
    protected $inbox;

    /** @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Message\Message") */
    protected $outbox;

    public function __construct() {
        $this->inbox = new ArrayCollection();
        $this->outbox = new ArrayCollection();
    }

    /**
     * @return WaveUser
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @param WaveUser $user
     */
    public function setUser($user) {
        $this->user = $user;
    }

    /**
     * @return ArrayCollection
     */
    public function getInbox() {
        return $this->inbox;
    }

    /**
     * @param ArrayCollection $inbox
     */
    public function setInbox($inbox) {
        $this->inbox = $inbox;
    }

    /**
     * @return ArrayCollection
     */
    public function getOutbox() {
        return $this->outbox;
    }

    /**
     * @param ArrayCollection $outbox
     */
    public function setOutbox($outbox) {
        $this->outbox = $outbox;
    }


} 