<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Message;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\EmbeddedDocument */
class Message {

    /** @ODM\String */
    protected $subject;

    /** @ODM\String */
    protected $text;

    /** @ODM\Date */
    protected $date;

    /** @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesBundle\Document\Account\WaveUser", cascade="all") */
    protected $receiver;

    /** @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesBundle\Document\Account\WaveUser", cascade="all") */
    protected $sender;

    public function __construct() {
        $this->receiver = new ArrayCollection();
        $this->sender = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getSubject() {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject) {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

    /**
     * @return DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate($date) {
        $this->date = $date;
    }

    /**
     * @return ArrayCollection
     */
    public function getReceiver() {
        return $this->receiver;
    }

    /**
     * @param ArrayCollection $receiver
     */
    public function setReceiver($receiver) {
        $this->receiver = $receiver;
    }

    /**
     * @return ArrayCollection
     */
    public function getSender() {
        return $this->sender;
    }

    /**
     * @param ArrayCollection $sender
     */
    public function setSender($sender) {
        $this->sender = $sender;
    }

}