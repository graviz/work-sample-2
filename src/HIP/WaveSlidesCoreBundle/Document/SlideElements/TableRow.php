<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class TableRow
 * @package HIP\WaveSlidesCoreBundle\Document\SlideElements
 *
 * @ODM\EmbeddedDocument
 */
class TableRow {

    /**
     * @var ArrayCollection
     * @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell")
     * @JSON\JsonProperty(name="cells", type="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell[]")
     */
    protected $cells = [];

    /**
     * @return ArrayCollection
     * @JSON\JsonProperty(name="cells", type="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell[]")
     */
    public function getCells() {
        return $this->cells;
    }

    /**
     * @param ArrayCollection $cells
     * @JSON\JsonProperty(name="cells", type="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell[]")
     */
    public function setCells($cells) {
        $this->cells = $cells;
    }

    public function __construct() {
        $this->cells = new ArrayCollection();
    }

} 