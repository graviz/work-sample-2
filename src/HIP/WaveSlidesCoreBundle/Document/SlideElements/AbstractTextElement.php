<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class AbstractTextElement
 * @package HIP\WaveSlidesCoreBundle\SlideElements
 *
 * @ODM\MappedSuperclass
 */
abstract class AbstractTextElement extends SlideElement {

    /**
     * @var string
     * @ODM\String()
     */
    protected $text;

    public function __construct() {
        parent::__construct();
    }

    //--------------------------------------------------------------------------------------

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

} 