<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\Bounds;

/** @ODM\Document(collection="elements") */
class TextElement extends AbstractTextElement {

    const Role = 'text';
    protected $type = self::Role;

    const DEFAULT_TEXT = "Text";
    const DEFAULT_HEIGHT = 16;
    const DEFAULT_WIDTH = 160;
    const DEFAULT_X = 320;
    const DEFAULT_Y = 192;

    public function __construct() {
        parent::__construct();
        $this->setBounds(Bounds::make(self::DEFAULT_WIDTH, self::DEFAULT_HEIGHT, self::DEFAULT_X, self::DEFAULT_Y));
        $this->text = self::DEFAULT_TEXT;
    }

    /**
     * @return string
     */
    public function getRole() {
        return self::Role;
    }
}