<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="elements") */
class TableElement extends SlideElement {

    const Role = 'table';
    protected $type = self::Role;

    /**
     * @var ArrayCollection
     * @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableRow")
     */
    protected $rows = [];

    public function __construct() {
        parent::__construct();
        $this->rows = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getRole() {
        return self::Role;
    }

    /**
     * @return ArrayCollection
     */
    public function getRows() {
        return $this->rows;
    }

    /**
     * @param ArrayCollection $rows
     */
    public function setRows($rows) {
        $this->rows = $rows;
    }

}