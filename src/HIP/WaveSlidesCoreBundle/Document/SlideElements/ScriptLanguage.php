<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use HIP\WaveSlidesCoreBundle\Document\AbstractDocument;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="script_languages") */
class ScriptLanguage extends AbstractDocument {

    /** @ODM\String */
    protected $name;

    /** @ODM\String */
    protected $css;

    /** @ODM\String */
    protected $value;
    const Value = 'value';

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getCss() {
        return $this->css;
    }

    /**
     * @param string $css
     */
    public function setCss($css) {
        $this->css = $css;
    }

    /**
     * @return string
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @param string $value
     */
    public function setValue($value) {
        $this->value = $value;
    }

} 