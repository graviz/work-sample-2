<?php
/**
 * @author Stefan Beier
 *
 * Currently not used.
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesCoreBundle\Document\Color;

/**
 * Class FormattedText
 * @package HIP\WaveSlidesCoreBundle\Document
 *
 * @ODM\EmbeddedDocument
 */
class FormattedText {

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="text", type="string")
     */
    public $text;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="size", type="int")
     */
    public $size;

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="align", type="string")
     */
    public $align;

    /**
     * @var Color
     * @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Color")
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public $color;

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="font", type="string")
     */
    public $font;

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="script", type="string")
     */
    public $script;

    /**
     * @var boolean
     * @ODM\Boolean()
     * @JSON\JsonProperty(name="bold", type="boolean")
     */
    public $bold;

    /**
     * @var boolean
     * @ODM\Boolean()
     * @JSON\JsonProperty(name="italic", type="boolean")
     */
    public $italic;

    /**
     * @var boolean
     * @ODM\Boolean()
     * @JSON\JsonProperty(name="strikeout", type="boolean")
     */
    public $strikeout;

    /**
     * @var boolean
     * @ODM\Boolean()
     * @JSON\JsonProperty(name="underline", type="boolean")
     */
    public $underline;

}