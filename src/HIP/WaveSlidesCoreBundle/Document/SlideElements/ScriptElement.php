<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Bounds;

/** @ODM\Document(collection="elements") */
class ScriptElement extends AbstractTextElement {

    const Role = 'script';
    protected $type = self::Role;

    const DEFAULT_TEXT = 'public class HelloWorld {<br/> &#09;public static void main(String... args) {<br/> &#09;&#09;System.out.println(\'Hello World!\');<br/> &#09;}<br/>}';

    /**
     * @var ScriptLanguage
     * @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage")
     */
    protected $language;

    public function __construct() {
        parent::__construct();
        $this->language = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:SlideElements\ScriptLanguage')->findOneBy([ScriptLanguage::Value => 'JAVA']);
        $this->setText(self::DEFAULT_TEXT);
        $this->setBounds(Bounds::make(373, 85, 100, 100));
    }

    /**
     * @return string
     */
    public function getRole() {
        return self::Role;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * @param \HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage $language
     */
    public function setLanguage($language) {
        $this->language = $language;
    }
}