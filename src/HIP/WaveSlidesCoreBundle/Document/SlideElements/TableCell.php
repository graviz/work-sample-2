<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\Color;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\BackgroundColorComponent;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\BorderComponent;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontColorComponent;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontSizeComponent;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesCoreBundle\Document\Dimension;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent;

/**
 * Class TableCell
 * @package HIP\WaveSlidesCoreBundle\Document\SlideElements
 *
 * @ODM\EmbeddedDocument
 */
class TableCell {

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="value", type="string")
     */
    protected $value = '';

    /**
     * @var StyleComponent[]
     * @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent", strategy="set")
     * @JSON\JsonProperty(name="styles", type="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[string]")
     */
    protected $styles = [];

    /**
     * @var Dimension
     * @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Dimension")
     * @JSON\JsonProperty(name="dimension", type="HIP\WaveSlidesCoreBundle\Document\Dimension")
     */
    protected $dimension;

    public function __construct() {
        $this->styles[FontColorComponent::Role] = FontColorComponent::make(Color::make(0.2, 0.2, 0.2));
        $this->styles[BackgroundColorComponent::Role] = new BackgroundColorComponent();
        $this->styles[FontSizeComponent::Role] = FontSizeComponent::make(12);
        $this->styles[BorderComponent::Role] = BorderComponent::make(1, BorderComponent::BORDER_STYLE_SOLID, Color::make(145 / 255, 145 / 255, 145 / 255));
        $this->dimension = Dimension::make(130, 25);
    }

    public static function make($value) {
        $cell = new TableCell();
        $cell->setValue($value);
        return $cell;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="value", type="string")
     */
    public function getValue() {
        return $this->value;
    }

    /**
     * @param string $value
     * @JSON\JsonProperty(name="value", type="string")
     */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[]
     * @JSON\JsonProperty(name="styles", type="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[string]")
     */
    public function getStyles() {
        return $this->styles;
    }

    /**
     * @param \HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[] $styles
     * @JSON\JsonProperty(name="styles", type="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[string]")
     */
    public function setStyles($styles) {
        $this->styles = $styles;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Dimension
     * @JSON\JsonProperty(name="dimension", type="HIP\WaveSlidesCoreBundle\Document\Dimension")
     */
    public function getDimension() {
        return $this->dimension;
    }

    /**
     * @param \HIP\WaveSlidesCoreBundle\Document\Dimension $dimension
     * @JSON\JsonProperty(name="dimension", type="HIP\WaveSlidesCoreBundle\Document\Dimension")
     */
    public function setDimension($dimension) {
        $this->dimension = $dimension;
    }

} 