<?php
/** @author Stefan Beier */

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\Bounds;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\BackgroundColorComponent;
use HIP\WaveSlidesCoreBundle\Document\View;
use MongoDBODMProxies\__CG__\HIP\WaveSlidesCoreBundle\Document\Color;

/**
 * @ODM\Document(collection="elements")
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({
 *          "image"="HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement",
 *          "text"="HIP\WaveSlidesCoreBundle\Document\SlideElements\TextElement",
 *          "script"="HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptElement",
 *          "table"="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableElement"
 * })
 */
abstract class SlideElement extends View {

    /**
     * @var Bounds
     * @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Bounds")
     */
    protected $bounds;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var int
     * @ODM\Int
     */
    protected $z;

    /**
     * @var float
     * @ODM\Float
     */
    protected $alpha = 1.0;

    public function __construct() {
        $this->styles[BackgroundColorComponent::Role] = BackgroundColorComponent::make(Color::make(1, 1, 1, 0));
        $this->bounds = Bounds::make(0, 0, 0, 0);
    }

    /**
     * @return int
     */
    public function getZ() {
        return $this->z;
    }

    /**
     * @param int $z
     */
    public function setZ($z) {
        $this->z = $z;
    }

    /**
     * @return Bounds
     */
    public function getBounds() {
        return $this->bounds;
    }

    /**
     * @param Bounds $bounds
     */
    public function setBounds($bounds) {
        $this->bounds = $bounds;
    }

    /**
     * @return string
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type) {
        $this->type = $type;
    }

    /**
     * @return float
     */
    public function getAlpha() {
        return $this->alpha;
    }

    /**
     * @param float $alpha
     */
    public function setAlpha($alpha) {
        $this->alpha = $alpha;
    }

}