<?php

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="elements") */
class ImageElement extends SlideElement {

    const Role = 'image';
    protected $type = self::Role;

    /**
     * @var Image
     * @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\SlideElements\Image", cascade="all")
     */
    protected $image;

    public function getOriginalHeight() {
        return $this->image->getOriginalHeight();
    }

    public function getOriginalWidth() {
        return $this->image->getOriginalWidth();
    }

    // ---------------------------------

    /** @return Image */
    public function getImage() {
        return $this->image;
    }

    /** param Image $image */
    public function setImage($image) {
        $this->image = $image;
    }

    /**
     * @return string
     */
    public function getRole() {
        return self::Role;
    }


}