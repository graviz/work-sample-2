<?php

namespace HIP\WaveSlidesCoreBundle\Document\SlideElements;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\GridFS\GridFSFile;

/** @ODM\Document(collection="resource_images") */
class Image extends GridFSFile {

    /**
     * @ODM\Int
     * @var int
     */
    protected $originalWidth = 0;

    /**
     * @ODM\Int
     * @var int
     */
    protected $originalHeight = 0;

    /**
     * @return int
     */
    public function getOriginalWidth() {
        return $this->originalWidth;
    }

    /**
     * @param int $originalWidth
     */
    public function setOriginalWidth($originalWidth) {
        $this->originalWidth = $originalWidth;
    }

    /**
     * @return int
     */
    public function getOriginalHeight() {
        return $this->originalHeight;
    }

    /**
     * @param int $originalHeight
     */
    public function setOriginalHeight($originalHeight) {
        $this->originalHeight = $originalHeight;
    }

}