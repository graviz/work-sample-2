<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\MappedSuperclass */
abstract class AbstractDocument {

    /** @ODM\Id */
    protected $id;
    const ID = 'id';

    /**
     * @param string $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

}
