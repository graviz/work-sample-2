<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="system") */
class System extends AbstractDocument {

    /** @var System */
    private static $instance;

    /**
     * @return System
     */
    public static function instance() {
        if (self::$instance == null)
            self::$instance = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:System')->findAll()[0];

        return self::$instance;
    }

    /** @ODM\Hash */
    protected $strings;

    /**
     * @return array
     */
    public function getStrings() {
        return $this->strings;
    }

    /**
     * @param array $strings
     */
    public function setStrings($strings) {
        $this->strings = $strings;
    }

} 