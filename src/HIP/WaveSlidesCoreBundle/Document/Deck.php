<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;

/**
 * @ODM\Document(collection="decks")
 * @ODM\HasLifecycleCallbacks
 */
class Deck extends AbstractDocument {

    const FORMAT_16_10 = '_16_10';
    const FORMAT_16_9 = '_16_9';
    const FORMAT_4_3 = '_4_3';

    /**
     * @var string
     * @ODM\String
     */
    protected $name;
    const Name = 'name';

    /**
     * @var ArrayCollection
     * @ODM\ReferenceMany(
     *      targetDocument="HIP\WaveSlidesCoreBundle\Document\Slide",
     *      cascade="all"
     * )
     */
    protected $slides;
    const Slides = 'slides';

    /**
     * @var WaveUser
     * @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\WaveUser")
     */
    protected $creator;
    const Creator = 'creator';

    /**
     * @var DateTime
     * @ODM\Date
     */
    protected $created;
    const Created = "created";

    /**
     * @var string
     * @ODM\String
     */
    protected $format;
    const Format = "format";

    /**
     * @var DateTime
     * @ODM\Date
     */
    protected $lastUpdated;
    const LastUpdated = 'lastUpdated';

    public function __construct() {
        $this->slides = new ArrayCollection();
        $this->format = self::FORMAT_4_3;
        $this->lastUpdated = new DateTime();
    }

    /** @ODM\PrePersist */
    public function prePersist() {
        $this->lastUpdated = new DateTime();
    }

    /**
     * @return string
     */
    public function getAlias() {
        $alias = str_replace(' ', '-', $this->name);
        $alias = strtolower($alias);
        return $alias;
    }

    //-----------------------------------------------------------------------

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return ArrayCollection
     */
    public function getSlides() {
        return $this->slides;
    }

    /**
     * @param ArrayCollection $slides
     */
    public function setSlides($slides) {
        $this->slides = $slides;
    }

    /**
     * @return WaveUser
     */
    public function getCreator() {
        return $this->creator;
    }

    /**
     * @param WaveUser $creator
     */
    public function setCreator($creator) {
        $this->creator = $creator;
    }

    /**
     * @return DateTime
     */
    public function getCreated() {
        return $this->created;
    }

    /**
     * @param DateTime $created
     */
    public function setCreated($created) {
        $this->created = $created;
    }

    /**
     * @return string
     */
    public function getFormat() {
        return $this->format;
    }

    /**
     * @param string $format
     */
    public function setFormat($format) {
        $this->format = $format;
    }

    /**
     * @return DateTime
     */
    public function getLastUpdated() {
        return $this->lastUpdated;
    }

    /**
     * @param DateTime $lastUpdated
     */
    public function setLastUpdated($lastUpdated) {
        $this->lastUpdated = $lastUpdated;
    }

    public function getSlideById($id) {
        foreach ($this->slides as $slide) /** @var $slide Slide */
            if ($slide->getId() == $id)
                return $slide;
    }

}