<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\StyleComponent;

use HIP\WaveSlidesCoreBundle\Document\Color;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class BackgroundColorComponent
 * @package HIP\WaveSlidesCoreBundle\Document\StyleComponent
 *
 * @ODM\EmbeddedDocument
 */
class BackgroundColorComponent extends StyleComponent {

    const Role = 'background-color';
    protected $type = self::Role;

    /**
     * @var Color
     * @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Color")
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    protected $color;

    public function __construct() {
        $this->color = Color::make();
    }

    public static function make(Color $c) {
        $component = new BackgroundColorComponent();
        $component->setColor($c);
        return $component;
    }

    public function toCSS() {
        return $this->color->toCSSRGBA();
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Color
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public function getColor() {
        return $this->color;
    }

    /**
     * @param \HIP\WaveSlidesCoreBundle\Document\Color $color
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public function setColor($color) {
        $this->color = $color;
    }
}