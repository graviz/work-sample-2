<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\StyleComponent;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class FontSizeComponent
 * @package HIP\WaveSlidesCoreBundle\Document\StyleComponent
 *
 * @ODM\EmbeddedDocument
 */
class FontSizeComponent extends StyleComponent {

    const Role = 'font-size';
    protected $type = self::Role;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="fontSize", type="int")
     */
    protected $fontSize = 10;

    public static function make($size) {
        $c = new FontSizeComponent();
        $c->setFontSize($size);
        return $c;
    }

    public function toCSS() {
        return $this->fontSize . 'px';
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="fontSize", type="int")
     */
    public function getFontSize() {
        return $this->fontSize;
    }

    /**
     * @param int $fontSize
     * @JSON\JsonProperty(name="fontSize", type="int")
     */
    public function setFontSize($fontSize) {
        $this->fontSize = $fontSize;
    }


}