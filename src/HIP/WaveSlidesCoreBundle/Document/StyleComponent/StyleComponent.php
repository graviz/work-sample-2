<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\StyleComponent;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({
 *          "background-color" = "HIP\WaveSlidesCoreBundle\Document\StyleComponent\BackgroundColorComponent",
 *          "border" = "HIP\WaveSlidesCoreBundle\Document\StyleComponent\BorderComponent",
 *          "font-size" = "HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontSizeComponent",
 *          "color" = "HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontColorComponent"
 * })
 *
 * @JSON\JsonTypeInfo(use=JSON\JsonTypeInfo::ID_NAME, include=JSON\JsonTypeInfo::AS_PROPERTY, property="type")
 * @JSON\JsonSubTypes({
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesCoreBundle\Document\StyleComponent\BackgroundColorComponent", name="background-color"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesCoreBundle\Document\StyleComponent\BorderComponent", name="border"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontSizeComponent", name="font-size"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesCoreBundle\Document\StyleComponent\FontColorComponent", name="color")
 * })
 */
abstract class StyleComponent {

    /**
     * @var string
     * @JSON\JsonProperty(name="type", type="string")
     */
    protected $type;

    /**
     * @return string
     */
    public abstract function toCSS();

    /**
     * @return string
     * @JSON\JsonProperty(name="type", type="string")
     */
    public function getType() {
        return $this->type;
    }

    /**
     * @param string $type
     * @JSON\JsonProperty(name="type", type="string")
     */
    public function setType($type) {
        $this->type = $type;
    }


} 