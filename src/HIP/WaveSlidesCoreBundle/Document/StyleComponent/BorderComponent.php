<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\StyleComponent;

use HIP\WaveSlidesCoreBundle\Document\Color;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class BorderComponent
 * @package HIP\WaveSlidesCoreBundle\Document\StyleComponent
 *
 * @ODM\EmbeddedDocument
 */
class BorderComponent extends StyleComponent {

    const Role = 'border';
    protected $type = self::Role;

    const BORDER_STYLE_SOLID = 'solid';

    /**
     * @var Color
     * @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Color")
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    protected $color;

    /**
     * @var string
     * @ODM\String
     * @JSON\JsonProperty(name="style", type="string")
     */
    protected $style = self::BORDER_STYLE_SOLID;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="width", type="int")
     */
    protected $width = 1;

    public function __construct() {
        $this->color = Color::make();
    }

    public static function make($width, $style, Color $color) {
        $c = new BorderComponent();
        $c->setColor($color);
        $c->setWidth($width);
        $c->setStyle($style);
        return $c;
    }

    public function toCSS() {
        return $this->width . 'px ' . $this->style . ' ' . $this->color->toCSSRGBA();
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Color
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public function getColor() {
        return $this->color;
    }

    /**
     * @param \HIP\WaveSlidesCoreBundle\Document\Color $color
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public function setColor($color) {
        $this->color = $color;
    }

    /**
     * @return string
     * @JSON\JsonProperty(name="style", type="string")
     */
    public function getStyle() {
        return $this->style;
    }

    /**
     * @param string $style
     * @JSON\JsonProperty(name="style", type="string")
     */
    public function setStyle($style) {
        $this->style = $style;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="width", type="int")
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * @param int $width
     * @JSON\JsonProperty(name="width", type="int")
     */
    public function setWidth($width) {
        $this->width = $width;
    }
}