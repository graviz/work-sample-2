<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Account;

use DateInterval;
use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\EmbeddedDocument */
class SessionData {

    /** @ODM\String */
    private $id;
    const SessionId = 'sessionId';

    /** @ODM\Date */
    private $expiration;
    const Expiration = 'expiration';

    const VALIDITY = 'P1D';

    /**
     * Create a SessionData object with a validity of 1 day.
     * @return SessionData
     */
    public static function generate() {
        $session = new SessionData();
        $session->id = uniqid('', true);
        $expiration = new DateTime();
        $expiration->add(new DateInterval(self::VALIDITY));
        $session->expiration = $expiration;
        return $session;
    }

    /**
     * Increase the validity of the session for 1 day.
     */
    public function refresh() {
        $expiration = new DateTime();
        $expiration->add(new DateInterval(self::VALIDITY));
        $this->expiration = $expiration;
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param string $sessionId
     */
    public function setId($sessionId) {
        $this->id = $sessionId;
    }

    /**
     * @return DateTime
     */
    public function getExpiration() {
        return $this->expiration;
    }

    /**
     * @param DateTime $expiration
     */
    public function setExpiration($expiration) {
        $this->expiration = $expiration;
    }

} 