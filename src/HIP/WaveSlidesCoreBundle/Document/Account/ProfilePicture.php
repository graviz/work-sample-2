<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Account;

use HIP\WaveSlidesCoreBundle\Document\GridFS\GridFSFile;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="profile_pictures") */
class ProfilePicture extends GridFSFile {

}