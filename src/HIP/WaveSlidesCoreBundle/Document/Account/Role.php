<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Account;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\AbstractDocument;
use Symfony\Component\Security\Core\Role\RoleInterface;

/** @ODM\Document(collection="roles") */
class Role extends AbstractDocument implements RoleInterface {

    const ADMIN = 'ROLE_ADMIN';
    const USER = 'ROLE_USER';

    /** @ODM\String */
    private $name;
    const Name = 'name';

    /** @ODM\String */
    private $role;
    const Role = 'role';

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * Returns the role.
     *
     * This method returns a string representation whenever possible.
     *
     * When the role cannot be represented with sufficient precision by a
     * string, it should return null.
     *
     * @return string|null A string representation of the role, or null
     */
    public function getRole() {
        return $this->role;
    }

    /**
     * @param mixed $role
     */
    public function setRole($role) {
        $this->role = $role;
    }

}