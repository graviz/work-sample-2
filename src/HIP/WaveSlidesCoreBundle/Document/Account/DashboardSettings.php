<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Account;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\EmbeddedDocument */
class DashboardSettings {

    /** @ODM\String */
    protected $sortOption;
    const SORT_OPTION = 'sortOption';

    /** @ODM\String */
    protected $viewOption;
    const VIEW_OPTION = 'viewOption';

    public function __construct() {
        $this->sortOption = SortOption::SORT_DATE;
        $this->viewOption = ViewOption::VIEW_ALL;
    }

    /**
     * @return string
     */
    public function getViewOption() {
        return $this->viewOption;
    }

    /**
     * @param string $viewOption
     */
    public function setViewOption($viewOption) {
        $this->viewOption = $viewOption;
    }

    /**
     * @return string
     */
    public function getSortOption() {
        return $this->sortOption;
    }

    /**
     * @param string $sortOption
     */
    public function setSortOption($sortOption) {
        $this->sortOption = $sortOption;
    }

}

interface SortOption {
    const SORT_NAME = 'sort_name';
    const SORT_DATE = 'sort_date';
}

interface ViewOption {
    const VIEW_ALL = 'view_all';
    const VIEW_MY = 'view_my';
    const VIEW_SHARED = 'view_shared';
}