<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document\Account;

use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use FOS\UserBundle\Model\User;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(collection="users")
 */
class WaveUser extends User {

    /** @ODM\Id(strategy="auto") */
    protected $id;
    const ID = 'id';

    const EMAIL = 'email';
    const USERNAME = 'username';

    /** @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\Role") */
    protected $role;
    const ROLE = 'role';

    /** @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\SessionData") */
    protected $session;
    const SESSION = 'session';

    /** @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Deck") */
    protected $decks;
    const DECKS = 'decks';

    /** @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Deck") */
    protected $sharedDecks;
    const SHARED_DECKS = 'sharedDecks';

    /** @ODM\EmbedOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\DashboardSettings") */
    protected $dashboardSettings;
    const DASHBOARD_SETTINGS = 'dashboardSettings';

    /** @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Team", cascade="all") */
    protected $teams;
    const TEAMS = 'teams';

    public function __construct() {
        parent::__construct();
        $this->decks = new ArrayCollection();
        $this->sharedDecks = new ArrayCollection();
        $this->dashboardSettings = new DashboardSettings();
        $this->teams = new ArrayCollection();
    }

    /**
     * Returns the deck from id from its user own decks or shared decks
     * @param $id
     * @return Deck
     */
    public function getDeck($id) {
        /** @var Deck $deck */
        foreach ($this->getDecks() as $deck)
            if ($deck->getId() == $id)
                return $deck;

        foreach ($this->getSharedDecks() as $deck)
            if ($deck->getId() == $id)
                return $deck;

        return null;
    }


    // -------------------------------------------------------------


    public function getRoles() {
        return array($this->getRole()->getRole());
    }

    /** @return Role */
    public function getRole() {
        return $this->role;
    }

    /** @param Role $role */
    public function setRole($role) {
        $this->role = $role;
    }

    /** @return SessionData */
    public function getSession() {
        return $this->session;
    }

    /** @param SessionData $session */
    public function setSession($session) {
        $this->session = $session;
    }

    /** @return ArrayCollection */
    public function getDecks() {
        return $this->decks;
    }

    /** @param ArrayCollection $decks */
    public function setDecks($decks) {
        $this->decks = $decks;
    }

    /** @return ArrayCollection */
    public function getSharedDecks() {
        return $this->sharedDecks;
    }

    /** @param ArrayCollection $sharedDecks */
    public function setSharedDecks($sharedDecks) {
        $this->sharedDecks = $sharedDecks;
    }

    /**
     * @return DashboardSettings
     */
    public function getDashboardSettings() {
        return $this->dashboardSettings;
    }

    /**
     * @param DashboardSettings $dashboardSettings
     */
    public function setDashboardSettings($dashboardSettings) {
        $this->dashboardSettings = $dashboardSettings;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeams() {
        return $this->teams;
    }

    /**
     * @param ArrayCollection $teams
     */
    public function setTeams($teams) {
        $this->teams = $teams;
    }

}