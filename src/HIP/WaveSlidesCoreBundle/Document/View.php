<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * Class View
 * @package HIP\WaveSlidesCoreBundle\Document
 *
 * @ODM\MappedSuperclass
 */
abstract class View extends AbstractDocument {

    /**
     * @var StyleComponent[]
     * @ODM\EmbedMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent", strategy="set")
     */
    protected $styles;

    /**
     * @return StyleComponent[]
     */
    public function getStyles() {
        return $this->styles;
    }

    /**
     * @param StyleComponent[] $styles
     */
    public function setStyles($styles) {
        $this->styles = $styles;
    }

    /**
     * @return string
     */
    public abstract function getRole();

} 