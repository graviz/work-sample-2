<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/** @ODM\Document(collection="teams") */
class Team extends AbstractDocument {

    /** @ODM\String */
    protected $name;

    /** @ODM\String */
    protected $description;

    /** @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\WaveUser", cascade="all") */
    protected $members;

    /** @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\Deck", cascade="all") */
    protected $decks;

    public function __construct() {
        $this->members = new ArrayCollection();
        $this->decks = new ArrayCollection();
        $this->teams = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name) {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description) {
        $this->description = $description;
    }

    /**
     * @return ArrayCollection
     */
    public function getMembers() {
        return $this->members;
    }

    /**
     * @param ArrayCollection $members
     */
    public function setMembers($members) {
        $this->members = $members;
    }

    /**
     * @return ArrayCollection
     */
    public function getDecks() {
        return $this->decks;
    }

    /**
     * @param ArrayCollection $decks
     */
    public function setDecks($decks) {
        $this->decks = $decks;
    }

    /**
     * @return ArrayCollection
     */
    public function getTeams() {
        return $this->teams;
    }

    /**
     * @param ArrayCollection $teams
     */
    public function setTeams($teams) {
        $this->teams = $teams;
    }

} 