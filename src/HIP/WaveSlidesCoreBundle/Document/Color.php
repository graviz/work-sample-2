<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * @ODM\EmbeddedDocument
 */
class Color {

    /**
     * @var float
     * @ODM\Float
     * @JSON\JsonProperty(name="r", type="float")
     */
    public $r;

    /**
     * @var float
     * @ODM\Float
     * @JSON\JsonProperty(name="g", type="float")
     */
    public $g;

    /**
     * @var float
     * @ODM\Float
     * @JSON\JsonProperty(name="b", type="float")
     */
    public $b;

    /**
     * @var float
     * @ODM\Float
     * @JSON\JsonProperty(name="a", type="float")
     */
    public $a = 1;

    /**
     * Returns a css stylesheet representation of this color in the RGBA format.
     * @return string
     */
    public function toCSSRGBA() {
        return 'rgba(' . round($this->r * 255) . ',' . round($this->g * 255) . ',' . round($this->b * 255) . ',' . $this->a . ')';
    }

    /**
     * Returns a css stylesheet representation of this color in the RGBA format.
     * @return string
     */
    public function toCSSRGB() {
        return 'rgb(' . round($this->r * 255) . ',' . round($this->g * 255) . ',' . round($this->b * 255) . ')';
    }

    public static function make($r = 1, $g = 1, $b = 1, $a = 1) {
        $c = new Color();
        $c->r = $r;
        $c->g = $g;
        $c->b = $b;
        $c->a = $a;
        return $c;
    }
} 