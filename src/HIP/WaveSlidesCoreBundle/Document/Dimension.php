<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * @ODM\EmbeddedDocument
 */
class Dimension {

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="width", type="int")
     */
    public $width;

    /**
     * @var int
     * @ODM\Int
     * @JSON\JsonProperty(name="height", type="int")
     */
    public $height;

    /**
     * @param int $width
     * @param int $height
     * @return Dimension
     */
    public static function make($width, $height) {
        $dim = new Dimension();
        $dim->setHeight($height);
        $dim->setWidth($width);
        return $dim;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="width", type="int")
     */
    public function getWidth() {
        return $this->width;
    }

    /**
     * @param int $width
     * @JSON\JsonProperty(name="width", type="int")
     */
    public function setWidth($width) {
        $this->width = $width;
    }

    /**
     * @return int
     * @JSON\JsonProperty(name="height", type="int")
     */
    public function getHeight() {
        return $this->height;
    }

    /**
     * @param int $height
     * @JSON\JsonProperty(name="height", type="int")
     */
    public function setHeight($height) {
        $this->height = $height;
    }

}