<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Document\StyleComponent\BackgroundColorComponent;

/** @ODM\Document(collection="slides") */
class Slide extends View {

    const Role = 'slide';

    /**
     * @var ArrayCollection
     * @ODM\ReferenceMany(targetDocument="HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement", cascade="all")
     */
    protected $elements;
    const Elements = 'elements';

    /**
     * @var int
     * @ODM\Int
     */
    protected $index;

    public function __construct() {
        $this->elements = new ArrayCollection();
        $this->styles[BackgroundColorComponent::Role] = new BackgroundColorComponent();
    }

    /**
     * @return ArrayCollection
     */
    public function getElements() {
        return $this->elements;
    }

    /**
     * @param ArrayCollection $elements
     */
    public function setElements($elements) {
        $this->elements = $elements;
    }

    /**
     * @return int
     */
    public function getIndex() {
        return $this->index;
    }

    /**
     * @param int $index
     */
    public function setIndex($index) {
        $this->index = $index;
    }

    /**
     * @return string
     */
    public function getRole() {
        return self::Role;
    }

    /**
     * @param string $id
     * @return SlideElement
     */
    public function getElementById($id) {
        foreach ($this->elements as $slideElement) /** @var SlideElement $slideElement */
            if ($slideElement->getId() == $id)
                return $slideElement;

        return null;
    }
}