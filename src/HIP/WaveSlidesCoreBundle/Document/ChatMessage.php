<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Document;

use DateTime;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;

/**
 * Class ChatMessage
 * @package HIP\WaveSlidesCoreBundle\Document
 * @ODM\Document(collection="chats")
 */
class ChatMessage extends AbstractDocument {

    /**
     * @var WaveUser
     * @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Account\WaveUser")
     */
    protected $sender;

    /**
     * @var Deck
     * @ODM\ReferenceOne(targetDocument="HIP\WaveSlidesCoreBundle\Document\Deck")
     */
    protected $deck;

    /**
     * @var Datetime
     * @ODM\Date
     */
    protected $date;

    /**
     * @var string
     * @ODM\String
     */
    protected $text;

    public function __construct() {
        $this->date = new DateTime();
    }

    /**
     * @return WaveUser
     */
    public function getSender() {
        return $this->sender;
    }

    /**
     * @param WaveUser $sender
     */
    public function setSender($sender) {
        $this->sender = $sender;
    }

    /**
     * @return Deck
     */
    public function getDeck() {
        return $this->deck;
    }

    /**
     * @param Deck $deck
     */
    public function setDeck($deck) {
        $this->deck = $deck;
    }

    /**
     * @return DateTime
     */
    public function getDate() {
        return $this->date;
    }

    /**
     * @param DateTime $time
     */
    public function setDate($time) {
        $this->date = $time;
    }

    /**
     * @return string
     */
    public function getText() {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText($text) {
        $this->text = $text;
    }

}