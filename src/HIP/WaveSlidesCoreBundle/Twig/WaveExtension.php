<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Twig;

use Twig_Extension;

class WaveExtension extends Twig_Extension {

    public function getFilters() {
        return array(
            new \Twig_SimpleFilter('num2char', array($this, 'num2charFilter')),
        );
    }

    public function num2charFilter($n) {
        for ($r = ""; $n >= 0; $n = intval($n / 26) - 1)
            $r = chr($n % 26 + 0x41) . $r;
        return $r;
    }

    public function getName() {
        return 'wave_extension';
    }
}