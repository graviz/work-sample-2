<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;

/**
 * Class ServletBridgeRequest
 * @package HIP\WaveSlidesCoreBundle\Common
 * @deprecated
 */
class ServletBridgeRequest {

    const Deck = 'deck';
    const Type = 'type';
    const User = 'user';
    const Session = 'session';

    /** @var string */
    protected $deckId;

    /** @var  string */
    protected $type;

    /** @var WaveUser */
    protected $user;

    /** @var string */
    protected $url;

    public function __construct($url, $user, $type, $deckId = null) {
        $this->url = $url;
        $this->user = $user;
        $this->type = $type;
        $this->deckId = $deckId;
    }

    /**
     * @return CurlResponse
     */
    public function send() {
        return Curl::post($this->url, array(
            'json' => $this->jsonData()
        ));
    }

    private function jsonData() {
        $jsonData = array(
            self::Type => $this->type,
            self::User => $this->user->getId(),
            self::Session => $this->user->getSession()->getId()
        );

        if ($this->deckId != null)
            $jsonData[self::Deck] = $this->deckId;

        return $jsonData;
    }

    /**
     * @param string $deckId
     */
    public function setDeckId($deckId) {
        $this->deckId = $deckId;
    }

}

interface ServletRequestType {
    const DeckRequest = 'DeckRequestData';
}