<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class FileResponse {

    public static function create($filepath, $filename, $contentType) {
        $response = new Response(file_get_contents($filepath));
        $d = $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
        $response->headers->set('Content-Disposition', $d);
        $response->headers->set('Content-Type', $contentType);
        return $response;
    }

} 