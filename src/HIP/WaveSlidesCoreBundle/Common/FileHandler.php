<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use HIP\WaveSlidesCoreBundle\Document\GridFS\GridFSFile;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\Image;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileHandler {

    public static $supportedImages = [];

    /**
     * @param UploadedFile $file
     * @return Image
     */
    public static function processImage(UploadedFile $file) {
        $image = new Image();

        //TODO:: Imagick seems to lack performance? Do we want to resize the image?
        //$imagick = new Imagick($file->getPathname());
        //$image->setOriginalHeight($imagick->getimageheight());
        //$image->setOriginalWidth($imagick->getimagewidth());

        $size = getimagesize($file->getPathname());
        $image->setOriginalHeight($size[1]);
        $image->setOriginalWidth($size[0]);

        self::createGrid($file, $image);

        return $image;
    }

    /**
     * @param UploadedFile $file
     * @return bool
     */
    public static function isImage(UploadedFile $file) {
        switch ($file->getClientMimeType()) {
            case "image/bmp":
            case "image/gif":
            case "image/jpeg":
            case "image/png":
            case "image/tiff":
                return true;
        }
        return false;
    }

    /**
     * @param UploadedFile $file
     * @param GridFSFile $grid
     */
    private static function createGrid(UploadedFile $file, GridFSFile &$grid) {
        $grid->setFile($file->getPathname());
        $grid->setFilename($file->getClientOriginalName());
        $grid->setContentType($file->getClientMimeType());
    }

} 