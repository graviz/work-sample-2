<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;


use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Document\Slide;

class Sorter {

    /**
     * @param ArrayCollection $slides
     * @return ArrayCollection
     */
    public static function SortSlidesAsc($slides) {
        $arr = $slides->toArray();

        uasort($arr, function (Slide $a, Slide $b) {
            return ($a->getIndex() < $b->getIndex()) ? -1 : 1;
        });

        /**
         * array_values resets the keys
         */
        return new ArrayCollection(array_values($arr));
    }

} 