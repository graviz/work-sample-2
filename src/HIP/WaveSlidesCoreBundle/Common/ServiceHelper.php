<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\TwigBundle\Debug\TimedTwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Config\FileLocator;

class ServiceHelper {

    /**
     * @return DocumentManager
     */
    public static function dm() {
        return self::container()->get('doctrine_mongodb')->getManager();
    }

    /**
     * @return TimedTwigEngine
     */
    public static function templating() {
        return self::container()->get('templating');
    }

    /**
     * @param $key
     * @return string
     */
    public static function parameter($key) {
        return self::container()->getParameter($key);
    }

    /**
     * @return FileLocator
     */
    public static function locator() {
        return self::container()->get('file_locator');
    }

    /**
     * @return \Symfony\Component\HttpKernel\KernelInterface
     */
    public static function kernel() {
        return self::container()->get('kernel');
    }

    /**
     * @return ContainerInterface
     */
    private static function container() {
        global $kernel;

        if ('AppCache' == get_class($kernel))
            $kernel = $kernel->getKernel();

        return $kernel->getContainer();
    }


}