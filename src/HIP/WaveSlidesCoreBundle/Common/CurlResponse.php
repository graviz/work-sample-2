<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;


use Symfony\Component\HttpFoundation\Response;

class CurlResponse {

    protected $result;
    protected $statusCode;
    protected $contentType;

    public function __construct(
        $result,
        $statusCode,
        $contentType
    ) {
        $this->result = $result;
        $this->statusCode = $statusCode;
        $this->contentType = $contentType;
    }

    public function toResponse() {
        $response = new Response($this->result, $this->statusCode, array(
            'Content-Type' => $this->contentType
        ));

        return $response;
    }

    public function result() {
        return $this->result;
    }

    /**
     * @return string
     */
    public function statusCode() {
        return $this->statusCode;
    }

    /**
     * @return string
     */
    public function contentType() {
        return $this->contentType;
    }
}