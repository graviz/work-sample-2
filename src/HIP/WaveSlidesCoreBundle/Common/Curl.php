<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

class Curl {

    /** Field name for the response data */
    const Response = 'response';

    /** Field name for the response status code */
    const StatusCode = 'code';

    /** Field name fot the response Content-Type */
    const ContentType = 'contentType';

    protected $ch;

    protected $body = [];

    public function __construct($url) {
        $this->ch = curl_init($url);
    }

    public function setCookie($cookie) {
        curl_setopt($this->ch, CURLOPT_COOKIE, $cookie);
    }

    public function setBody($body) {
        $this->body = $body;
    }

    public function get() {
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        return $this->execute();
    }

    public function postRequest($multiPart = false) {

        if (isset($this->body['json']))
            $this->body['json'] = json_encode($this->body['json']);

        if (!$multiPart)
            $this->body = http_build_query($this->body);

        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($this->ch, CURLOPT_POST, true);
        curl_setopt($this->ch, CURLOPT_POSTFIELDS, $this->body);

        return $this->execute();
    }

    private function execute() {
        $result = curl_exec($this->ch);
        $statusCode = curl_getinfo($this->ch, CURLINFO_HTTP_CODE);
        $contentType = curl_getinfo($this->ch, CURLINFO_CONTENT_TYPE);

        curl_close($this->ch);

        return new CurlResponse($result, $statusCode, $contentType);
    }

    /**
     * @param string $url
     * @param array $body
     *          The data to be written in the body of the post request.
     *          If passing data associated with the 'json' keyword, it will be json encoded.
     * @param bool $multiPart
     *          If true, the body data will be encoded in the multipart/form-data format.
     *          If false, the body data will be encoded in the application/x-www-form-urlencoded format.
     * @return CurlResponse
     */
    public static function post($url, array $body, $multiPart = false) {
        $curl = new Curl($url);
        $curl->setBody($body);
        return $curl->postRequest($multiPart);
    }

}