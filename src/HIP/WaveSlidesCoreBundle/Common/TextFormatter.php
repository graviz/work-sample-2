<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use HIP\WaveSlidesCoreBundle\Document\SlideElements\FormattedText;
use HIP\WaveSlidesCoreBundle\Html\CSS;
use HIP\WaveSlidesCoreBundle\Html\Element;

class TextFormatter {

    const UNDERLINE_CLASS = 'underline';
    const STRIKEOUT_CLASS = 'strikeout';
    const SUB_CLASS = 'sub';
    const SUPER_CLASS = 'super';

    /**
     * @param FormattedText[] $formattedTexts
     * @return string
     */
    public static function toHTML($formattedTexts) {
        $html = '';
        foreach ($formattedTexts as $text) {
            $span = Element::span();

            $string = nl2br($text->text);
            $span->setString($string);

            if ($text->color != null)
                $span->css(CSS::COLOR, $text->color->toCSSRGBA());

            if ($text->font != null)
                $span->css(CSS::FONT_FAMILY, $text->font);

            if ($text->align != null)
                $span->css(CSS::TEXT_ALIGN, $text->align);

            if ($text->size != null)
                $span->css(CSS::FONT_SIZE, $text->size . 'px');

            if ($text->script == self::SUB_CLASS)
                $span->addClass(self::SUB_CLASS);
            else if ($text->script == self::SUPER_CLASS)
                $span->addClass(self::SUPER_CLASS);

            if ($text->bold)
                $span->addClass(CSS::BOLD);

            if ($text->italic)
                $span->addClass(CSS::ITALIC);
            
            if ($text->strikeout)
                $span->addClass(self::UNDERLINE_CLASS);

            if ($text->underline)
                $span->addClass(self::STRIKEOUT_CLASS);

            $html .= $span->render();
        }
        return $html;
    }

} 