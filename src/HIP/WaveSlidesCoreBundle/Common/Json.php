<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use Weasel\JsonMarshaller\JsonMapper;
use InvalidArgumentException;
use Weasel\WeaselDoctrineAnnotationDrivenFactory;

class Json {

    /**
     * @var JsonMapper
     */
    private static $mapper;

    /**
     * @param string $string
     * @param string $type FQ Class name
     * @return mixed
     */
    public static function decode($string, $type) {
        if (self::$mapper == null)
            self::init();

        try {
            $message = self::$mapper->readString($string, $type);
            return $message;
        } catch (InvalidArgumentException $e) {
        }
        return null;
    }

    /**
     * @param $object
     * @return string
     */
    public static function encode($object) {
        if (self::$mapper == null)
            self::init();

        return self::$mapper->writeString($object);
    }

    private static function init() {
        $factory = new WeaselDoctrineAnnotationDrivenFactory();
        self::$mapper = $factory->getJsonMapperInstance();
    }

} 