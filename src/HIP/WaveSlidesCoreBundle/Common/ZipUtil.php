<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Common;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use SplFileInfo;
use ZipArchive;

class ZipUtil {

    public static function zip($sourcePathname, $destinationPathname) {

        if (!is_dir($sourcePathname))
            return false;

        $zip = new ZipArchive();

        $zip->open($destinationPathname, ZIPARCHIVE::CREATE);

        FileUtil::traverseDirectoryRecursive(
            $sourcePathname,
            function (SplFileInfo $item, RecursiveIteratorIterator $iterator) use ($sourcePathname, $zip) {
                /** @var RecursiveDirectoryIterator $iterator */
                if ($item->isDir())
                    $zip->addEmptyDir($iterator->getSubPathName());
                else
                    $zip->addFromString($iterator->getSubPathName(), file_get_contents($item));
            }
        );

        return $zip->close();
    }

} 