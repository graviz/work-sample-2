<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesCoreBundle\Html;

/**
 * Class CSS
 * @package HIP\WaveSlidesCoreBundle\Html\Dom
 */
class CSS {
    const FONT_SIZE = 'font-size';
    const COLOR = 'color';
    const FONT_FAMILY = 'font-family';
    const TEXT_ALIGN = 'text-align';
    const FONT_WEIGHT = 'font-weight';
    const FONT_STYLE = 'font-style';
    const TEXT_DECORATION = 'text-decoration';

    const BOLD = 'bold';
    const ITALIC = 'italic';
    const UNDERLINE = 'underline';
    const LINE_THROUGH = 'line-through';
}