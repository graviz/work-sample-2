<?php
/** @author Stefan Beier */

namespace HIP\WaveSlidesCoreBundle\Html;

/**
 * Class DOM
 * @package HIP\WaveSlidesCoreBundle\Html\Dom
 */
class DOM {

    const DIV = 'div';
    const SPAN = 'span';
    const LABEL = 'label';
    const TABLE = 'table';
    const TD = 'td';
    const TR = 'tr';
    const TBODY = 'tbody';
    const THEAD = 'thead';
    const TH = 'th';
    const OPTION = 'option';
    const P = 'p';
    const BUTTON = 'button';
    const A = 'a';
    const FORM = 'form';
    const INPUT = 'input';
    const SELECT = 'select';
    const TEXTAREA = 'textarea';
    const IMG = 'img';

    public $tag;

    private function __construct($tag) {
        $this->tag = $tag;
    }

    public static function dom($tag) {
        return new DOM($tag);
    }
}