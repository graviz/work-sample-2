<?php
/** @author Stefan Beier */

namespace HIP\WaveSlidesCoreBundle\Html;

/**
 * Class Element
 * @package HIP\WaveSlidesCoreBundle\Html\Dom
 */
class Element extends AbstractElement {

    public function __construct(DOM $dom = null) {
        if ($dom == null)
            $dom = DOM::dom(DOM::DIV);
        parent::__construct($dom);
    }

    /**
     * @return Element
     */
    public static function span() {
        return new Element(DOM::dom(DOM::SPAN));
    }

}