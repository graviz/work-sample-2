<?php
/** @author Stefan Beier */

namespace HIP\WaveSlidesCoreBundle\Html;

/**
 * Class AbstractElement
 * @package HIP\WaveSlidesCoreBundle\Html\Dom
 */
abstract class AbstractElement {

    /** @var array */
    protected $children = array();

    /** @var array */
    protected $attributes = array();

    /** @var string */
    protected $tagName = '';

    /** @var string */
    protected $stringContent = '';

    const NAME = 'name';
    const DISABLED = 'disabled';
    const A_CLASS = 'class';
    const ID = 'id';
    const STYLE = 'style';

    /** @param DOM $dom */
    public function __construct(DOM $dom) {
        $this->tagName = $dom->tag;
    }

    /** @return string */
    public function render() {
        $html = '<' . $this->tagName . ' ' . $this->getAttributeString() . '>';

        /** @var AbstractElement $content */
        foreach ($this->children as $content)
            $html .= $content->render();

        $html .= $this->stringContent;

        $html .= '</' . $this->tagName . '>';
        return $html;
    }

    protected function getAttributeString() {
        $attributeString = '';

        if (isset($this->attributes[self::STYLE]))
            $styles = $this->attributes[self::STYLE];
        else
            $styles = [];

        if (count($styles) > 0)
            $attributeString .= self::STYLE . '="';

        if (count($styles) > 0)
            foreach ($styles as $key => $value)
                $attributeString .= $key . ': ' . $value . '; ';

        $attributeString = rtrim($attributeString, ' ');

        if (count($styles) > 0)
            $attributeString .= '" ';

        unset($this->attributes[self::STYLE]);

        foreach ($this->attributes as $key => $attributes) {
            $attributeString .= $key . '="';
            $attributesSingle = '';
            foreach ($attributes as $attribute)
                $attributesSingle .= $attribute . ' ';
            $attributesSingle = rtrim($attributesSingle);
            $attributeString .= $attributesSingle . '" ';
        }

        return $attributeString;
    }

    /** @return int */
    public function getChildrenCount() {
        return count($this->children);
    }

    /**
     * @param $name
     * @param $value
     * @return $this AbstractElement
     */
    public function addAttribute($name, $value) {
        $this->attributes[$name][] = $value;
        return $this;
    }

    /**
     * @param $style
     * @param $value
     * @return $this AbstractElement
     */
    public function css($style, $value) {
        $this->attributes[self::STYLE][$style] = $value;
        return $this;
    }

    /**
     * @param $class
     * @return $this AbstractElement
     */
    public function addClass($class) {
        $this->attributes[self::A_CLASS][] = $class;
        return $this;
    }

    /** @param string $name */
    public function setName($name) {
        $this->addAttribute('name', $name);
    }

    /** @return string */
    public function getName() {
        return $this->attributes['name'][0];
    }

    /** @param AbstractElement $content */
    public function addChild(AbstractElement $content) {
        $this->children[] = $content;
    }

    /** @param $string */
    public function appendString($string) {
        $this->stringContent .= $string;
    }

    /** @param $string */
    public function setString($string) {
        $this->stringContent = $string;
    }

    /** @return string */
    public function getStringContent() {
        return $this->stringContent;
    }

    /** @param string $position relative|static|absolute|fixed */
    public function setPosition($position) {
        $this->addStyle('position', $position);
    }

    /** @return string */
    public function getPosition() {
        return $this->getStyle('position');
    }

    /** @param string $id */
    public function setId($id) {
        $this->addAttribute(self::ID, $id);
    }

    /** @return string */
    public function getId() {
        return $this->attributes[self::ID];
    }

    /**
     * @param $disabled
     * @return $this AbstractElement
     */
    public function setDisabled($disabled) {
        if ($disabled)
            $this->addAttribute(self::DISABLED, self::DISABLED);
        else
            unset($this->attributes[self::DISABLED]);
        return $this;
    }

}