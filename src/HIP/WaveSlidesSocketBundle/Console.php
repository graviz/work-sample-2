<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle;


use Symfony\Component\Console\Output\OutputInterface;

class Console {

    /**
     * @var OutputInterface
     */
    private static $output;

    public static function init(OutputInterface $output) {
        self::$output = $output;
    }

    public static function info($title, $string) {
        $title = self::convertTitle($title);
        if (OutputInterface::VERBOSITY_DEBUG <= self::$output->getVerbosity())
            self::$output->writeln('<info>' . $title . '</info>' . $string);
    }

    public static function error($title, $string) {
        $title = self::convertTitle($title);
        if (OutputInterface::VERBOSITY_DEBUG <= self::$output->getVerbosity())
            self::$output->writeln('<error>' . $title . '</error>' . $string);
    }

    private static function convertTitle($title) {
        return $title = '[' . date('Y-m-d H:i:s] ') . $title . ': ';
    }

}