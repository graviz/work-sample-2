<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class ChangeActiveSlideMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Incomming
 *
 * Received, when the user changes the slide.
 */
class ChangeActiveSlideMessage extends IncommingMessage {

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function execute(Connection $conn) {
        $conn->setActiveSlide($this->slideId);
    }
}