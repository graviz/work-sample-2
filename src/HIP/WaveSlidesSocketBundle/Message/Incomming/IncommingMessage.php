<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesSocketBundle\Message\Executable;

/**
 * Class IncommingMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Incomming
 */
abstract class IncommingMessage implements Executable {

}