<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OnSlideElementRemovedMessage;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;

class SlideElementRemovedMessage extends IncommingMessage {

    /**
     * @var string
     * @JSON\JsonProperty(name="elementId", type="string")
     */
    public $elementId;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function execute(Connection $conn) {
        $deck = $conn->getDeck();
        $slide = $deck->getSlideById($this->slideId);

        if ($slide == null)
            return;

        $toDelete = $slide->getElementById($this->elementId);

        if ($toDelete == null)
            return;

        $slide->getElements()->removeElement($toDelete);

        /** @var SlideElement[] $elements */
        $elements = array_values($slide->getElements()->toArray());

        uasort($elements, function (SlideElement $a, SlideElement $b) {
            return $a->getZ() < $b->getZ() ? -1 : 1;
        });

        /**
         * Decrease the z index of all elements after the deleted one by one.
         * Elements must be sorted.
         */
        for ($i = $toDelete->getZ(); $i < count($elements); $i++) {
            $element = $elements[$i];
            $element->setZ($element->getZ() - 1);
        }

        ServiceHelper::dm()->remove($toDelete);
        ServiceHelper::dm()->flush();

        $onSlideElementRemovedMessage = new OnSlideElementRemovedMessage($conn);
        $onSlideElementRemovedMessage->elementId = $this->elementId;
        $onSlideElementRemovedMessage->slideId = $this->slideId;
        $onSlideElementRemovedMessage->sendToOthers();
    }
}