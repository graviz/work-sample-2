<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\AuthenticationMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class HandshakeMessage extends IncommingMessage {

    /**
     * @var string
     * @JSON\JsonProperty(name="session", type="string")
     */
    public $session;

    /**
     * @var string
     * @JSON\JsonProperty(name="deck", type="string")
     */
    public $deckId;

    public function execute(Connection $con) {

        /** @var WaveUser $user */
        $user = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')->findOneBy([
            WaveUser::SESSION . '.id' => $this->session
        ]);

        $authMessage = new AuthenticationMessage($con);

        if ($user != null) {
            $deck = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($this->deckId);

            if ($deck == null) {
                $authMessage->error = Connection::ERROR_DECK_NOT_FOUND;
                $authMessage->failed();
            } else {
                $con->registerAtServer($user, $deck->getId());
                $authMessage->success($deck);
            }

        } else {
            $authMessage->error = Connection::ERROR_SESSION_INVALID;
            $authMessage->failed();
        }
    }


}