<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesCoreBundle\Document\Slide;
use HIP\WaveSlidesSocketBundle\DTO\UISlideView;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OnSlideChangedMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class SlideChangedMessage extends IncommingMessage {

    /**
     * @var UISlideView
     * @JSON\JsonProperty(name="view", type="HIP\WaveSlidesSocketBundle\DTO\UISlideView")
     */
    public $view;

    public function execute(Connection $conn) {
        $deck = $conn->getDeck();
        $slide = $deck->getSlideById($this->view->id);

        if ($slide == null)
            return;

        $slide->setStyles($this->view->styles);

        /**
         * Slides must be sorted
         */
        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        if ($this->view->index > $slide->getIndex()) // slide pulled down
            $this->shiftLeft($slides, $this->view->index, $slide->getIndex());
        else if ($this->view->index < $slide->getIndex()) // slide pulled up
            $this->shiftRight($slides, $this->view->index, $slide->getIndex());

        if ($this->view->index != $slide->getIndex())
            $slide->setIndex($this->view->index);

        ServiceHelper::dm()->flush();

        $onSlideChangedMessage = new OnSlideChangedMessage($conn);
        $onSlideChangedMessage->view = $this->view;
        $onSlideChangedMessage->sendToOthers();
    }

    private function shiftLeft(ArrayCollection $slides, $index, $oldIndex) {
        for ($i = $oldIndex + 1; $i <= $index; $i++) {
            /** @var Slide $slide */
            $slide = $slides->get($i);
            $slide->setIndex($slide->getIndex() - 1);
        }
    }

    private function  shiftRight(ArrayCollection $slides, $index, $oldIndex) {
        for ($i = $index; $i < $oldIndex; $i++) {
            /** @var Slide $slide */
            $slide = $slides->get($i);
            $slide->setIndex($slide->getIndex() + 1);
        }
    }
}