<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OnSlideRemovedMessage;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;

class SlideRemovedMessage extends IncommingMessage {

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function execute(Connection $conn) {
        $deck = $conn->getDeck();

        if ($deck->getSlides()->count() == 1)
            return;

        $slideForDelete = $deck->getSlideById($this->slideId);

        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        for ($i = $slideForDelete->getIndex(); $i < $slides->count(); $i++) {
            $slide = $slides->get($i);
            $slide->setIndex($slide->getIndex() - 1);
        }

        $deck->getSlides()->removeElement($slideForDelete);

        ServiceHelper::dm()->remove($slideForDelete);
        ServiceHelper::dm()->flush();

        $onSlideRemovedMessage = new OnSlideRemovedMessage($conn);
        $onSlideRemovedMessage->slideId = $this->slideId;
        $onSlideRemovedMessage->sendToOthers();
    }
}