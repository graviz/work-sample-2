<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ImageElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptLanguage;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableRow;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TextElement;
use HIP\WaveSlidesSocketBundle\DTO\UIImageView;
use HIP\WaveSlidesSocketBundle\DTO\UIScriptView;
use HIP\WaveSlidesSocketBundle\DTO\UISlideElementView;
use HIP\WaveSlidesSocketBundle\DTO\UITableView;
use HIP\WaveSlidesSocketBundle\DTO\UITextView;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OnSlideElementChangedMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Received, when the user has changed a slide element in any way.
 */
class SlideElementChangedMessage extends IncommingMessage {

    /**
     * @var UISlideElementView
     * @JSON\JsonProperty(name="view", type="HIP\WaveSlidesSocketBundle\DTO\UISlideElementView")
     */
    public $view;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    /**
     * Update the values of an SlideElement by the values of the DTO.
     * @param Connection $conn
     * @return void
     */
    public function execute(Connection $conn) {
        $deck = $conn->getDeck();
        $slide = $deck->getSlideById($this->slideId);

        if ($slide == null)
            return;

        /** @var SlideElement $slideElement */
        $slideElement = $slide->getElementById($this->view->id);

        if ($slideElement == null)
            return;

        $this->updateSlideElement($slideElement);

        if ($this->view instanceof UITextView)
            $this->updateTextElement($slideElement);
        else if ($this->view instanceof UIImageView)
            $this->updateImageElement($slideElement);
        else if ($this->view instanceof UIScriptView)
            $this->updateScriptElement($slideElement);
        else if ($this->view instanceof UITableView)
            $this->updateTableElement($slideElement);

        ServiceHelper::dm()->flush();

        $outgoingMessage = new OnSlideElementChangedMessage($conn);
        $outgoingMessage->view = $this->view;
        $outgoingMessage->slideId = $this->slideId;
        $outgoingMessage->sendToOthers();
    }

    private function updateTextElement(TextElement $textElement) {
        /** @var UITextView $view */
        $view = $this->view;
        $textElement->setText($view->text);
    }

    private function updateImageElement(ImageElement $imageElement) {
    }

    private function updateScriptElement(ScriptElement $scriptElement) {
        /** @var UIScriptView $view */
        $view = $this->view;

        $scriptLanguage = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:SlideElements\ScriptLanguage')->findOneBy([ScriptLanguage::Value => $view->langValue]);
        $scriptElement->setLanguage($scriptLanguage);

        $view->langName = $scriptLanguage->getName();
        $view->langCss = $scriptLanguage->getCss();
        $this->view = $view;
    }

    private function updateTableElement(TableElement $tableElement) {
        /** @var UITableView $view */
        $view = $this->view;

        $rows = new ArrayCollection();

        foreach ($view->rows as $row) {
            $tableRow = new TableRow();
            $tableRow->setCells(new ArrayCollection($row->cells));

            $rows->add($tableRow);
        }

        $tableElement->setRows($rows);
    }

    private function  updateSlideElement(SlideElement $slideElement) {
        $slideElement->setStyles($this->view->styles);
        $slideElement->setBounds($this->view->bounds);
        $slideElement->setZ($this->view->z);
        $slideElement->setAlpha($this->view->alpha);
    }
}