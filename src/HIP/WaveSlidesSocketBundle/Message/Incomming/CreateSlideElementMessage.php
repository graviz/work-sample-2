<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\ScriptElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\SlideElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableElement;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableRow;
use HIP\WaveSlidesCoreBundle\Document\SlideElements\TextElement;
use HIP\WaveSlidesCoreBundle\Widget\SlideElementWidgetFactory;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\SlideElementCreatedMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class CreateSlideElementMessage extends IncommingMessage {

    /**
     * Determine which slide element should be created.
     * @var string
     * @JSON\JsonProperty(name="viewType", type="string")
     */
    public $viewType;

    /**
     * On which slide the element should be created.
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function execute(Connection $conn) {
        $slide = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:Slide')->find($this->slideId);

        /** @var SlideElement $slidElement */
        $slidElement = null;

        switch ($this->viewType) {
            case TextElement::Role:
                $slidElement = $this->createTextElement();
                break;
            case ScriptElement::Role:
                $slidElement = $this->createScriptElement();
                break;
            case TableElement::Role:
                $slidElement = $this->createTableElement();
                break;
        }

        if ($slidElement != null) {
            $slidElement->setZ($slide->getElements()->count());

            $slide->getElements()->add($slidElement);

            ServiceHelper::dm()->flush();

            $slideElementCreatedMessage = new SlideElementCreatedMessage($conn);
            $slideElementCreatedMessage->viewType = $this->viewType;
            $slideElementCreatedMessage->slideId = $this->slideId;
            $slideElementCreatedMessage->createdView = SlideElementWidgetFactory::render($slidElement, '/app_dev.php/deck/image/');
            $slideElementCreatedMessage->sendToAll();
        }
    }

    /**
     * @return TextElement
     */
    private function createTextElement() {
        return new TextElement();
    }

    /**
     * @return ScriptElement
     */
    private function createScriptElement() {
        return new ScriptElement();
    }

    /**
     * @return TableElement
     */
    private function createTableElement() {
        $table = new TableElement();

        for ($i = 0; $i < 5; $i++) {
            $tableRow = new TableRow();
            for ($j = 0; $j < 5; $j++)
                $tableRow->getCells()->add(TableCell::make("Text"));

            $table->getRows()->add($tableRow);
        }

        $table->getBounds()->x = 10;
        $table->getBounds()->y = 10;

        return $table;
    }
}