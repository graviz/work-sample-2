<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Incomming;


use Doctrine\Common\Collections\ArrayCollection;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesCoreBundle\Document\Slide;
use HIP\WaveSlidesCoreBundle\Widget\SlideThumbnailWidget;
use HIP\WaveSlidesCoreBundle\Widget\SlideWidget;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OnSlideCreatedMessage;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;

class CreateSlideMessage extends IncommingMessage {

    public function execute(Connection $conn) {

        $deck = $conn->getDeck();
        $activeSlide = $conn->getActiveSlide();

        if ($activeSlide == null)
            return;

        $insertIndex = $activeSlide->getIndex();

        /**
         * Slides must be sorted
         */
        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        /**
         * Insert the slide at the current active slide.
         * Increase the index of all slides started by the next slide of the active slide by one.
         */
        for ($i = $insertIndex + 1; $i < count($slides); $i++) {
            /** @var Slide $slide */
            $slide = $slides->get($i);
            $slide->setIndex($slide->getIndex() + 1);
        }

        $slide = new Slide();
        $slide->setIndex($insertIndex + 1);
        $deck->getSlides()->add($slide);

        ServiceHelper::dm()->flush();

        $onSlideCreatedMessage = new OnSlideCreatedMessage($conn);
        $onSlideCreatedMessage->createdView = SlideWidget::render($slide, '');
        $onSlideCreatedMessage->thumbnail = SlideThumbnailWidget::render($slide);
        $onSlideCreatedMessage->sendToAll();
    }

}