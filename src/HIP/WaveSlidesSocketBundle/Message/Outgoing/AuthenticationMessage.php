<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;


use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Common\Sorter;
use HIP\WaveSlidesCoreBundle\Document\ChatMessage;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Widget\ChatMessageWidget;
use HIP\WaveSlidesCoreBundle\Widget\SlideThumbnailWidget;
use HIP\WaveSlidesCoreBundle\Widget\SlideWidget;
use HIP\WaveSlidesSocketBundle\DTO\UserDto;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use HIP\WaveSlidesSocketBundle\WebSocket\Server;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class AuthenticationMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Outgoing
 *
 * @JSON\JsonInclude(JSON\JsonInclude::INCLUDE_NON_NULL)
 */
class AuthenticationMessage extends OutgoingMessage {

    const Type = 'AuthenticationMessage';

    /**
     * @var boolean
     * @JSON\JsonProperty(name="success", type="bool")
     */
    public $success = false;

    /**
     * @var int
     * @JSON\JsonProperty(name="error", type="int")
     */
    public $error;

    /**
     * @var UserDto[]
     * @JSON\JsonProperty(name="users", type="HIP\WaveSlidesSocketBundle\DTO\UserDto[]")
     */
    public $users = [];

    /**
     * @var DeckComponent
     * @JSON\JsonProperty(name="deck", type="HIP\WaveSlidesSocketBundle\Message\Outgoing\DeckComponent")
     */
    public $deckComponent;

    /**
     * @var string[]
     * @JSON\JsonProperty(name="chats", type="string[]")
     */
    public $chatMessages;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
        $this->deckComponent = new DeckComponent();
    }

    public function failed() {
        $this->success = false;
        $this->send();
    }

    public function success(Deck $deck) {
        $this->success = true;
        $this->deckComponent->format = $deck->getFormat();

        /** Slides must be sorted */
        $slides = Sorter::SortSlidesAsc($deck->getSlides());

        foreach ($slides as $slide) {
            $this->deckComponent->slides[] = SlideWidget::render($slide, true);
            $this->deckComponent->thumbnails[] = SlideThumbnailWidget::render($slide);
        }

        $deckPool = Server::instance()->deckPool($deck->getId());
        $users = $deckPool->getUsers();

        foreach ($users as $user) {
            $color = $deckPool->userPool($user)->getColor();
            $this->users[] = UserDto::create($user, $color);
        }

        /** @var ChatMessage[] $messages */
        $messages = ServiceHelper::dm()
            ->getRepository('HIPWaveSlidesCoreBundle:ChatMessage')
            ->createQueryBuilder()
            ->field('deck')
            ->references($deck)
            ->limit(50)
            ->sort('date', 'asc')
            ->getQuery()
            ->execute();

        foreach ($messages as $message)
            $this->chatMessages[] = ChatMessageWidget::render(
                $message->getSender(),
                $message->getText(),
                $message->getDate(),
                null
            );

        $this->send();
    }

}

/**
 * Class DeckComponent
 * @package HIP\WaveSlidesSocketBundle\Message\Outgoing
 */
class DeckComponent {

    /**
     * @var string[]
     * @JSON\JsonProperty(name="slides", type="string[]")
     */
    public $slides = [];

    /**
     * @var string[]
     * @JSON\JsonProperty(name="thumbnails", type="string[]")
     */
    public $thumbnails = [];

    /**
     * @var string
     * @JSON\JsonProperty(name="format", type="string")
     */
    public $format;

}