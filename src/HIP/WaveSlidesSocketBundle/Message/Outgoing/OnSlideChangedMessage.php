<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;


use HIP\WaveSlidesSocketBundle\DTO\UISlideView;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class OnSlideChangedMessage extends OutgoingMessage {

    const Type = 'OnSlideChangedMessage';

    /**
     * @var UISlideView
     * @JSON\JsonProperty(name="view", type="HIP\WaveSlidesSocketBundle\DTO\UISlideView")
     */
    public $view;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }


} 