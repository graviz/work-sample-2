<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class OnSlideRemovedMessage extends OutgoingMessage {

    const Type = 'OnSlideRemovedMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }

}
