<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\DTO\UserDto;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;

/**
 * Class UserLeftMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Outgoing
 */
class UserLeftMessage extends OutgoingMessage {

    const Type = 'UserLeftMessage';

    /**
     * @var UserDto
     * @JSON\JsonProperty(name="userId", type="string")
     */
    public $userId;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }

} 