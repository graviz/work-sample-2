<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\Console;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use HIP\WaveSlidesSocketBundle\WebSocket\MessageDecoder;
use HIP\WaveSlidesSocketBundle\WebSocket\Server;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class OutgoingMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Outgoing
 * @JSON\JsonIgnoreProperties(names={"connection"})
 */
abstract class OutgoingMessage {

    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var string
     * @JSON\JsonProperty(name="sender", type="string")
     */
    public $sender;

    /**
     * @var string
     * @JSON\JsonProperty(name="type", type="string")
     */
    public $type;

    public function __construct($type, Connection $conn = null) {
        $this->connection = $conn;
        $this->type = $type;

        if ($conn != null)
            $this->sender = $conn->getId();
    }

    /**
     * Send this message to a Collection of connections.
     * @param Connection[] $cons
     */
    public function sendTo(array $cons) {
        if (empty($cons))
            return;

        $message = $this->toJson();
        foreach ($cons as $con)
            $con->send($message);
    }

    /**
     * Send the message to the sender.
     */
    public function send() {
        $this->connection->send($this->toJson());
    }

    /**
     * Sends the message to all open connections of the deck, except the connection which send this message.
     */
    public function sendToOthers() {
        if (Server::instance()->deckPool($this->connection->getDeckId())->size() == 1)
            return;

        $msg = $this->toJson();

        foreach (Server::instance()->deckPool($this->connection->getDeckId())->all() as $con)
            if ($con->getId() != $this->connection->getId())
                $con->send($msg);
    }

    /**
     * Sends the message to all open connection of the deck.
     */
    public function sendToAll() {
        $msg = $this->toJson();

        foreach (Server::instance()->deckPool($this->connection->getDeckId())->all() as $con)
            $con->send($msg);
    }

    /**
     * @return string
     */
    private function  toJson() {
        $msg = MessageDecoder::encode($this);
        Console::info('ws.SEND', $msg);
        return $msg;
    }

}