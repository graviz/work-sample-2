<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\DTO\UserDto;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;

/**
 * Class UserJoinedMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Outgoing
 */
class UserJoinedMessage extends OutgoingMessage {

    const Type = 'UserJoinedMessage';

    /**
     * @var UserDto
     * @JSON\JsonProperty(name="user", type="HIP\WaveSlidesSocketBundle\DTO\UserDto")
     */
    public $user;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }
} 