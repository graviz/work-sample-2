<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class OnSlideCreatedMessage extends SlideElementCreatedMessage {

    const Type = 'OnSlideCreatedMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="thumbnail", type="string")
     */
    public $thumbnail;

    public function __construct(Connection $conn) {
        parent::__construct($conn, self::Type);
    }

}