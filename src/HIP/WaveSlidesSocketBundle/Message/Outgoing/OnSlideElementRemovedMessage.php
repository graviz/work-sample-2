<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class OnSlideElementRemovedMessage extends OutgoingMessage {

    const Type = 'OnSlideElementRemovedMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    /**
     * @var string
     * @JSON\JsonProperty(name="elementId", type="string")
     */
    public $elementId;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }

}

