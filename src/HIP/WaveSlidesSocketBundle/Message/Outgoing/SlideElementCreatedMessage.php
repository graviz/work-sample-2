<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Inform other connections, that a user have created a slide element.
 */
class SlideElementCreatedMessage extends OutgoingMessage {

    const Type = 'SlideElementCreatedMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="createdView", type="string")
     */
    public $createdView;

    /**
     * @var string
     * @JSON\JsonProperty(name="viewType", type="string")
     */
    public $viewType;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function __construct(Connection $conn, $type = null) {
        if ($type == null)
            parent::__construct(self::Type, $conn);
        else
            parent::__construct($type, $conn);

    }

}

