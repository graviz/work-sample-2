<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Outgoing;

use HIP\WaveSlidesSocketBundle\DTO\UISlideElementView;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class OnSlideElementChangedMessage extends OutgoingMessage {

    const Type = 'OnSlideElementChangedMessage';

    /**
     * @var UISlideElementView
     * @JSON\JsonProperty(name="view", type="HIP\WaveSlidesSocketBundle\DTO\UISlideElementView")
     */
    public $view;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    public function __construct(Connection $conn) {
        parent::__construct(self::Type, $conn);
    }

}
