<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Bi;


use HIP\WaveSlidesCoreBundle\Common\HTMLFilter;
use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\ChatMessage;
use HIP\WaveSlidesCoreBundle\Widget\ChatMessageWidget;
use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class ChatMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Bi
 *
 * @JSON\JsonInclude(JSON\JsonInclude::INCLUDE_NON_NULL)
 */
class WSChatMessage extends BidirectionalMessage {

    const Type = 'ChatMessage';

    /**
     * @var string
     * @JSON\JsonProperty(name="text", type="string")
     */
    public $text;

    /**
     * @var string
     * @JSON\JsonProperty(name="userId", type="string")
     */
    public $userId;

    /**
     * Only used when this Message is used as an outgoing one.
     * @JSON\JsonProperty(name="view", type="string")
     */
    public $view;

    public function __construct(Connection $conn = null) {
        parent::__construct(self::Type, $conn);
    }

    public function execute(Connection $conn) {
        $chatMessage = new ChatMessage();
        $chatMessage->setDeck($conn->getDeck());

        /**
         * For some reason it is not possible to reference the user from the connection object.
         * -> Cannot create a DBRef without an identifier. UnitOfWork::getDocumentIdentifier() did not return an identifier for class HIP\WaveSlidesCoreBundle\Document\Account\WaveUser
         */
        $user = ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:Account\WaveUser')->find($conn->getUser()->getId());
        $chatMessage->setSender($user);
        $chatMessage->setText($this->text);
        ServiceHelper::dm()->persist($chatMessage);
        ServiceHelper::dm()->flush();

        $outChatMessage = new WSChatMessage($conn);
        $outChatMessage->userId = $conn->getUser()->getId();
        $outChatMessage->view = ChatMessageWidget::render($conn->getUser(), $this->text, $chatMessage->getDate(), $conn->userPool()->getColor());
        $outChatMessage->sendToAll();
    }
}