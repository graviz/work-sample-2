<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Bi;

use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;


/**
 * Class SlideElementSelectedMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Bi
 *
 * @JSON\JsonInclude(JSON\JsonInclude::INCLUDE_NON_NULL)
 */
class SlideElementSelectedMessage extends SelectionMessage {

    const Type = 'SlideElementSelectedMessage';

    public function __construct(Connection $conn = null) {
        parent::__construct(self::Type, $conn);
    }

    public function execute(Connection $conn) {
        $this->connection = $conn;
        $this->sendToOthers();
    }

}