<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Bi;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Handled when the user has selected or unselected a slide element.
 * This Message is directly sent to all other connections after it has been received.
 */
abstract class SelectionMessage extends BidirectionalMessage {

    /**
     * @var string
     * @JSON\JsonProperty(name="elementId", type="string")
     */
    public $elementId;

    /**
     * @var string
     * @JSON\JsonProperty(name="slideId", type="string")
     */
    public $slideId;

    /**
     * @var string
     * @JSON\JsonProperty(name="userId", type="string")
     */
    public $userId;

}