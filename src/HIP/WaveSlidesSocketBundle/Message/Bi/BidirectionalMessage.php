<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Bi;

use HIP\WaveSlidesSocketBundle\Message\Executable;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\OutgoingMessage;

abstract class BidirectionalMessage extends OutgoingMessage implements Executable {


}