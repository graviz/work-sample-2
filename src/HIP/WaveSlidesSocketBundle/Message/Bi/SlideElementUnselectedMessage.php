<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message\Bi;


use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class SlideElementUnselectedMessage
 * @package HIP\WaveSlidesSocketBundle\Message\Bi
 *
 * @JSON\JsonInclude(JSON\JsonInclude::INCLUDE_NON_NULL)
 */
class SlideElementUnselectedMessage extends SelectionMessage {

    const Type = 'SlideElementUnselectedMessage';

    public function __construct(Connection $conn = null) {
        parent::__construct(self::Type, $conn);
    }

    public function execute(Connection $conn) {
        $this->connection = $conn;
        $this->sendToOthers();
    }
}