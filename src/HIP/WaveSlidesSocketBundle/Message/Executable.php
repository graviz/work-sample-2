<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Message;


use HIP\WaveSlidesSocketBundle\WebSocket\Connection;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * @JSON\JsonTypeInfo(use=JSON\JsonTypeInfo::ID_NAME, include=JSON\JsonTypeInfo::AS_PROPERTY, property="type")
 * @JSON\JsonSubTypes({
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Bi\SlideElementSelectedMessage", name="SlideElementSelectedMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Bi\SlideElementUnselectedMessage", name="SlideElementUnselectedMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Bi\WSChatMessage", name="ChatMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\ChangeActiveSlideMessage", name="ChangeActiveSlideMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\CreateSlideElementMessage", name="CreateSlideElementMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\CreateSlideMessage", name="CreateSlideMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\HandshakeMessage", name="HandshakeMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\SlideChangedMessage", name="SlideChangedMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\SlideElementChangedMessage", name="SlideElementChangedMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\SlideElementRemovedMessage", name="SlideElementRemovedMessage"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\Message\Incomming\SlideRemovedMessage", name="SlideRemovedMessage")
 * })
 */
interface Executable {

    /**
     * @param Connection $conn
     * @return mixed
     */
    public function execute(Connection $conn);

}