<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\Command;

use HIP\WaveSlidesSocketBundle\Console;
use HIP\WaveSlidesSocketBundle\WebSocket\Server;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

class SocketCommand extends ContainerAwareCommand {

    const DEBUG = 'debug';

    protected function configure() {
        $this
            ->setName('ws:start')
            ->setDescription('Start the WS Server')
            ->addOption('debug', null, InputOption::VALUE_NONE, 'Enable debugging');
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $debug = $input->getOption('debug');

        if ($debug) {
            Debug::enable();
            $output->setVerbosity(OutputInterface::VERBOSITY_DEBUG);
        }

        Console::init($output);

        $this->getContainer()->enterScope('request');
        $this->getContainer()->set('request', Request::createFromGlobals(), 'request');
        Server::instance()->run();
    }
}
