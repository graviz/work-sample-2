<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\WebSocket;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Deck;
use HIP\WaveSlidesCoreBundle\Document\Slide;
use HIP\WaveSlidesSocketBundle\DTO\UserDto;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\UserJoinedMessage;
use HIP\WaveSlidesSocketBundle\Message\Outgoing\UserLeftMessage;
use Ratchet\ConnectionInterface;

/**
 * Class Connection
 * @package HIP\WaveSlidesSocketBundle\WebSocket
 *
 * Wrapper class for Ratchet ConnectionInterface
 */
class Connection {

    const ERROR_DECK_NOT_FOUND = 102;
    const ERROR_SESSION_INVALID = 100;

    /** @var ConnectionInterface */
    protected $wsCon;

    /** @var WaveUser */
    private $user;

    /** @var string */
    private $activeSlide;

    /** @var string */
    private $deckId;

    /** @var string */
    private $id;

    public function __construct(ConnectionInterface $wsCon) {
        $this->wsCon = $wsCon;
        /** @noinspection PhpUndefinedFieldInspection */
        $this->id = $wsCon->resourceId;
    }

    public function userPool() {
        return Server::instance()->deckPool($this->deckId)->userPool($this->user);
    }

    /**
     * @param WaveUser $user
     * @param string $deckId
     *
     * Register a connection on the server pool.
     */
    public function registerAtServer(WaveUser $user, $deckId) {
        $this->user = $user;
        $this->deckId = $deckId;

        Server::instance()->addConnection($this);

        /**
         * If this is the first connection of the user we have to notify other users that a new user has joined
         */
        if (Server::instance()->deckPool($deckId)->userPool($user)->size() == 1) {
            $color = Server::instance()->deckPool($deckId)->userPool($user)->getColor();
            $userJoinedMessage = new UserJoinedMessage($this);
            $userJoinedMessage->user = UserDto::create($user, $color);
            $userJoinedMessage->sendToOthers();
        }

    }

    /**
     * Removes the connection from the server pool if it is logged in.
     */
    public function logoff() {
        if (!$this->isLoggedIn())
            return;

        $userPool = Server::instance()->deckPool($this->deckId)->userPool($this->user);

        if ($userPool->size() == 1) {
            $userLeftMessage = new UserLeftMessage($this);
            $userLeftMessage->userId = $this->user->getId();
            $userLeftMessage->sendToOthers();
        }

        Server::instance()->deckPool($this->deckId)->removeConnection($this);
    }

    /**
     * @param string $msg
     */
    public function send($msg) {
        $this->wsCon->send($msg);
    }

    /**
     * @return WaveUser
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getDeckId() {
        return $this->deckId;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Deck
     */
    public function getDeck() {
        return ServiceHelper::dm()->getRepository('HIPWaveSlidesCoreBundle:Deck')->find($this->deckId);
    }

    /**
     * @return string
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Returns true if the user is logged in.
     * A user is logged in after he sent a success handshake message.
     */
    protected function isLoggedIn() {
        return $this->user != null;
    }

    /**
     * @param $slideId
     */
    public function setActiveSlide($slideId) {
        $this->activeSlide = $slideId;
    }

    /**
     * @return Slide
     */
    public function getActiveSlide() {
        foreach ($this->getDeck()->getSlides() as $slide) /** @var Slide $slide */
            if ($this->activeSlide == $slide->getId())
                return $slide;

        return null;
    }

    public function getActiveSlideId() {
        return $this->activeSlide;
    }

}