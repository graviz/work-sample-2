<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\WebSocket;

use HIP\WaveSlidesCoreBundle\Common\Json;
use HIP\WaveSlidesSocketBundle\Message\Executable;

class MessageDecoder {

    const TYPE = 'HIP\WaveSlidesSocketBundle\Message\Executable';

    /**
     * @param $msg string
     * @return Executable
     */
    public static function decode($msg) {
        return Json::decode($msg, self::TYPE);
    }

    /**
     * @param $object
     * @return string
     */
    public static function encode($object) {
        return Json::encode($object);
    }

}