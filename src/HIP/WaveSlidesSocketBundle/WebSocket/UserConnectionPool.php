<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\WebSocket;

use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Color;

//TODO:: we should create a new color after the user has left for 30-60min
class UserConnectionPool {

    /** @var WaveUser */
    protected $user;

    /** @var Color */
    protected $color;

    /**
     * @var Connection[]
     */
    protected $connections = [];

    public function __construct(WaveUser $user) {
        $this->user = $user;
        $this->color = Color::make(rand(15, 90) / 100, rand(15, 90) / 100, rand(15, 90) / 100);
    }

    /**
     * Add a connection to this pool.
     */
    public function putConnection(Connection $con) {
        $this->connections[$con->getId()] = $con;
    }

    /**
     * Get the number of open connections.
     * @return int
     */
    public function size() {
        return count($this->connections);
    }

    /**
     * Remove a connection from this pool.
     * @param Connection $con
     */
    public function removeConnection(Connection $con) {
        unset($this->connections[$con->getId()]);
    }

    public function all() {
        return $this->connections;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Account\WaveUser
     */
    public function getUser() {
        return $this->user;
    }

    /**
     * @return \HIP\WaveSlidesCoreBundle\Document\Color
     */
    public function getColor() {
        return $this->color;
    }
} 