<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\WebSocket;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\ZMQ\ZMQHelper;
use HIP\WaveSlidesSocketBundle\Console;
use HIP\WaveSlidesSocketBundle\Message\Executable;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServer;
use Ratchet\MessageComponentInterface;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Factory;

class Server implements MessageComponentInterface {

    /**
     * @var Server
     */
    private static $instance;

    /**
     * @var DeckConnectionPool[]
     * Key Value: DeckId = > Pool
     */
    protected $deckPools = [];

    /**
     * @var Connection[]
     */
    protected $connections = [];

    public static function instance() {
        if (self::$instance == null)
            self::$instance = new Server();

        return self::$instance;
    }

    private function __construct() {
    }

    private function __clone() {
    }

    public function run() {
        $app = new self();
        $loop = Factory::create();

        $socket = ZMQHelper::createServer($loop);
        /** @noinspection PhpUndefinedMethodInspection */
        $socket->on('message', array($app, 'onZMQMessage'));

        $webSocket = new \React\Socket\Server($loop);
        $webSocket->listen(
            ServiceHelper::parameter('ws_port'),
            ServiceHelper::parameter('ws_bind_ip')
        );

        new IoServer(
            new  HttpServer(
                new WsServer(
                    $app
                )
            ),
            $webSocket
        );

        Console::info('ws.INFO', 'Server started.');

        $loop->run();
    }

    /**
     * onOpen
     *
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn) {
        $connection = new Connection($conn);
        $this->connections[$connection->getId()] = $connection;
    }

    /**
     * onOpen
     *
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg) {
        Console::info('ws.message', $msg);

        /** @var Executable $message */
        $message = MessageDecoder::decode($msg);

        if ($message != null) {
            /** @noinspection PhpUndefinedFieldInspection */
            $message->execute($this->con($from));
            /** Detach all entities. Its important. */
            ServiceHelper::dm()->clear();
        } else
            $from->close();

    }

    /**
     * onClose
     *
     * @param ConnectionInterface $wsCon
     */
    public function onClose(ConnectionInterface $wsCon) {
        $conn = $this->con($wsCon);
        $conn->logoff();

        if (isset($this->deckPools[$conn->getDeckId()]))
            $this->deckPools[$conn->getDeckId()]->removeConnection($conn);

        /** @noinspection PhpUndefinedFieldInspection */
        unset($this->connections[$wsCon->resourceId]);
    }

    /**
     * onError
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e) {
        Console::error('ws.error', $e->getMessage());
        Console::error('ws.error', $e->getTraceAsString());
    }

    /**
     * @param string $json
     */
    public function onZMQMessage($json) {
        Console::info('ws.zmq.message', $json);
        ZMQHelper::decode($json)->handle($this);
    }

    public function addConnection(Connection $conn) {
        if (!isset($this->deckPools[$conn->getDeckId()]))
            $deckPool = new DeckConnectionPool($conn->getDeckId());
        else
            $deckPool = $this->deckPools[$conn->getDeckId()];

        $deckPool->putConnection($conn);
        $this->deckPools[$deckPool->getDeckId()] = $deckPool;
    }

    /**
     * Returns Connection based on id.
     *
     * @param $id
     * @return Connection
     */
    public function connection($id) {
        return $this->connections[$id];
    }

    /**
     * @param $deckId
     * @return DeckConnectionPool
     */
    public function deckPool($deckId) {
        return $this->deckPools[$deckId];
    }

    /**
     * Returns Connection based on ConnectionInterface
     *
     * @param ConnectionInterface $conn
     * @return Connection
     */
    protected function con(ConnectionInterface $conn) {
        /** @noinspection PhpUndefinedFieldInspection */
        return $this->connections[$conn->resourceId];
    }

}