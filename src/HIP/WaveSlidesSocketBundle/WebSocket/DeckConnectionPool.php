<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\WebSocket;

use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;

class DeckConnectionPool {

    /**
     * @var string
     */
    protected $deckId;

    /**
     * @var UserConnectionPool[]
     */
    protected $connectionPools = [];

    public function __construct($deckId) {
        $this->deckId = $deckId;
    }

    /**
     * @return string
     */
    public function getDeckId() {
        return $this->deckId;
    }

    /**
     * Get an array of all users which have a open connection. No dupes.
     * @return WaveUser[]
     */
    public function getUsers() {
        $users = [];
        foreach ($this->connectionPools as $userPool)
            if ($userPool->size() > 0)
                $users[$userPool->getUser()->getId()] = $userPool->getUser();

        return $users;
    }

    /**
     * Returns all connection by an user.
     * @param WaveUser $user
     * @return UserConnectionPool
     */
    public function userPool(WaveUser $user) {
        return $this->connectionPools[$user->getId()];
    }

    public function putConnection(Connection $conn) {
        if (!isset($this->connectionPools[$conn->getUser()->getId()])) {
            $userPool = new UserConnectionPool($conn->getUser());
            $this->connectionPools[$conn->getUser()->getId()] = $userPool;
        } else
            $userPool = $this->connectionPools[$conn->getUser()->getId()];

        $userPool->putConnection($conn);
    }

    public function removeConnection(Connection $conn) {
        if (isset($this->connectionPools[$conn->getUser()->getId()])) {
            $userPool = $this->connectionPools[$conn->getUser()->getId()];
            $userPool->removeConnection($conn);
        }

        //TODO:: if the con size of the userpool is 0 we should remove the userpool from the pools after 30min ->  https://github.com/krakjoe/pthreads ?
    }

    /**
     * Return all open connections for this deck.
     * @return Connection[]
     */
    public function all() {
        $cons = [];
        foreach ($this->connectionPools as $userPool)
            foreach ($userPool->all() as $con)
                $cons[] = $con;

        return $cons;
    }

    /**
     * Return the number of connections open for this deck.
     * @return int
     */
    public function size() {
        $size = 0;

        foreach ($this->connectionPools as $userPool)
            $size += $userPool->size();

        return $size;
    }

} 