<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class TableRowDTO
 * @package HIP\WaveSlidesSocketBundle\DTO
 */
class TableRowDTO {

    /**
     * @var TableCell[]
     * @JSON\JsonProperty(name="cells", type="HIP\WaveSlidesCoreBundle\Document\SlideElements\TableCell[]")
     */
    public $cells = [];

}