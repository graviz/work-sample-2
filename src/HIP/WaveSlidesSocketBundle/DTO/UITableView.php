<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class UITableView extends UISlideElementView {

    /**
     * @var TableRowDTO[]
     * @JSON\JsonProperty(name="rows", type="HIP\WaveSlidesSocketBundle\DTO\TableRowDTO[]")
     */
    public $rows;

} 