<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class UIImageView extends UISlideElementView {

    /**
     * @var string
     * @JSON\JsonProperty(name="imageRef", type="string")
     */
    public $imageRef;

}