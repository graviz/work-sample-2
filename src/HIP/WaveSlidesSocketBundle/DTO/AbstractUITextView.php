<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use HIP\WaveSlidesCoreBundle\Document\SlideElements\FormattedText;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

abstract class AbstractUITextView extends UISlideElementView {

    /**
     * @var string
     * @JSON\JsonProperty(name="text", type="string")
     */
    public $text;

} 