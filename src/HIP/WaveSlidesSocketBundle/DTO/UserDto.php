<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use HIP\WaveSlidesCoreBundle\Common\ServiceHelper;
use HIP\WaveSlidesCoreBundle\Document\Account\WaveUser;
use HIP\WaveSlidesCoreBundle\Document\Color;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class UserDto
 * @package HIP\WaveSlidesSocketBundle\DTO
 *
 * @JSON\JsonInclude(JSON\JsonInclude::INCLUDE_NON_NULL)
 */
class UserDto {

    /**
     * @var string
     * @JSON\JsonProperty(name="view", type="string")
     */
    public $view;

    /**
     * @var string
     * @JSON\JsonProperty(name="name", type="string")
     */
    public $name;

    /**
     * @var string
     * @JSON\JsonProperty(name="id", type="string")
     */
    public $id;

    /**
     * @var Color
     * @JSON\JsonProperty(name="color", type="HIP\WaveSlidesCoreBundle\Document\Color")
     */
    public $color;

    /**
     * @param WaveUser $user
     * @param Color $color
     * @return UserDto
     */
    public static function create(WaveUser $user, Color $color) {
        $userDto = new UserDto();

        $userDto->color = $color;
        $userDto->id = $user->getId();
        $userDto->name = $user->getUsername();
        $userDto->view = ServiceHelper::templating()->render('HIPWaveSlidesCoreBundle:Widget:user_info_widget.html.twig', [
            'user' => $userDto
        ]);


        return $userDto;
    }


} 