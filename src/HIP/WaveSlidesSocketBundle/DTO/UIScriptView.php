<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class UIScriptView extends AbstractUITextView {

    /**
     * @var string
     * @JSON\JsonProperty(name="langValue", type="string")
     */
    public $langValue;

    /**
     * @var string
     * @JSON\JsonProperty(name="langName", type="string")
     */
    public $langName;

    /**
     * @var string
     * @JSON\JsonProperty(name="langCss", type="string")
     */
    public $langCss;

} 