<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class UIView
 * @package HIP\WaveSlidesSocketBundle\DTO
 */
abstract class UIView {

    /**
     * @var string
     * @JSON\JsonProperty(name="id", type="string")
     */
    public $id;

    /**
     * @var StyleComponent[]
     * @JSON\JsonProperty(name="styles", type="HIP\WaveSlidesCoreBundle\Document\StyleComponent\StyleComponent[string]")
     */
    public $styles = [];

} 