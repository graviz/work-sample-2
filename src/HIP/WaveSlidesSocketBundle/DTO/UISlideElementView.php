<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use HIP\WaveSlidesCoreBundle\Document\Bounds;
use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

/**
 * Class UISlideElementView
 * @package HIP\WaveSlidesSocketBundle\DTO
 *
 * @JSON\JsonTypeInfo(use=JSON\JsonTypeInfo::ID_NAME, include=JSON\JsonTypeInfo::AS_PROPERTY, property="type")
 * @JSON\JsonSubTypes({
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\DTO\UITextView", name="text"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\DTO\UIImageView", name="image"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\DTO\UIScriptView", name="script"),
 *              @JSON\JsonSubTypes\Type(value="HIP\WaveSlidesSocketBundle\DTO\UITableView", name="table")
 * })
 */
abstract class UISlideElementView extends UIView {

    /**
     * @var Bounds
     * @JSON\JsonProperty(name="bounds", type="HIP\WaveSlidesCoreBundle\Document\Bounds")
     */
    public $bounds;

    /**
     * @var float
     * @JSON\JsonProperty(name="alpha", type="float")
     */
    public $alpha;

    /**
     * @var int
     * @JSON\JsonProperty(name="z", type="int")
     */
    public $z;

}