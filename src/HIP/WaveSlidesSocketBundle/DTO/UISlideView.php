<?php
/**
 * @author Stefan Beier
 */

namespace HIP\WaveSlidesSocketBundle\DTO;

use Weasel\JsonMarshaller\Config\DoctrineAnnotations as JSON;

class UISlideView extends UIView {

    /**
     * @var int
     * @JSON\JsonProperty(name="index", type="int")
     */
    public $index;

    /**
     * @var string
     * @JSON\JsonProperty(name="type", type="string")
     */
    public $type;

} 